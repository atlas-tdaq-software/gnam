package gnam

author wainer.vandelli@cern.ch federico.zema@cern.ch roberto.ferrari@cern.ch
manager wainer.vandelli@cern.ch roberto.ferrari@cern.ch

public

use DataflowPolicy
use emon
use ers
use oh
use RunControl
use genconfig
use ROOT	*	DQMCExternal
use Boost       *       TDAQCExternal
use eformat

# Put things to sw repository database

macro sw.repository.binary.gnam:name         "Gnam Monitoring Application"
macro sw.repository.binary.gnam:description  "A Monitoring Application that is configurable through user dynamic libraries"

macro sw.repository.script.reset-or-kill-all-gnam.py:name   "gnamResetter"
macro sw.repository.script.reset-or-kill-all-gnam.py:description  "Script to reset or kill all the gnam instances in a given partition"

private

macro generate-config-include-dirs     "${TDAQ_INST_PATH}/share/data/emon.dal \
                                        ${TDAQ_INST_PATH}/share/data/dal \
					${TDAQ_INST_PATH}/share/data/DFConfiguration"

document generate-config  Gnamdal -s=../databases/gnam/schema/ \
                          namespace="gnamdal"          \
                          include="gnamdal"            \
                          gnam.schema.xml

########   Definition of libraries     ##########

library gnamdal	     		"$(bin)Gnamdal.tmp/*.cpp"

macro gnamdal_shlibflags	"-ldaq-core-dal -lemon-dal"

library gnamutils                     "../gnamutils/*.cxx "

macro root_linkopts " $(shell root-config --libs)"
macro ROOT_CONFIG "$(includes | sed 's/i686-slc5-gcc47-opt/uuuuuyyyyyi686-slc5-gcc43-opt/' )"

#macro_remove cppflags 		         -ansi 
#macro_remove cppflags                   -pedantic
#macro_remove cppflags                   -pedantic
#macro_remove cflags 		     -ansi 
#macro_remove cflags                  -pedantic

########################################################
# WARNING OPTIONS
#
# boost: -Wundef -Wshadow -Wcast-qual 
# root: -Wfloat-equal 

macro warn_common \
  "-Wall -Wall -W -Wpointer-arith -Wcast-align -Wwrite-strings \
-Wsign-compare -Wredundant-decls -Wconversion"

macro warn_cflags \
  "-Wdeclaration-after-statement -Wbad-function-cast -Wstrict-prototypes \
-Wmissing-prototypes -Wnested-externs"

macro warn_cppflags \
  "-Wctor-dtor-privacy -Wnon-virtual-dtor -Wreorder \
-Woverloaded-virtual"

#macro warn_error "-Werror -Wno-unused-parameter"
#macro warn_error ""

macro_append cflags   " $(warn_common) $(warn_cflags) $(warn_error)"

macro_append cppflags " $(warn_common) $(warn_cppflags) $(warn_error)"
#
# WARNING OPTIONS
########################################################

########    Definitions of applications #########

application gnam	            "../src/*.cxx" 

macro_append gnamlinkopts           "$(root_linkopts) -lrc_ItemCtrl \
				    -lemon -lemon-dal -lis -lipc \
                                    -lomniORB4 -lomnithread -lowl \
                                    -lcmdline -loh -lohroot \
                                    -leformat \
				    -ldaq-core-dal -lconfig -lpmgsync \
                                    $(socket_libs) \
                                    -lgnamdal -lgnamutils \
                                    -lboost_filesystem-${boost_libsuffix}"
############################################################################
# Define which binaries to build
############################################################################

macro	constituents    "gnam"

############               Installation          #################

ignore_pattern        inst_headers_auto
apply_pattern install_headers  	name=gnam \        
				src_dir="../gnam" \
                               	target_dir=gnamutils \  
				files="gnamutils/Gnam*.h"

apply_pattern install_headers   name=gnamdal \
                                src_dir="$(bin)/gnamdal" \
                                files="*.hh *.h" \
                                target_dir="../gnamdal/"

macro  gnam_libs       "libgnamdal.so libgnamutils.so"

macro  gnam_apps       "gnam"

apply_pattern install_libs           files="$(gnam_libs)"

apply_pattern install_apps	     files="$(gnam_apps)"

#apply_pattern install_data           name=partitions \
#                                     src_dir="../databases/gnam/partitions" \
#                                     files="*.xml"  \
#                                     target_dir="partitions"

#apply_pattern install_data 	     name=segments \   
#				     src_dir="../databases/gnam/segments" \   
#				     files="*.xml"  \ 
#				     target_dir="segments"

apply_pattern install_db_files 	     name=schema \   
				     src_dir="../databases/gnam/schema" \   
				     files="*.xml"  \ 
				     target_dir="schema"

#Installing helper scripts.
apply_pattern install_scripts src_dir="../scripts" files=" *.py "


#apply_pattern	check_target	name=gnam_test	file=../bin/gnam_test.sh
                                          

############               Dependencies          #################

macro gnamdal_dependencies "generate-dal Gnamdal"
macro gnam_dependencies "gnamdal gnamutils"
