#!/bin/sh

check_with()
{
    count=0
    until $@
      do
      sleep 1
      count=`expr $count + 1`
      
      if test $count -eq 15
	  then
	  echo "Timeout!"
	  echo "(Server was not started)"
	  clear_procs
	  exit 1
      fi
    done
    echo " done. )"
}

check_emon()
{
    count=0
    until grep started $emon_out > /dev/null
      do
      sleep 1
      count=`expr $count + 1`
      
      if test $count -eq 15
	  then
	  echo "Timeout!"
	  echo "(Emon was not started)"
	  clear_procs
	  exit 1
      fi
    done
    echo " done. )"
}


check_sampler()
{
    count=0
    until grep "sampler started" $sampler_out > /dev/null
      do
      sleep 1
      count=`expr $count + 1`
      
      if test $count -eq 15
	  then
	  echo "Timeout!"
	  echo "(Sampler was not started)"
	  clear_procs
	  exit 1
      fi
    done
    echo " done. )"
}

clear_procs()
{
    kill -9 $sampler_pid
    echo "Cleaning everything with ipc_rm: "
    ipc_rm -f -i ".*" -n ".*" -p $partition
    sleep 5
    ipc_rm -f -i ".*" -n ".*"

    echo "Sampler output:"
    cat $sampler_out
}

TDAQ_DIR="/afs/cern.ch/atlas/project/tdaq"
TDAQ_IPC_SILENCE=yes

export TDAQ_DIR
export TDAQ_IPC_SILENCE

export TDAQ_DB_PATH=../databases:$TDAQ_DB_PATH
export TDAQ_DB_DATA=gnam/partitions/gnam.data.xml
export TDAQ_PARTITION=gnam

source get_online_env.sh

if test -r "${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh"
    then
    . ${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh || { echo "setup_daq: can
not set TDAQ envronment variables. Exiting." ; exit 1 ; } ;
else
    echo "Can not access ${TDAQ_INST_PATH}/share/bin/get_tdaq_env.sh. Did you set up Online SW release?"
    exit 1
fi

#start general ipc partition
if ! test_ipc_server; then
    ipc_server >/dev/null 2>&1 &
    echo -n "  ( waiting for initial partition server to startup ..."
    check_with test_ipc_server
fi

partition=gnam

#start gnam ipc partition
if ! test_ipc_server -p $partition; then
    ipc_server -p $partition >/dev/null 2>&1 &
    echo -n "  ( waiting for gnam partition server to startup ..."
    check_with test_ipc_server -p $partition
fi


#start oh server
if ! test_is_server -p $partition -n Histogramming; then
    is_server -p $partition -n Histogramming -b /tmp/Histogramming.backup >/dev/null 2>&1 &
    is_pid=$!
    echo -n "  ( waiting for OH server to startup ..."
    check_with test_is_server -p $partition -n Histogramming
fi

#start emon

emon_out=/tmp/emon.out

if ! ps -f |grep emon | grep $partition > /dev/null; then
    emon_conductor -p $partition > $emon_out 2>&1 &
    emon_pid=$!
    echo -n "  ( waiting for emon conductor to startup ..."
    check_emon
fi    

#sampler

sampler_out=/tmp/sampler.out


#check if the sampler already exists
process=`ps -o pid,args |grep file_sampler |grep $partition |grep "k 1" |grep "v 1"`
if [ $? -eq 0 ];then

    sampler_pid=`expr match "$process" '\(.*\)\ .*file_sampler.*'`
    
    kill -9 $sampler_pid

    if [ $? -eq 0 ];then
	echo "Previous existing sampler killed"
    else
	echo "Impossible to kill the already running file sampler"
	exit 1
    fi
fi

file_sampler -k 1 -v 1 -p $partition -F ../detector_example/Events_v3.0.dat > $sampler_out 2>&1 &
sampler_pid=$!

#wait for sampler
echo -n "  ( waiting for sampler to startup ..."
check_sampler


#with icc we have only the file sampler
if [ ${CMTCONFIG} == i686-slc3-icc8-opt ]; then
    clear_procs
    echo "File sampler check passed"
    exit 0
fi

config_file=/tmp/gnam_conf

echo ${TDAQ_INST_PATH}/${CMTCONFIG}/lib/libDecode1.so > $config_file
echo ${TDAQ_INST_PATH}/${CMTCONFIG}/lib/libHisto1.so >> $config_file

#gnam

../bin/run_gnam.exp - ${CMTCONFIG} $partition $config_file

if [ $? -eq 0 ]; then
    clear_procs
    echo "Gnam & file sampler check passed"
else
    clear_procs
    echo "Gnam & file sampler check not passed"
    exit 1
fi


