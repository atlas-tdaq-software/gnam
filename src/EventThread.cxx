//$Id$

#include "gnam/gnam/EventThread.h"
#include "gnam/gnamutils/GnamMessRep.h"
#define MR_LOG(mess) do { \
    ERS_LOG (mess); \
} while (false) 

#include "gnam/gnam/getrods.h"
#include "gnam/gnam/histos.h"
#include "gnam/gnam/CommListener.h"
#include "gnam/gnam/GnamException.h"
#include "gnam/gnamutils/GnamMessRep.h"
#include "eformat/eformat.h"
#include "eformat/write/eformat.h"

#include <memory>
#include <sstream>

#include <unistd.h>

using namespace daq::gnam;
using namespace daq::gnamlib;

/*
  These two functions (FSM_*) should be equivalent to the Update and Reset
  requests. At present they only deal with histograms, but you should
  not rely on this and treat them as pure histogram management functions.
*/

void set_gnamHistoReady( bool s );
bool get_gnamHistoReady();

static int
FSM_update (OHRootProvider *provider,
            std::vector <struct dynalib> &dynalibs)

{
    GnamHisto::GnamHistoFitEnable (true);
    for (size_t k = 0; k < dynalibs.size (); k++) {
        DLEXEC_VOID_1 (true, dynalibs[k], prePublish, false);
    } 
    GnamHisto::GnamHistoFitEnable (false);
      
    // histo_set_publish_all();
      
#ifndef ERS_NO_DEBUG
    unsigned int channels =
#endif
        histo_publish_all (provider, 0);
    MR_DEBUG (1, "published " << channels << " channels");
      
    return 0;
}

static int
FSM_reset (OHRootProvider *provider,
           std::vector <struct dynalib> & dynalibs)
{
    histo_reset_all ();
    return FSM_update (provider, dynalibs);
}

static int
FSM_warmstart (std::vector <struct dynalib> &dynalibs)
{
    for (size_t k = 0; k < dynalibs.size (); k++) {
        DLEXEC_VOID_0 (true, dynalibs[k], warmStart);
    }
    GnamHisto::warmStart();
    return 0;
}

static int
FSM_warmstop (std::vector <struct dynalib> &dynalibs)
{
    for (size_t k = 0; k < dynalibs.size (); k++) {
        DLEXEC_VOID_0 (true, dynalibs[k], warmStop);
    }
    GnamHisto::warmStop();
    return 0;
}

static void
proc_commands_core (OHRootProvider *provider,
                    std::vector <struct dynalib> & dynalibs,
                    int argc, const char **argv)
{
    // first useful argument
    int first_arg = 0;
    if (argc > 0 && strcasecmp (argv[0], "core") == 0) {
        first_arg = 1;
    }
    if (first_arg >= argc) {
        MR_WARNING (BadCommand (ERS_HERE, "", "null command"));
        return;
    }
    if (strcasecmp (argv[first_arg], "Reset") == 0) {
        FSM_reset (provider, dynalibs);
    } else if (strcasecmp (argv[first_arg], "Update") == 0) {
        FSM_update (provider, dynalibs);
    } else {
        MR_WARNING (BadCommand (ERS_HERE, argv[first_arg]));
        return;
    }
}

int getLibHisto (std::vector <struct dynalib> & dynalibs,
                 const char* name, unsigned int & lib, GnamHisto* & histo )
{
    histo = histo_name2histo (name);
    if (histo)
    {
        lib = histo->GetDynalib();
        return 0;
    }
    for (size_t k = 0; k < dynalibs.size (); k++)
        if (strcasecmp (name, dynalibs[k].name) == 0)
        {
            lib = k;
            return 0;
        }
    return 1;
}

static void
proc_commands (OHRootProvider *provider, 
               std::vector <struct dynalib> & dynalibs,
               GnamVariables & vars)
{
    struct CommQueueEntry *command;
    GnamHisto *histo;
    while ((command = CommQueuePop())) {
        MR_DEBUG (2, "histo: " << command->hname);
        MR_DEBUG (2, "command: " << command->cmd);
        MR_DEBUG (2, "argc: " << command->argc);
        for (int k = 0; k <= command->argc; k++) {
            MR_DEBUG (2, "argv[" << k << "]: "
                      << (command->argv[k] ? command->argv[k] : "NULL"));
        }
        // look for histogram

        // hname = hname_str.c_str()
        //if (command->hname != NULL) {
        ERS_ASSERT (command->hname);
        if (*(command->hname)) {

            histo = histo_name2histo (command->hname);
            if (histo == NULL) {
                std::ostringstream buf;
                buf << command->cmd;
                MR_WARNING (BadCommandHistogram
                            (ERS_HERE, command->hname, buf.str()));
                continue;
            }
            MR_DEBUG (2, "histo: " << command->hname << " - lib: "
                      << dynalibs[histo->GetDynalib()].name);
        } else {
            histo = NULL;
        }
	MR_DEBUG (2, "Going to switch. Histo: " << histo);
        // read command
        switch (command->cmd) {
            //
        case GNAM_COMM_RESET:
            if (histo != NULL) {
                histo_reset (histo, provider);
            } else {
                FSM_reset (provider, dynalibs);
            }
            break;
            //
        case GNAM_COMM_UPDATE:
            if (histo != NULL) {
                GnamHisto::GnamHistoFitEnable (true);
                for (size_t k = 0; k < dynalibs.size (); k++) {
                    DLEXEC_VOID_1 (true, dynalibs[k], prePublish, false);
                }
                GnamHisto::GnamHistoFitEnable (false);
                
                histo->SetPublishFlag();
                
#ifndef ERS_NO_DEBUG
                unsigned int channels = 
#endif
                    histo_publish (histo, provider);
                MR_DEBUG (1, "histo " << histo->GetName()
                          << " published (" << channels << " channels)");
                
            } else {
                FSM_update (provider, dynalibs);
            }
            break;
            //
        case GNAM_COMM_REBIN:
            if (histo == NULL) {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_REBIN_STR,
                             "it must be associated to a histogram"));
                continue;       
            }
            histo_rebin (histo, provider, command->argc, command->argv);
            break;
            //
        case GNAM_COMM_PUBLISHED: {
            if (histo != NULL) {
                histo->IsPublished (false);
                MR_DEBUG (1, "histo " <<  histo->GetName()
                          << " will not be published any more");
            } else {
                GnamHisto *tmp = NULL;
                if (command->argc > 0) {
                    tmp = histo_name2histo (command->argv[0]);
                }
                if (tmp == NULL) {
                    MR_WARNING (BadCommandSyntax
                                (ERS_HERE, GNAM_COMM_PUBLISHED_STR,
                                 " histogram name required"));
                    continue;
                }
                tmp->IsPublished (true);
#ifndef ERS_NO_DEBUG
                unsigned int channels = 
#endif
                    histo_publish (tmp, provider);
                MR_DEBUG (1, "histo " << tmp->GetName() << " published ("
                          << channels << " channels)");
            }
            break;
        }
        case GNAM_COMM_FILLED: {
            if (histo == NULL) {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_FILLED_STR,
                             "it must be associated to a histogram"));
                continue;
            }
            if (command->argc != 1) {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE,  GNAM_COMM_FILLED_STR,
                             "it needs exactly one argument:"
                             " \"true\" or \"false\""));
                continue;
            }
            bool tmp;
            if (strcasecmp (command->argv[0], "true") == 0) {
                tmp = true;
            } else if (strcasecmp (command->argv[0], "false") == 0) {
                tmp = false;
            } else  {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_FILLED_STR,
                             "it needs exactly one argument:"
                             " \"true\" or \"false\""));
                continue;
            }
            histo->IsFilled (tmp);
            MR_DEBUG (1, "histo " << histo->GetName() << ": IsFilled ("
                      << tmp << ")"); 
            break;
        }
        case GNAM_COMM_DUMPED: {
            if (histo == NULL) {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_DUMPED_STR,
                             "it must be associated to a histogram"));
                continue;
            }
            if (command->argc != 1) {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_DUMPED_STR,
                             "it needs exactly one argument:"
                             " \"true\" or \"false\""));
                continue;
            }
            bool tmp;
            if (strcasecmp (command->argv[0], "true") == 0) {
                tmp = true;
            } else if (strcasecmp (command->argv[0], "false") == 0) {
                tmp = false;
            } else  {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_DUMPED_STR,
                             "it needs exactly one argument:"
                             " \"true\" or \"false\""));
                continue;
            }
            histo->IsDumped (tmp);
            MR_DEBUG (1, "histo " << histo->GetName()  << ": IsDumped ("
                      << tmp << ")");
            break;
        }
        case GNAM_COMM_CUSTOM:
            if (command->argc > 0)
            {
                if (strcasecmp (command->argv[0], "core") == 0)
                {
                    proc_commands_core (provider, dynalibs,
                                    command->argc, command->argv);
                } else if (histo != 0) {
                    DLEXEC_VOID_3 (true, dynalibs[histo->GetDynalib()],
                               customCommand, histo,
                               command->argc, command->argv);
                } else {
                    unsigned int lib;
                    if ( getLibHisto( dynalibs, command->argv[0], lib, histo ) == 0) {
                        DLEXEC_VOID_3 (true, dynalibs[lib],
                               customCommand, histo,
                               command->argc-1, &command->argv[1]);
                    }
                }
            }
            break;
        case GNAM_COMM_NEVER_RESET:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->NeverReset(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_NEVER_RESET_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_RESET_AT_WASTA:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->ResetAtWarmStart(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_RESET_AT_WASTA_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_RESET_AT_WASTO:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->ResetAtWarmStop(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_RESET_AT_WASTO_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_RESET_AT_WASS:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->ResetAtWarmSS(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_RESET_AT_WASS_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_FILL_ANYTIME:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->FillAnyTime(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_FILL_ANYTIME_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_FILL_WHEN_READY:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->FillWhenReady(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_FILL_WHEN_READY_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_FILL_WHEN_STANDBY:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->FillWhenStandby(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_FILL_WHEN_STANDBY_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_NEVER_FILL:
            if ((histo == NULL) && (command->argc > 0)) {
                histo = histo_name2histo (command->argv[0]);
            }
            if (histo != NULL) {
                histo->NeverFill(true);
            } else {
                MR_WARNING (BadCommandSyntax
                            (ERS_HERE, GNAM_COMM_NEVER_FILL_STR,
                             " histogram name required"));
            }
            break;
        case GNAM_COMM_WASTA:
            if (!vars.get_runstate()) {
                FSM_warmstart (dynalibs);
                vars.new_runst(true);
            }
            break;
        case GNAM_COMM_WASTO:
            if (vars.get_runstate()) {
                FSM_warmstop (dynalibs);
                vars.new_runst(false);
            }
            break;
            //
        default:
            std::ostringstream buf;
            buf << "unexpected command code: " << command->cmd;
            FatalError (InternalError (ERS_HERE, buf.str()));
        }
    }
}


EventThread::EventThread(GnamVariables &vars,
                         gnamEventIterator *agnam_ev_it,
                         std::vector <struct dynalib> &aDynalibs,
                         OHRootProvider *aProvider)
    : variables (vars),
      dynalibs (aDynalibs),
      m_running (false)
{
    gnam_ev_it = agnam_ev_it;
    provider=aProvider;
}

void EventThread::startExecution()
{
    m_running = true;
    m_thread = std::thread(&EventThread::run, this);
}

void EventThread::stopExecution()
{
    m_running = false;
    m_thread.join();
}

void EventThread::run()
{
    int lp_event = 0, ld_event = 0;
    time_t lp_time;
    lp_time = time (NULL);
    time_t t_mess = 0, first_time_no_event = 0;
    bool err_msg = !(variables.get_try2reconnect());
        
    if (variables.get_runstate()) FSM_warmstart(dynalibs);
  
    while (m_running) {
        time_t now = time (NULL);
	
	MR_DEBUG(2,"Loop start");

        // look for commands
        proc_commands (provider, dynalibs, variables);
        
        //TO DO:
	// in order to sample events only during run, we used 
	// the pause/resume transitions. Since these are gone 
        // (07/2011) now we keep sampling even if there is no
        // dataflow. This could lead to an increase in the number
        // of sampling timeouts. The old behaviour can possibly
        // be recovered polling some IS variable
 
        // check whether it's time to publish the histograms
        if ((variables.get_updatefr_evt() > 0
             && variables.get_eventsinrun() - lp_event >=
             variables.get_updatefr_evt())
            || (variables.get_updatefr_sec() > 0
                && now - lp_time >= variables.get_updatefr_sec()))
        {
            lp_event = variables.get_eventsinrun();
            lp_time = now;
            // update and publish histograms
            GnamHisto::GnamHistoFitEnable (true);
            for (size_t k = 0; k < dynalibs.size (); k++) {
                DLEXEC_VOID_1 (true, dynalibs[k], prePublish, false);
            }
            GnamHisto::GnamHistoFitEnable (false);
#ifndef ERS_NO_DEBUG                
            unsigned int channels =
#endif
                histo_publish_all (provider, 0);
                // histo_publish_all (provider, variables.get_chan2oh());
            MR_DEBUG (0, "published " << channels << " channels");
                
        }

        // check whether it's time to dump the histograms
        if (variables.get_dump() && variables.get_dump_fr_evt() > 0
            && variables.get_eventsinrun() - ld_event >=
            variables.get_dump_fr_evt())
        {
            MR_DEBUG (0, "dumping histos to file");
            histo_dump (variables.get_outfile().c_str(),
                        variables.get_runnumber());
            ld_event = variables.get_eventsinrun();
        }                

        emon::Event event;
        
        // look for an event
        try {
            // we may have to try and reconnect
            if (variables.get_try2reconnect()) {
                MR_DEBUG (1,"trying to re-connect to the sampler");
                gnam_ev_it->disconnect ();
                gnam_ev_it->connect ();
                MR_LOG ("Sampler re-connection successful");
                variables.set_try2reconnect (false);
                err_msg=true;
                // don't complain soon if the new buffer is empty
                first_time_no_event = 0;
            }
            MR_DEBUG (2, "trying nextEvent");
            event = gnam_ev_it->iter()->nextEvent (100);
	    MR_DEBUG (2, "nextEvent done");
            first_time_no_event = 0;
        }
        catch (emon::NoMoreEvents &ex) {
            if (first_time_no_event == 0) {
                first_time_no_event = now;
            }
            //if (now - first_time_no_event > 20) {
            if (now - first_time_no_event > 86400) {
                MR_DEBUG (0, "Got no events in the past 86400 seconds;"
                          " trying and reconnect");
                variables.set_try2reconnect (true);
            } else {
                if (now - t_mess > 2) {
                    MR_DEBUG (1, "Empty eventSampler buffer");
                    t_mess = now;
                }
                usleep (100000);
            }
            continue;
        }
        catch (emon::SamplerStopped &ex) {
            // the sampler crashed or exited...
            err_msg=!err_msg;
            MR_ERROR (EMONSamplerStopped (ERS_HERE, ex));
          
            variables.set_try2reconnect (nullptr == ::getenv("TDAQ_GNAM_NO_RECONNECT"));
          
            time_t t0=time(NULL);
            while(time(NULL)-t0<10){
                usleep (100000);
            }
          
            continue;
        }
        catch (SamplerNotFound &ex) {
            // reconnection failed
            if(err_msg){
                err_msg=!err_msg;
                ers::error(ex);
                //MR_ERROR ("We will try and reconnect later");
            }else{
                MR_DEBUG (1, ex << "; will try and reconnect later");
            }
          
            variables.set_try2reconnect (true);
          
            time_t t0=time(NULL);
            while(time(NULL)-t0<10){
                usleep (100000);
            }
          
            continue;
        }

        // process the event
        MR_DEBUG (3, "got an event");
        unsigned long size = event.size( );
        const uint32_t *data = (const uint32_t *) event.data();
        
        //++ (variables.get_eventsinrun());
        //++ (variables.get_eventstotal());
        variables.one_more_event();
        
        MR_DEBUG (2, "Events total: " << variables.get_eventstotal() << ";"
                  " events in run: " << variables.get_eventsinrun() << ";"
                  " event size: " << size);
        
        if (variables.get_eventsinrun() % 1000 == 0) {
            MR_DEBUG (0, "eventsinrun: " << variables.get_eventsinrun());
        } else if (variables.get_eventsinrun() % 100 == 0) {
            MR_DEBUG (1, "eventsinrun: " << variables.get_eventsinrun());
        }
    
        std::vector<const uint32_t *> rods; 
        std::vector<unsigned long int> sizes;
        
        // Check if data is a FullEvent fragment, latest version, that could be compressed
        // In the case, decompress it
        eformat::Header h(&data[0]);
        std::unique_ptr<eformat::read::FullEventFragment> f;
        if (h.marker() == eformat::FULL_EVENT and h.version() == eformat::DEFAULT_VERSION)
        {
          try 
          {
            f.reset(new eformat::read::FullEventFragment(&data[0]));
            f->check_tree(); 
	  }
	  catch (eformat::Issue &ex) {
            MR_WARNING (DataDecodingIssue (ERS_HERE, ex));
            continue;
          }
          catch (ers::Issue &ex) {
            MR_WARNING (DataDecodingIssue (ERS_HERE, ex));
            continue;
          }

          // Dump info for the 1st five events for debugging purpose
          static int cnt=0;
          if(cnt < 5)
          {
            std::vector<eformat::read::ROBFragment> robs;
            f->robs(robs);
            std::ostringstream o;
            o << "; #ROBs = " << robs.size();
            if (cnt==0)
            {
              o << ": \n" << std::hex;
              for(auto r : robs)
                o << "0X" << r.rob_source_id() << ", ";
            }    
            ERS_LOG("FullEvent sz(compr/uncompr) = " 
                << f->payload_size_word() << "/" 
                << f->readable_payload_size_word()  
                << o.str() );
            cnt++;    
          }
          try {
            getrods(f->readable_payload(),
                    f->readable_payload_size_word(),
                    rods, sizes);
          } 
          catch (const eformat::WrongUncompressedSizeIssue &ex) {
            MR_WARNING (FullEventFragmentReadIssue (ERS_HERE, ex));
            continue;
          }
          catch (const eformat::CompressionIssue &ex) {
            MR_WARNING (FullEventFragmentReadIssue (ERS_HERE, ex));
            continue;
          }
          catch (ers::Issue &ex) {
            MR_WARNING (FullEventFragmentReadIssue (ERS_HERE, ex));
            continue;
          }
        }
        else
        {
          // ERS_LOG("Not a FullEvent or Fullevent from a previous eformat version");
          getrods (data, size, rods, sizes);
        }
        if (rods.size() != 0) {
            for (size_t k = 0; k < dynalibs.size (); k++) {
                DLEXEC_VOID_4 (true, dynalibs[k], decode, &rods,
                               &sizes, data, size);
            }
            for (size_t k = 0; k < dynalibs.size (); k++) {
                DLEXEC_VOID_0 (true, dynalibs[k], fillHisto);
            }
        }
    }
}

