//$Id$

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "gnam/gnamutils/GnamPAO.h"
#include "gnam/gnamutils/GnamMessRep.h"
#define MR_LOG(mess) do { \
    ERS_LOG (mess); \
} while (false) 

#include "gnam/gnam/histos.h"
#include "gnam/gnam/GnamException.h"
#include "traceSegv.h"

#include <TFile.h>
#include <TClass.h>

#include <sstream>
#include <vector>
#include <map>

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <pthread.h>

using namespace daq::gnam;
using namespace daq::gnamlib;

#define MAX_HNAME_LEN ((size_t) 1023)

void
histo_reset (GnamHisto *histo, OHRootProvider *provider)
{
    MR_DEBUG (2, "resetting histogram: " << histo->GetName());
    histo->Reset();
    histo_publish (histo, provider);
}

void
histo_reset_all ()
{
    GnamHisto::resetAll();
}

#define GET_NUMBER_INT( var, arg) { \
    var = (Int_t) strtol (arg, &first_invalid, 10); \
    MR_DEBUG (3, "Setting " #var); \
    if (*first_invalid) { \
        std::ostringstream buf; \
        buf << "invalid argument: " << arg; \
        ers::error (BadCommand  (ERS_HERE, "rebin", buf.str())); \
        return; \
    } \
}

#define GET_NUMBER_FLOAT( var, arg) { \
    var = strtod (arg, &first_invalid); \
    MR_DEBUG (3, "Setting " #var); \
    if (*first_invalid) { \
        std::ostringstream buf; \
        buf << "invalid argument: " << arg; \
        ers::error (BadCommand  (ERS_HERE, "rebin", buf.str())); \
        return; \
    } \
}

void
histo_rebin (GnamHisto *histo, OHRootProvider *provider,
             int argc, const char * const *argv)
{
    char *first_invalid;
    Int_t nXbin = 0, nYbin = 0;
    Axis_t Xmin = 0.0, Xmax = 0.0, Ymin = 0.0, Ymax = 0.0;
    MR_DEBUG (1, "rebinning histogram: " << histo->GetName());
    if (argc < 3) {
        std::ostringstream buf;
        buf << "3 arguments required, only " << argc << " received";
        MR_ERROR (BadCommand (ERS_HERE, "rebin", buf.str().c_str()));
        return;
    }
  
    GET_NUMBER_INT (nXbin, argv[0]);
    GET_NUMBER_FLOAT (Xmin, argv[1]);
    GET_NUMBER_FLOAT (Xmax, argv[2]);

    if (!histo->IsA()->InheritsFrom("TH2")) {
        MR_DEBUG (3, "SetBins (" << nXbin << ", " << Xmin << ", " << Xmax);
        histo->SetBins (nXbin, Xmin, Xmax);
        // histo_publish (histo, provider);
        histo_reset (histo, provider);
        return;
    }
  
    if (argc < 6) {
        std::ostringstream buf;
        buf << "6 arguments required, only " << argc << " received";
        ers::error (BadCommand
                    (ERS_HERE, "rebin", buf.str().c_str()));
        return;
    }

    GET_NUMBER_INT (nYbin, argv[3]);
    GET_NUMBER_FLOAT (Ymin, argv[4]);
    GET_NUMBER_FLOAT (Ymax, argv[5]);
  
    MR_DEBUG (3, "SetBins (" << nXbin << ", " << Xmin << ", " << Xmax << ", "
              << nYbin << ", " << Ymin << ", " << Ymax);
    histo->SetBins (nXbin, Xmin, Xmax, nYbin, Ymin, Ymax);
    // histo_publish (histo, provider);
    histo_reset (histo, provider);
}

void
histo_dump_move (const char *filename, int run, bool first)
{
    char oldname[MAX_HNAME_LEN];
    char newname[MAX_HNAME_LEN];
    char bckname[MAX_HNAME_LEN];
    int tmp;
    struct stat fileinfo;

    snprintf (oldname, MAX_HNAME_LEN, "%s_%d.writing", filename, run);
    snprintf (newname, MAX_HNAME_LEN, "%s_%d.root", filename, run);
    if (first) {
        if (lstat (newname, &fileinfo) == 0) {
            snprintf (bckname, MAX_HNAME_LEN, "%s_%d.root~", filename, run);
            MR_WARNING (ROOTFileExists (ERS_HERE, newname, bckname));
            tmp = rename (newname, bckname);
            if (tmp != 0) {
                std::ostringstream buf;
                buf << "renaming `" << newname << "' to `" << bckname << "'";
                MR_ERROR (POSIXError (ERS_HERE, "rename", errno, buf.str()));
            }
        }
    } else {
        MR_DEBUG (1, "renaming `" << oldname << "' to `" << newname << "'");
        tmp = rename (oldname, newname);
        if (tmp != 0) {
            std::ostringstream buf;
            buf << "renaming `" << oldname << "' to `" << newname << "'";
            ers::error (POSIXError (ERS_HERE, "rename", errno,
                                    buf.str().c_str()));
        }
    }
}

class dumpMutex {
private:
    pthread_mutex_t m_mutex;
    char m_tname[MAX_HNAME_LEN];
    char m_fname[MAX_HNAME_LEN];

public:
    dumpMutex() { pthread_mutex_init ( &m_mutex, NULL ); }
    ~dumpMutex() { pthread_mutex_destroy ( &m_mutex ); }

    int trylock() { return pthread_mutex_trylock ( &m_mutex ); }
    int unlock() { return pthread_mutex_unlock ( &m_mutex ); }

    void names (const char *filename, int run ) {
        snprintf (m_tname, MAX_HNAME_LEN, "%s_%d.writing", filename, run);
        snprintf (m_fname, MAX_HNAME_LEN, "%s_%d.root", filename, run);
    }
    char *tname() { return m_tname; }
    char *fname() { return m_fname; }
};
  
unsigned long msleep ( unsigned long msec )
 {
  struct timespec treq, trem;
  treq.tv_sec = time_t(msec/1000);
  treq.tv_nsec = (msec%1000)*1000000L;
  if ( nanosleep ( &treq, &trem ) != 0 )
   {
    return trem.tv_sec*1000+trem.tv_nsec/1000000L;
   }
  return 0;
 }
 
void
histo_dump_move (dumpMutex &dmutex)
{
    char *oldname = dmutex.tname();
    char *newname = dmutex.fname();
    int tmp;

    MR_DEBUG (1, "renaming `" << oldname << "' to `" << newname << "'");
    tmp = rename (oldname, newname);
    if (tmp != 0) {
        std::ostringstream buf;
        buf << "renaming `" << oldname << "' to `" << newname << "'";
        ers::error (POSIXError (ERS_HERE, "rename", errno,
                                buf.str().c_str()));
    }
}

static dumpMutex dmutex;

static bool nowClone(true);

void
histo_dump (const char *filename, int run)
{
    msleep(1500);
    MR_LOG("histo_dump ************ updating root file with " << (nowClone ? "Clone" : "SetName"));

    dmutex.names (filename, run);
    MR_DEBUG (1, "dumping all histograms into file " << dmutex.tname());

    TFile *f;
  
    char *tname = dmutex.tname();
    MR_DEBUG (1, "dumping all histograms into file " << tname);
    std::string ss;
    ss = "write_file ";
    ss += tname;
    setMess(ss.c_str());

    f = new TFile (tname, "RECREATE", "GNAM dump file", 2);
    static bool last_opened = true;
    if (f->IsOpen()) {
        gnamMap_t & gnamHistoMap = GnamHisto::getHistoMap();
        gnamMap_t::iterator ghit;
        GnamHisto::flushAll();
        last_opened = true;
        GnamHisto *ghisto;
        TH1 *rhisto;
        TObject *whisto;
        TObject *obj;
        char name[MAX_HNAME_LEN];
        char buf[MAX_HNAME_LEN];
        char *dir, *slash;
        for ( ghit=gnamHistoMap.begin(); ghit!=gnamHistoMap.end(); ghit++ ) {
            ghisto = (*ghit).second;
            const char *hnm = ghisto->GetName();
            ss = "looping ";;
            ss += hnm;
            setMess(ss.c_str());
            if (ghisto->IsDumped()) {
                rhisto = ghisto->gTH1();
                name[0] = 0;
                strncat (name, hnm, MAX_HNAME_LEN - 1);
                memcpy (buf, name, MAX_HNAME_LEN);
                dir = buf;
                f->cd ();
                while ((slash = index (dir, '/'))) {
                    *slash = 0;
                    obj = gDirectory->Get (dir);
                    if (!obj) {
                        gDirectory->mkdir (dir, dir);
                    }
                    gDirectory->Cd (dir);
                    dir = slash + 1;
                }
                // now dir is the basename
                ss = "writing ";
                ss += hnm;
                setMess(ss.c_str());
                if (nowClone) {
                    whisto = rhisto->Clone (dir);
                    whisto->Write();
                    delete whisto;
                } else {
                    rhisto->SetName (dir);
                    rhisto->Write();
                    rhisto->SetName (name);
                }
            }
        }
        ss = "writing all ";
        setMess(ss.c_str());
        f->Write ();
        ss = "closing file ";
        setMess(ss.c_str());
        f->Close ();
        MR_DEBUG (3, "done");
    } else {
        if (last_opened) {
            ers::error (BadROOTFileName (ERS_HERE, tname));
            last_opened = false;
        }
    }
    ss = "deleting file pointer ";
    setMess(ss.c_str());
    delete f;

    ss = "moving file ";
    setMess(ss.c_str());
    histo_dump_move (dmutex);
    // nowClone = !nowClone;

    MR_LOG("thread " << pthread_self() << ": root file " << dmutex.fname()
            << " updated");
}

unsigned int
histo_publish1 (GnamHisto *histo, OHRootProvider *provider)
// publishes a single histo
{
    int tmp;
    unsigned int pub_chan = 0;
    tmp = histo->Publish (provider, &pub_chan);
    if (tmp == 0) {
        /*if (histo->IsA()->InheritsFrom("TH2")) {
          pub_chan = (histo->GetNbinsX() * histo->GetNbinsY());
          } else {
          pub_chan = histo->GetNbinsX();
          }*/
        MR_DEBUG (2, "published histo " << histo->GetName()
                  << " (" << pub_chan << " channels)");
    } else if (tmp == 5) {
        MR_DEBUG (2, "histo " << histo->GetName() << " not published"
                  " because not updated since last publication");
    }
    return pub_chan;
}

unsigned int
histo_publish (GnamHisto *histo, OHRootProvider *provider)
// publishes a single histo or a complete PAO (is the histo belongs to a PAO)
{
    unsigned int pub_chan = 0;
    GnamPAO *pao = histo->GetPAO();
    if (pao == NULL) {
        return histo_publish1 (histo, provider);
    }
    const std::vector<GnamHisto *> &pao_list = PAOGetHistos (pao);
    for (size_t k = 0; k < pao_list.size(); k++) {
        pub_chan += histo_publish1 (pao_list[k], provider);
    }
    return pub_chan;
}

void
histo_set_publish_all ()
{
    GnamHisto::setPublishAll();
}

unsigned int
histo_publish_all (OHRootProvider *provider, unsigned int n_chan)
{
    MR_DEBUG (1, "entering histo_publish_all, n_chan " << n_chan);

    if (provider == 0) {
        MR_DEBUG (1, "null provider");
        return 0;
    }

    if (! GnamHisto::available2publish()) {
        MR_DEBUG (1, "empty list");
        return 0;
    }

    // number of channels published during this function run
    unsigned int pub_chan = 0;
    unsigned int pub_histo = 0;
    while (GnamHisto::available2publish()) {
        GnamHisto *histo = GnamHisto::next2publish();
        // publish histogram and update pub_chan
        pub_chan += histo_publish (histo, provider);
        pub_histo ++;
        if (n_chan && (pub_chan >= n_chan)) break;
    }
    /*
    if (n_chan == 0) {
        pub_chan += GnamHisto::publishAll (provider);
    } else while ((pub_chan < n_chan) && (GnamHisto::available2publish())) {
        GnamHisto *histo = GnamHisto::next2publish();
        // publish histogram and update pub_chan
        pub_chan += histo_publish (histo, provider);
        pub_histo ++;
    }
    */
    MR_DEBUG (3, "n_chan " << n_chan << ", pub_chan " << pub_chan
              << ", pub_histo " << pub_histo);
    return pub_chan;
}

GnamHisto *
histo_name2histo (const char *name)
{
    return GnamHisto::name2histo (name);
}

void
histo_clear (const std::vector <struct dynalib> &dynalibs)
{
    std::map <uint32_t, uint32_t> tobecleaned;
    std::map <uint32_t, uint32_t>::iterator it;   

    GnamHisto::cleanUp (tobecleaned);
    for( it = tobecleaned.begin(); it != tobecleaned.end(); it++ )
    {
        MR_WARNING (HistoMemoryLeak (ERS_HERE, it->second,
                                     dynalibs[it->first].name));
    }
}

