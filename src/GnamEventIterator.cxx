// $Id$

#include "gnam/gnam/GnamEventIterator.h"
#include "gnam/gnamutils/GnamMessRep.h"
#include <stdlib.h>
#include <ostream>

using namespace daq::gnam;
using namespace daq::gnamlib;

/*
 * gnamEventIterator
 */

gnamEventIterator::gnamEventIterator (const IPCPartition &partition_in,
                                      const emon::dal::SamplingParameters 
				       * Sampler)
                                      
    : m_iter (NULL),
      partition (partition_in),
      sampler (Sampler)
{
}

void
gnamEventIterator::connect ()
{
    ERS_PRECONDITION (m_iter == NULL);
    MR_DEBUG (0, "connecting to the sampler");
    try{
      m_iter = new emon::EventIterator(partition, *sampler);
    }catch(const emon::Exception& ex){
      throw SamplerNotFound (ERS_HERE, this->dumpsampler(), ex);
    }
}

void
gnamEventIterator::disconnect ()
{
    if (m_iter != NULL) {
        MR_DEBUG (0, "disconnecting from sampler");
        delete m_iter;
        m_iter = NULL;
    }
}

gnamEventIterator::~gnamEventIterator ()
{
    disconnect();
}

std::string
gnamEventIterator::dumpsampler () const
{
  std::ostringstream buf;
  buf << "\n  Type: \"" << sampler->get_SamplerType() << "\"";
  
  const std::vector<std::string> names = sampler->get_SamplerNames();
  for (size_t k = 0; k < names.size(); ++k) {
    buf << "\n  Names[" << k << "]: '"  << names[k] << "'";
  }

  buf << "\n NumberOfSampler: " << sampler->get_NumberOfSamplers();

  buf << "\n  lvl1trigtype: " << sampler->get_L1TriggerType();
  buf << "\n  lvl1trigtype_ingore: " << (sampler->get_L1TriggerType_Ignore() ? "true": "false");

  const std::vector<uint16_t> l1bits = sampler->get_L1Bits();

  for (size_t k = 0; k < l1bits.size(); ++k) {
    buf << "\n  L1Bits[" << k << "]: "  << l1bits[k] << "";
  }
  
  buf << "\n  L1Bits_Logic: " << sampler->get_L1Bits_Logic();
  buf << "\n  L1Bits_Origin: " << sampler->get_L1Bits_Origin();
  
  buf << "\n  statusword: " << sampler->get_StatusWord();
  buf << "\n  statusword_ignore: " << (sampler->get_StatusWord_Ignore() ? "true": "false");
  
  buf << "\n  StreamType: " << sampler->get_StreamType();

  const std::vector<std::string> s_names = sampler->get_StreamNames();
  for (size_t k = 0; k < s_names.size(); ++k) {
    buf << "\n  StreamNames[" << k << "]: '"  << s_names[k] << "'";
  }
  
  buf << "\n  Stream_Logic: " << sampler->get_Stream_Logic();

  buf << "\n BufferSize: " << sampler->get_BufferSize();

  buf << "\n Group Name: " << sampler->get_GroupName();

  return buf.str();
}
