//$Id$

#include <vector>
#include <bits/types.h>

#include "gnam/gnamutils/GnamMessRep.h"
#include "gnam/gnam/getrods.h"

#define ROBMARKER 0xdd1234dd
#define RODMARKER 0xee1234ee

int getrods(const uint32_t * data, unsigned long int size,
            std::vector<const uint32_t *> &rods,
            std::vector<unsigned long int> &sizes)
{
    unsigned long int j;
    unsigned int nrods = 0;

    for (j = 0; j < size; j++) {
        if (data[j] == ROBMARKER) {
	    unsigned long int fragsize = data[j + 1];
            unsigned long int headsize = data[j + 2];
            unsigned long int nstatusw = data[j + 5];
            unsigned long int trailerpresent = data[j + nstatusw + 6 ];
            unsigned long int rodsize;
            unsigned long int rodindex;
            MR_DEBUG (3, "ROB FOUND " << j << " " << fragsize
                      << " " << headsize);
            rodindex = j + headsize;

            if(trailerpresent)
                rodsize = fragsize - headsize - 1;
            else
                rodsize = fragsize - headsize;
            
            MR_DEBUG (3, "rodindex " << rodindex);

            if (data[rodindex] == RODMARKER) {
	        rods.push_back(&data[rodindex]);
                sizes.push_back(rodsize);
                nrods++;
                MR_DEBUG (3, "ROD FOUND " << rodindex << " " << rodsize);
            }
            j += fragsize - 1;
        } else {
            if (data[j] == RODMARKER) {
                nrods++;
                // single rod event
                rods.push_back(&data[j]);
                sizes.push_back(size - j);
                break;
            }
        }
    }

    return nrods;
}
