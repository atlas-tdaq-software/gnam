//$Id$

#include "gnam/gnamutils/GnamMessRep.h"
#define MR_LOG(mess) do { \
    ERS_LOG (mess); \
} while (false) 

#include "gnam/gnam/GnamException.h"
#include "gnam/gnam/Gnam.h"

#include "RunControl/ItemCtrl/ItemCtrl.h"
#include "RunControl/ItemCtrl/ControllableDispatcher.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Controllable.h"
#include "RunControl/Common/Exceptions.h"
#include <ipc/core.h>

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

using namespace daq::gnam;
using namespace daq::gnamlib;

static int
main_actual (int argc, char **argv)
{
    MR_LOG ("parsing on-line arguments:");
    GnamVariables variables (argc, argv);

    MR_LOG ("pid = " << getpid());
  
    MR_LOG ("creating new Gnam instance for: "
             << variables.get_processname());

    if(variables.get_isinteractive())
        MR_DEBUG (0, "Starting LocalController client in interactive mode" );
    else
        MR_DEBUG (0, "Starting LocalController client in controlled mode" );

    std::string name = variables.get_processname();
    std::string parent = variables.get_parent();
    std::string segment = variables.get_segment();
    std::string partition = variables.get_partition();
    daq::rc::ItemCtrl myItem (name, parent, segment, partition, std::shared_ptr<daq::rc::Controllable>(new Gnam(variables)), variables.get_isinteractive());
    myItem.init(); 
    
    MR_DEBUG (0, "Starting program job" );
    myItem.run(); 

    MR_DEBUG (0, "Program terminating gracefully!" );
    return 0;
}

int
main (int argc, char **argv)
{
    try {
        return main_actual (argc, argv);
    }
    catch (const std::exception &ex) {
        FatalError (ex);
    }
    catch (...) {
        FatalError (InternalError (ERS_HERE, "unknown exception caught"));
    }
}
