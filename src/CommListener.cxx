// $Id$

#include "gnam/gnam/CommListener.h"
#include "gnam/gnamutils/GnamMessRep.h"

#include <boost/thread/mutex.hpp>

#include <queue> 

#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

using namespace daq::gnam;

/*
 * Split
 */

void
Split (const std::string &String, const char &Delimiters,
       std::vector<std::string> &Tokens)
    noexcept
{
    Tokens.clear();
    const size_t len = String.size();
    size_t start = 0;
    do {
        size_t delim = String.find_first_of (Delimiters, start);
        if (delim == std::string::npos) {
            Tokens.push_back (String.substr (start));
            return;
        } else {
            if (delim > start) {
                Tokens.push_back (String.substr (start, delim - start));
            }
            start = delim + 1;
        }
    } while (start < len);
}

/*
 * CommQueueEntry
 */

void
CommQueueEntry::clear ()
    noexcept
{
    hname_str.clear();
    command.clear();
    buf.clear();
    hname = NULL;
    cmd = -1;
    argc = -1;
    delete[] argv;
    argv = NULL;
}    

void
CommQueueEntry::init (const std::string &HistoName, const std::string &Command)
{
    // reset
    clear();

    // fundamental variables (used in copy-ctor and assignment operator
    hname_str = HistoName;
    command = Command;

    if (command.empty()) {
        throw BadCommand (ERS_HERE, "EMPTY COMMAND");
    }
  
    if (hname_str.empty()) {
        MR_DEBUG (1, "received global command: " << command);
    } else {
        MR_DEBUG (1, "histo " << HistoName << ": received command: "
                  << command);
    }
  
    // split command
    const char sep = '|';
    Split (command, sep, buf);
    ERS_ASSERT (buf.size() > 0);
  
    /*
      fill entry
    */
    // histo name
    hname = hname_str.c_str();
    // command
    const char *comm = buf[0].c_str();
    if (strcasecmp (comm, GNAM_COMM_RESET_STR.c_str()) == 0) {
        cmd = GNAM_COMM_RESET;
    } else if (strcasecmp (comm, GNAM_COMM_UPDATE_STR.c_str()) == 0) {
        cmd = GNAM_COMM_UPDATE;
    } else if (strcasecmp (comm, GNAM_COMM_REBIN_STR.c_str()) == 0) {
        cmd = GNAM_COMM_REBIN;
    } else if (strcasecmp (comm, GNAM_COMM_CUSTOM_STR.c_str()) == 0) {
        cmd = GNAM_COMM_CUSTOM;
    } else if (strcasecmp (comm, GNAM_COMM_PUBLISHED_STR.c_str()) == 0) {
        cmd = GNAM_COMM_PUBLISHED;
    } else if (strcasecmp (comm, GNAM_COMM_FILLED_STR.c_str()) == 0) {
        cmd = GNAM_COMM_FILLED;
    } else if (strcasecmp (comm, GNAM_COMM_DUMPED_STR.c_str()) == 0) {
        cmd = GNAM_COMM_DUMPED;
    } else if (strcasecmp (comm, GNAM_COMM_NEVER_RESET_STR.c_str()) == 0) {
        cmd = GNAM_COMM_NEVER_RESET;
    } else if (strcasecmp (comm, GNAM_COMM_RESET_AT_WASTA_STR.c_str()) == 0) {
        cmd = GNAM_COMM_RESET_AT_WASTA;
    } else if (strcasecmp (comm, GNAM_COMM_RESET_AT_WASTO_STR.c_str()) == 0) {
        cmd = GNAM_COMM_RESET_AT_WASTO;
    } else if (strcasecmp (comm, GNAM_COMM_RESET_AT_WASS_STR.c_str()) == 0) {
        cmd = GNAM_COMM_RESET_AT_WASS;
    } else if (strcasecmp (comm, GNAM_COMM_FILL_ANYTIME_STR.c_str()) == 0) {
        cmd = GNAM_COMM_FILL_ANYTIME;
    } else if (strcasecmp (comm, GNAM_COMM_FILL_WHEN_READY_STR.c_str()) == 0) {
        cmd = GNAM_COMM_FILL_WHEN_READY;
    } else if (strcasecmp (comm, GNAM_COMM_FILL_WHEN_STANDBY_STR.c_str()) == 0) {
        cmd = GNAM_COMM_FILL_WHEN_STANDBY;
    } else if (strcasecmp (comm, GNAM_COMM_NEVER_FILL_STR.c_str()) == 0) {
        cmd = GNAM_COMM_NEVER_FILL;
    } else if (strcasecmp (comm, GNAM_COMM_START_FILLING_RESET_WHEN_READY_STR.c_str()) == 0) {
        cmd = GNAM_COMM_START_FILLING_RESET_WHEN_READY;
    } else if (strcasecmp (comm, GNAM_COMM_WASTA_STR.c_str()) == 0) {
        cmd = GNAM_COMM_WASTA;
    } else if (strcasecmp (comm, GNAM_COMM_WASTO_STR.c_str()) == 0) {
        cmd = GNAM_COMM_WASTO;
    } else {
        throw BadCommand (ERS_HERE, comm);
        return;
    }
    // optional arguments
    argv = new const char * [buf.size()];
    argc = int(buf.size()-1);
    for (unsigned int k = 1; k < buf.size(); ++ k) {
        argv[k - 1] = buf[k].c_str();
    }
    argv[argc] = NULL;
}


CommQueueEntry::CommQueueEntry ()
    noexcept
    : argv (NULL)
{
    clear();
}

CommQueueEntry::CommQueueEntry (const std::string &HistoName,
                                const std::string &Command)
    : argv (NULL)
{
    init (HistoName, Command);
}

CommQueueEntry::CommQueueEntry (const CommQueueEntry &Source)
    noexcept
    : argv (NULL)
{
    init (Source.hname_str, Source.command);
    ERS_ASSERT (argc == Source.argc);
}

CommQueueEntry &
CommQueueEntry::operator= (const CommQueueEntry &Source)
    noexcept
{
    if (this == &Source) {
        return *this;
    }
    init (Source.hname_str, Source.command);
    ERS_ASSERT (argc == Source.argc);
    return *this;
}

CommQueueEntry::~CommQueueEntry ()
    noexcept
{
    clear();
}

/*
 * Commands queue management
 */

static std::queue <CommQueueEntry> CommQueueEntries;
static boost::mutex queue_mutex;

static void CommQueuePush (const std::string &hname, const std::string &cmd)
{
    try {
        CommQueueEntry entry (hname, cmd);
        boost::mutex::scoped_lock lock (queue_mutex);
        CommQueueEntries.push (entry);
    }
    catch (const BadCommand &ex) {
        MR_WARNING (ex);
    }
}

CommQueueEntry *
daq::gnam::CommQueuePop (void)
{
    static CommQueueEntry entry;
    // get entry from the list
    boost::mutex::scoped_lock lock (queue_mutex);
    if (CommQueueEntries.empty()) {
        return NULL;
    } else {
        entry = CommQueueEntries.front();
        CommQueueEntries.pop();
        return &entry;
    }
}

/*
 * CommListener
 */

void CommListener::command (const std::string &hname, const std::string &cmd)
{
    CommQueuePush (hname, cmd);
}

void CommListener::command (const std::string &cmd)
{
    CommQueuePush ("", cmd);
}

