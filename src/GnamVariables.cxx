// $Id$

#include "gnam/gnam/GnamVariables.h"
#include "gnam/gnam/GnamException.h"
#include "gnam/gnamutils/GnamMessRep.h"

#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <libgen.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <cstdio>

#include <vector>
#include <string>

#include "RunControl/Common/CmdLineParser.h"
#include "RunControl/Common/Exceptions.h"

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

using namespace daq::gnam;
namespace fs = boost::filesystem;
namespace po = boost::program_options;

bool checkAndCreatePath(const fs::path & path){

  fs::path fullpath = fs::system_complete(path);

  if(!fs::exists(fullpath)){

    if(checkAndCreatePath(fullpath.parent_path())){
      std::string tmp = fullpath.string();
      ::mkdir(tmp.c_str(),0777);
    }
  }
  
  return (fs::exists(fullpath) && fs::is_directory(fullpath));
}

// constructor with null values
GnamVariables::GnamVariables (int argc, const char * const *argv)
    : // cmdline
    isinteractive (false),
    providername (processname),
    // database
    servername ("Histogramming"),
    outfile ("/tmp/gnam-out"),
    updatefr_evt (1000),
    updatefr_sec (10),
    dump (true),
    dump_fr_evt (50000),
    chan2oh (20000),
    start_to (5),
    thread_to (60),
    // sampler
    // internal
    runnumber (0),
    runstate (false),
    eventstotal (0),
    eventsinrun (0),
    try2reconnect (false)
{
    /*
     * get partition from environment
     */

    const char *part = getenv ("TDAQ_PARTITION");
    if (part == NULL || *part == 0) {
        throw daq::gnam::NoPartition (ERS_HERE);
    } else {
        partition = part;
    }

    /*
     * command line parameters
     */
    po::options_description desc("GNAM Core - Online Monitoring Task.");
    try {
        // Parser for the command line options required by the Run Control framework
        // Note that the "help" functionality is enabled
        daq::rc::CmdLineParser cmdParser(argc, (char**)argv, true);

        /*
         * fill the variables from command line
         */
        isinteractive = cmdParser.interactiveMode();
        processname = cmdParser.applicationName();
        parent = cmdParser.parentName();
        segment = cmdParser.segmentName();

        // check processname, as it is used as OHS provider name
        if (processname.length() == 0
            || processname.find ('.') != std::string::npos)
        {
            throw daq::gnam::InvalidProcessName (ERS_HERE, processname);
        }
    }
    catch(daq::rc::CmdLineHelp& ex) {
        // Show the help message: note that both messages from the CmdLineParser and the specific parser are reported
        std::cout << ex.message() << std::endl;
	throw;
    }
    catch(ers::Issue& ex) {
        // Any exception thrown during the ItemCtrl construction or initialization is a fatal error
        ers::fatal(ex);
	throw;
    }

}

GnamVariables::~GnamVariables ()
{
}

GnamVariables &
GnamVariables::operator= (const gnamdal::GnamApplication *GnamDB)
    noexcept
{
    servername = GnamDB->get_OHServerName();
    outfile = GnamDB->get_RootOutputFile();
    updatefr_evt = GnamDB->get_UpdateFreqEvt();
    updatefr_sec = GnamDB->get_UpdateFreqSec();
    dump = GnamDB->get_DumpEnabled();
    dump_fr_evt = GnamDB->get_DumpFreqEvt();
    chan2oh = GnamDB->get_ChannelsToPublish();
    start_to = GnamDB->get_SamplerContactTimeout();
    thread_to = GnamDB->get_EventThreadExitTimeout();

    fs::path p = fs::system_complete(fs::path(outfile));
    if(p.has_filename()) p = p.parent_path();

    try{
      if(!checkAndCreatePath(p)){
	ers::error(BadROOTFileName(ERS_HERE, outfile.c_str()));
      }
    }catch(fs::filesystem_error& ex){
      ers::error(BadROOTFileName(ERS_HERE, outfile.c_str(),ex));
    }

    return *this;
}

std::string
GnamVariables::Dump ()
    noexcept
{
    std::ostringstream buf;

    buf << "\n  partition = \"" << partition << "\n  \"";

    buf << "\n  isinteractive = " << (isinteractive ? "true" : "false");
    buf << "\n  parent = \"" << parent << "\n  \"";
    buf << "\n  segment = \"" << segment << "\n  \"";
    buf << "\n  processname = \"" << processname << "\n  \"";
    buf << "\n  providername = \"" << providername << "\n  \"";

    buf << "\n  servername = \"" << servername << "\n  \"";
    buf << "\n  outfile = \"" << outfile << "\n  \"";
    buf << "\n  updatefr_evt = " << updatefr_evt;
    buf << "\n  updatefr_sec = " << updatefr_sec;
    buf << "\n  dump = " << dump;
    buf << "\n  dump_fr_evt = " << dump_fr_evt;
    buf << "\n  chan2oh = " << chan2oh;
    buf << "\n  start_to = " << start_to;
    buf << "\n  thread_to = " << thread_to;

    return buf.str();
}

void
GnamVariables::new_run (int RunNumber, const std::string&RunType)
    noexcept
{
    runnumber = RunNumber;
    runtype = RunType;
}

void
GnamVariables::new_runst (bool st)
    noexcept
{
    runstate = st;
}

void
GnamVariables::reset_eventsinrun ()
    noexcept
{
    eventsinrun = 0;
}

