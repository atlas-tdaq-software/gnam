//$Id$

#include <dlfcn.h>
#include <time.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdint.h>
#include <ostream>
#include <exception>

#include <ipc/core.h>
#include <is/infodictionary.h>
#include <is/infoT.h> 
#include <rc/RunParams.h>
#include <ers/ers.h>

#include <RunControl/FSM/FSMStates.h>
#include <RunControl/FSM/FSMCommands.h>
#include <RunControl/Common/RunControlCommands.h>
#include <RunControl/Common/OnlineServices.h>

#include "gnam/gnam/Gnam.h"
#include "gnam/gnamutils/GnamMessRep.h"

#define MR_LOG(mess) ERS_LOG(mess)

#include "gnam/gnam/GnamException.h"
#include "gnam/gnam/histos.h"
#include "gnam/gnamutils/GnamHisto.h"
#include "gnam/gnamutils/GnamPAO.h"
#include "gnam/gnamutils/PAO-impl.h"
#include "gnam/gnamutils/Branches-core.h"
#include "gnam/gnamutils/dynalib_histo.h"

#include <config/Configuration.h>
#include <dal/util.h>
#include "gnamdal/GnamLibrary.h"

using namespace daq::gnam;
using namespace daq::gnamlib;

static std::vector <struct dynalib> dynalibs;

void dlopen_terminate()
{
    MR_DEBUG (0, "Caught an exception during dlopen");
  
    std::string msg="Caught an exception during dynamic"
        " library loading. Probably you throwed it from a global scope.";
  
    try{
        throw;
    }
    catch (ers::Issue &exc) { 
        LibraryUnusable issue (ERS_HERE, msg, exc);
        FatalError(issue);
    }
    catch (std::exception &exc) {
        LibraryUnusable issue (ERS_HERE, msg, exc);
        FatalError (issue);
    }
    catch (...) {
        LibraryUnusable issue (ERS_HERE, msg);
        FatalError (issue);
    }
}

static void
dynalibs_clear (void)
{
    int k;
    char *error;
    for (k = int(dynalibs.size()-1); k >= 0; k--) {
        dlerror();
        MR_DEBUG (0, "dlclose " << k << "-th library");
        dlclose (dynalibs[k].lib);
        error = dlerror();
        if (error) {
            MR_WARNING (dlcloseError (ERS_HERE, dynalibs[k].name, error));
        }
    }
    dynalibs.clear();
}

Gnam::Gnam (GnamVariables &Variables)
    : daq::rc::Controllable(),
      vars (Variables),
      gnamDB(NULL),
      provider (NULL),
      eventThread (NULL),
      lst (NULL),
      gnam_ev_it (NULL)
{
}
 
Gnam::~Gnam () noexcept
{
    if (eventThread != NULL) {
        eventThread->stopExecution();
        MR_DEBUG (0, "deleting eventThread");
        delete eventThread;

        for (size_t k = 0; k < dynalibs.size(); k++) {
            DLEXEC_VOID_0 (true, dynalibs[k], DFStopped);
            DLEXEC_VOID_0 (true, dynalibs[k], endOfRun);
        }
        dynalibs_clear();
    }

    if (provider != NULL) {
        MR_DEBUG (0, "deleting Online provider");
        delete provider;
    }
    if (lst != NULL) {
        MR_DEBUG (0, "deleting command listener");
        delete lst;
    }
    if (gnam_ev_it != NULL) {
        MR_DEBUG (0, "deleting Emon iterator");
        delete gnam_ev_it;
    }
}

typedef void (*ptvf) (void);
ptvf pto2ptf(void* p)                // dlsym breaks ISO C++ rules ...
{
    union { void* f; ptvf t; } u;
    u.f = p; return u.t;
}

void
Gnam::configure (const daq::rc::TransitionCmd&)
{
    unsigned int k;
    //int tmp;
    char * error;
    struct dynalib dlib;
    std::vector<const gnamdal::GnamLibrary *> libraries;
    std::vector<const gnamdal::GnamLibrary *> valid_libs;
    std::vector<int> valid_conf_count;
    std::vector<char **> valid_conf_args;
    unsigned int n_libs;
  
  
    MR_DEBUG (0,"configure");
  
    timertotal.start();
  
    //WARNING!!In interactive mode you must set the TDAQ_DB variable
    //
    //TDAQ_DB="oksconfig:daq/partitions/be_test.data.xml" 
    //use oks and file daq/partitions/be_test.data.xml
    //
    //TDAQ_DB="rdbconfig:RDB"
    //use rdb and server with name RDB
    auto & confDB = daq::rc::OnlineServices::instance().getConfiguration();
    if (!confDB.loaded()) {
        if (vars.get_isinteractive()) {
            MR_WARNING(MissingTDAQ_DB(ERS_HERE));
        }
        ConfDBNotFound issue(ERS_HERE);
        FatalError(issue);
    }

    //looks for us in the DB
    gnamDB
      = confDB.get<gnamdal::GnamApplication>(vars.get_processname(),true);
      
    if (gnamDB)
        MR_DEBUG (0, "Configuration object found");
    else {
        ConfObjNotFound issue(ERS_HERE,vars.get_processname());
        FatalError(issue);
    }
      
    vars = gnamDB;

    MR_DEBUG (0, "Configuration loaded: " << vars.Dump());
        
    //get libraries in our db object
    libraries=gnamDB->get_Libraries();
    
    n_libs=(unsigned int)libraries.size();
  
#define DLSYM(fun) { \
    dlib.fun = (fun ## _t) pto2ptf(dlsym (dlib.lib, # fun)); \
    if (dlerror()) dlib.fun = NULL; \
}
  
    for(k=0;k<n_libs;k++){
        dlib.usable = true;

        dlib.name=(char *)(libraries[k]->get_PathToFile()).c_str();

        MR_DEBUG (1, "k: " << k << " - name: " << dlib.name);
        MR_LOG ("loading library: " << dlib.name);
    
        // 3p libraries
        const std::vector<std::string> &tplibs
            = libraries[k]->get_ThirdPartyLibraries();
        for (std::vector<std::string>::const_iterator it = tplibs.begin();
             it != tplibs.end(); ++ it)
        {
            if (third_party_libs.find (*it) != third_party_libs.end()) {
                continue;
            }
            MR_LOG ("loading 3-party library: " << *it);
            ::dlerror ();
            void (*old_term)() = std::set_terminate(dlopen_terminate);
            ::dlopen (it->c_str(), RTLD_NOW | RTLD_GLOBAL);
            std::set_terminate(old_term);
            error=::dlerror();
            if (error) {
                DLOpenError issue(ERS_HERE,error,dlib.name);
                ers::error(issue);
            } else {
                third_party_libs.insert (*it);
            }
        }

        ::dlerror ();
        
        void (*old_term)() = std::set_terminate(dlopen_terminate);
        
        dynalib_histo::set ((unsigned int)dynalibs.size());
        dlib.lib = ::dlopen (dlib.name, RTLD_NOW);
        
        std::set_terminate(old_term);
        
        error=::dlerror();
        
        if (error) {
            DLOpenError issue(ERS_HERE,error,dlib.name);
            ers::error(issue);
        } else {
            //DLSYM (init);
            DLSYM (initDB);
            DLSYM (startOfRun);
            DLSYM (decode);
            DLSYM (fillHisto);
            DLSYM (prePublish);
            DLSYM (endOfRun);
            DLSYM (DFStopped);
            DLSYM (customCommand);
            DLSYM (end);
            DLSYM (warmStart);
            DLSYM (warmStop);
            dynalibs.push_back (dlib);
            valid_libs.push_back(libraries[k]);
        }
    }
  
    MR_DEBUG (0,"IPC partition: " << vars.get_partition());
  
    IPCPartition partition(vars.get_partition());
  
    if (!partition.isValid()) {
        dynalibs_clear ();
        InvalidPartition issue (ERS_HERE, vars.get_partition());
        FatalError(issue);
    }
  
    ISInfoDictionary dict(partition);

    try{
        ISInfoBool rSt;
        dict.getValue ("RunParams.Ready4Physics", rSt);
        bool s = rSt;
        vars.new_runst (s);
    } catch (daq::is::Exception &ex) {
        vars.new_runst (false);
    }
    GnamHisto::GnamHistoSetReady (vars.get_runstate());

    for (k = 0; k < dynalibs.size (); k++) {
        dynalib_histo::set (k);
        DLEXEC_VOID_2 (true, dynalibs[k], initDB, &confDB, valid_libs[k]);
    }
}

void
Gnam::connect (const daq::rc::TransitionCmd&)
{
    
    MR_DEBUG (0, "connect");
  
    if (lst == NULL) {
        MR_DEBUG (0, "Creating CommListener");
        lst = new CommListener();
    }
  
    MR_LOG ("OH server: " << vars.get_servername()
             << " - Provider name: " << vars.get_providername());
  
    if (provider == NULL) {
        try {
            provider = new OHRootProvider( IPCPartition(vars.get_partition()),
                    vars.get_servername(), vars.get_providername(), lst);
        } catch(daq::oh::Exception & ex) {
            MR_ERROR (OHSProvider (ERS_HERE, ex));
            provider=NULL;
        }
    }
}

void
Gnam::disconnect (const daq::rc::TransitionCmd&)
{

    MR_DEBUG (0,"disconnect");
  
    if (provider != NULL) {
        delete provider;
        provider = NULL;
    }

    ERS_ASSERT (lst);
    delete lst;
    lst = NULL;
}

void
Gnam::unconfigure (const daq::rc::TransitionCmd&)
{
    unsigned int k;

    MR_DEBUG (0,"unconfigure");
  
    // unload libraries
    for (k = 0; k < dynalibs.size (); k++) {
        dynalib_histo::set (k);
        DLEXEC_VOID_0 (false, dynalibs[k], end);
    }
    histo_clear (dynalibs);
    dynalibs_clear ();
  
    // reset SPIE
    spie_reset ();
    
    timertotal.stop();   
  
    MR_LOG ("Total events received: " << vars.get_eventstotal());
    MR_LOG ("Total CPU Time is: "
             << (timertotal.userTime() + timertotal.systemTime()));   
    MR_LOG ("Total Time is: " << timertotal.totalTime());
} 

 
void
Gnam::prepareForRun (const daq::rc::TransitionCmd&)
{
    unsigned int k;
    time_t t0, t1;

    RunParams params;

    MR_DEBUG (0,"prepareForRun");

    timerinrun.start();

    ERS_ASSERT (gnam_ev_it == NULL);
    gnam_ev_it = new gnamEventIterator (IPCPartition(vars.get_partition()),
            gnamDB->get_SamplerSpecification());

    time (&t0);
    while (true) {
        time (&t1);
        try {
            MR_DEBUG (0, "Connecting to the sampler ...");
            gnam_ev_it->connect ();
            vars.set_try2reconnect (false);
            MR_DEBUG (0, "Got a sampler!");
            break;
        }
        catch (SamplerNotFound &ex) {
            if ((t1 - t0) < vars.get_start_to()) {
                MR_DEBUG (0, ex.what());
            } else {
                MR_ERROR (ex);
                vars.set_try2reconnect (true);
                break;
            }
        }
        usleep (200000);
    }

    ISInfoDictionary dict(vars.get_partition());
    
    try{
        dict.getValue ("RunParams.RunParams",params);
        vars.new_run (params.run_number, params.run_type);
    } catch (daq::is::Exception &ex) {
        MR_WARNING (ISRunNumber (ERS_HERE, ex));
        vars.new_run (0, "");
    }

    MR_DEBUG (0,"Starting run: " << vars.get_runnumber());
        
    for (k = 0; k < dynalibs.size(); k++) {
        const gnamList_t * partialhistolist = NULL;
        /*
          MR_DEBUG (0, "bookHisto (%d-th library)", k);
          if (dynalibs[k].bookHisto != NULL) {
          dynalib_ind_set (k);
          partialhistolist = dynalibs[k].bookHisto ();
          dynalib_ind_reset ();
          MR_LOG ("Library `%s' booked %d histograms",
          dynalibs[k].name, partialhistolist->size());
          }
        */
        dynalib_histo::set (k);
        DLEXEC_2 (true, partialhistolist, dynalibs[k], startOfRun,
                  vars.get_runnumber(), vars.get_runtype()); 
        dynalib_histo::reset ();
        if (partialhistolist) {
            MR_DEBUG (0, "Library `" << dynalibs[k].name << "' booked "
                      << partialhistolist->size() << " histograms");
        } else {
            MR_DEBUG (0, "Library `" << dynalibs[k].name
                      << "' booked no histograms");
        }
    }

  /*
    histo_set_publish_all();

    // publish and dump histograms immediately
    #ifndef ERS_NO_DEBUG
    unsigned int channels =
    #endif
    histo_publish_all (provider, 0);
    MR_DEBUG (0, "published %u channels", channels);
  */
    
    if(vars.get_dump()){
        //Make sure the file does not exist, or in case, move it 
        histo_dump_move (vars.get_outfile().c_str(), vars.get_runnumber(),
                         true);
        //Do not dump at the start of run
        //histo_dump (vars.get_outfile().c_str(), vars.get_runnumber());
    }
      
    eventThread = new EventThread (vars, gnam_ev_it, dynalibs,
                                   provider);
    
    vars.reset_eventsinrun();
    eventThread->startExecution();

    histo_publish_all (provider, 0);
}


void
Gnam::stopRecording (const daq::rc::TransitionCmd&)
{
    MR_DEBUG (0,"stopSFO");
    if( eventThread != NULL)
     {
      eventThread->stopExecution();
      MR_DEBUG (0, "deleting eventThread");
      delete eventThread;
      eventThread = NULL;
     }
    
    GnamHisto::GnamHistoFitEnable (true);
    unsigned int k;
    for (k = 0; k < dynalibs.size (); k++) {
        dynalib_histo::set (k);
        DLEXEC_VOID_1 (true, dynalibs[k], prePublish, true);
    }
    GnamHisto::GnamHistoFitEnable (false);

    // dump the histograms
#ifndef ERS_NO_DEBUG
    unsigned int channels =
#endif
        histo_publish_all (provider, 0);
    MR_DEBUG (0, "published " << channels << " channels");
    
    if(vars.get_dump())
        histo_dump (vars.get_outfile().c_str(), vars.get_runnumber());

    // Reset internal publishing index
    // histo_publish_all (0, 0);
    
    for (k = 0; k < dynalibs.size(); k++) {
        dynalib_histo::set (k);
        DLEXEC_VOID_0 (true, dynalibs[k], endOfRun);
    }

    delete gnam_ev_it;
    gnam_ev_it = NULL;

    timerinrun.stop();

    MR_LOG ("Ending run: " << vars.get_runnumber());
    MR_LOG ("Events received: " << vars.get_eventsinrun());
    MR_LOG ("Run CPU Time is: "
             << (timerinrun.userTime () + timerinrun.systemTime ()));
    MR_LOG ("Total Run Time is: " << timerinrun.totalTime());
}

void 
Gnam::stopGathering(const daq::rc::TransitionCmd&)
{
    MR_DEBUG (0,"stop Gathering");
  
    for (unsigned int k = 0; k < dynalibs.size(); k++) {
        dynalib_histo::set (k);
        DLEXEC_VOID_0 (true, dynalibs[k], DFStopped);
    }
}

void 
Gnam::stopArchiving(const daq::rc::TransitionCmd&)
{
    MR_DEBUG (0,"stop Archiving");
}

void 
Gnam::onExit(daq::rc::FSM_STATE state) noexcept
{
}

void
Gnam::user(const daq::rc::UserCmd& cmd)
{
    // Forward the command to the CommListener member
    ERS_LOG("Received UserCmd = " << cmd.commandName() << " from RC; forwading it to OHListener ... ");
    lst->command(cmd.commandName());
}

