//$Id$

/* Global include file for Gnam libraries */

// GNAM: prototypes for the callbacks
#include <gnam/gnamutils/GnamDynalibs.h>

// GNAM: macros for accessing data from decoding and histograming routines
#include <gnam/gnamutils/GnamBranches.h>

// GNAM: histogram class
#include <gnam/gnamutils/GnamHisto.h>

/*
 * OBSOLETED
 *
 * GNAM: message reporting functions (MESS_REP and MESS_REP_DEB)
 * #include "gnam/gnamutils/GnamMessRep.h"
 *
 * OBSOLETED: USE ERS INSTEAD
 */
#include <ers/ers.h>

// GNAM: GnamLibrary abstract definition
#include <gnamdal/GnamLibrary.h>
