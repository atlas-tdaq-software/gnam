//$Id$

#ifndef GNAM_BRANCHES_HDR
#define GNAM_BRANCHES_HDR

#include <gnam/gnamutils/GnamLibException.h>

#include <ers/ers.h>

#include <string>

/*
  You should not call these functions directly from your library.
  Use only the macros GNAM_BRANCH_REGISTER and GNAM_BRANCH_FILL
*/
void spie_register (const std::string &branchname, const void *p2object); // throw (daq::gnamlib::AlreadyExistingBranch);
const void *spie_fill (const std::string &branchname); // throw (daq::gnamlib::NotExistingBranch);

#define GNAM_BRANCH_REGISTER( branchname, p2object) do { \
    spie_register ((branchname), (const void *) (p2object)); \
} while (0)

#define GNAM_BRANCH_FILL( branchname, class, p2object) do { \
    if (!p2object) { \
        p2object = (const class *) spie_fill (branchname); \
        ERS_ASSERT (p2object); \
    } \
} while (0)

#endif // #ifndef GNAM_BRANCHES_HDR

