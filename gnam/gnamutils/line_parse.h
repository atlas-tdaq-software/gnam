/* $Id$ */

#ifndef LINE_PARSE_HDR
#define LINE_PARSE_HDR

#include <stdlib.h>
#include <ctype.h>

int line_parse (char *string, ssize_t size, size_t *n_strings, size_t *n_chars,
                char **first_string);

# endif /* #ifndef LINE_PARSE_HDR */
