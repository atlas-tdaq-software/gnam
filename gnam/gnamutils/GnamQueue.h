#ifndef _Gnam_Queue_h
#define _Gnam_Queue_h

#include <list>
#include <set>
#include <string>
#include <pthread.h>

class GnamQueue
 {
  public:
    GnamQueue();
    ~GnamQueue();
    bool empty(void);
    std::string::size_type size(void);
    void push (void*);
    void* pop(void);
    void erase (void*);

  private:
    std::list<void*> m_gqueue;
    std::set<void*> m_isin;
    pthread_mutex_t m_qmutex;
 };

#endif // _Gnam_Queue_h
