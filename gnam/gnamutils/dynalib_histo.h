/* $Id$ */

#ifndef DYNALIB_HISTO_HDR
#define DYNALIB_HISTO_HDR

namespace daq {
    namespace gnam {
        namespace dynalib_histo {
            // dynalib index
            void set (unsigned int ind);
            void reset (void);
            unsigned int get (void);
        }
    }
}

#endif /* DYNALIB_HISTO_HDR */

