/* $Id$ */

#ifndef FILE2STRINGS_HDR
#define FILE2STRINGS_HDR

struct std_c_args {
    int argc;
    char **argv;
};

#ifdef __cplusplus
#define cextern extern "C"
#else
#define cextern
#endif

cextern struct std_c_args *file2strings (const char *filename,
                                         unsigned int *nlibs);
cextern void file2strings_reset (void);

#endif /* #ifndef FILE2STRINGS_HDRfile2strings */

