//$Id$

#ifndef GNAM_LIB_EXCEPTION
#define GNAM_LIB_EXCEPTION

#include <ers/ers.h>

#include <string.h>

namespace daq
{

// Void Structure, Just For SemiColon
#define VSJFSC struct _VSJFSC_

    // ExitNow
    ERS_DECLARE_ISSUE (gnamlib, ExitNow, "FATAL ERROR", ) VSJFSC;

    namespace gnamlib
    {
        inline void FatalError (const std::exception &ex) // throw (ExitNow)
            __attribute__ ((noreturn));
        inline void FatalError (const std::exception &ex) // throw (ExitNow)
        {
            ExitNow issue (ERS_HERE, ex);
            ers::fatal (issue);
            //throw ExitNow (ERS_HERE, ex);
            abort();
        }
    }

    // Exceptions 

    ERS_DECLARE_ISSUE (gnamlib, Exception, , ) VSJFSC;

    // From GNAM to the libraries
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, InvalidHistoName, Exception,
     "Histograms Naming Convention not honoured, please refer to this"
     " document: https://edms.cern.ch/document/710911."
     " Invalid histogram name: `" << name << "'; reason: " << reason,
     , ((std::string) name) ((std::string) reason)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, DuplicatedHistoName, Exception,
     "An histogram with this name " << name << " already exists",
     , ((std::string) name)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, AlreadyExistingBranch, Exception,
     "branch already exists: " << branch,
     , ((std::string) branch)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, NotExistingBranch, Exception,
     "Branch does not exist: " << branch,
     , ((std::string) branch)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, AlreadyExistingPAO, Exception,
     "PAO already exists: " << name,
     , ((std::string) name)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, UnexpectedHistoType, Exception,
     "unexpected histo_type: " << histo_type
     << "; much likely, it indicates an error in the management of dynamic"
     " memory allocation. Anyway, there is a serious bug, either in the GNAM"
     " core on in the user plugin(s).",
     , ((unsigned int) histo_type)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, NotExistingPAO, Exception,
     "PAO does not exist: " << name,
     , ((std::string) name)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, HistoAlreadyInPAO, Exception,
     "Histogram `" << histoname << "' already in PAO `" << paoname << "'",
     , ((std::string) histoname) ((std::string) paoname)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, GnamHistoAPIUsageBase, Exception, , , ) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, GnamHistoAPIUsage, GnamHistoAPIUsageBase,
     message,
     , ((std::string) message)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, DimNumMismatch, GnamHistoAPIUsageBase,
     reqDim << "-dim method (" << method  << ") requested for "
     << thisDim << "-dim histogram: " << histoname,
     , ((std::string) method) ((unsigned int) reqDim)
     ((std::string) histoname) ((unsigned int) thisDim)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, HistoryBinOutOfRange, GnamHistoAPIUsageBase,
     method << " called with out-of-range bin value - max: " << binMax 
     << " current: " << binVal << " histogram: " << histoname,
     , ((std::string) method) ((unsigned int) binMax)
     ((std::string) histoname) ((unsigned int) binVal)) VSJFSC;
    
    // From the libraries to GNAM
    // Users will define their own issues inheriting from LibraryUnusableBase
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, LibraryUnusableBase, Exception, , , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, LibraryUnusable, LibraryUnusableBase,
     message,
     , ((std::string) message)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, CannotRegisterBranch, LibraryUnusableBase,
     "Failed to register branch: " << branch,
     , ((std::string) branch)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, CannotFindBranch, LibraryUnusableBase,
     "Branch not found: " << branch,
     , ((std::string) branch)) VSJFSC;

    // Exceptions 

    ERS_DECLARE_ISSUE (gnamlib, Message, , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, NoFitting, Message,
     "fitting is permitted only during PrePublish",
     ,) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnamlib, OHSObjectTypeMismatch, Message,
     "ObjectTypeMismatch exception received from OHS while trying to publish"
     " histogram " << histo << "; it will not be published any more",
     , ((std::string) histo)) VSJFSC;
    
    ERS_DECLARE_ISSUE_BASE
    (gnamlib, OHSRepositoryNotFound, Message,
     "transient (?) error received from OHS",
     , ) VSJFSC;

}

#endif // GNAM_LIB_EXCEPTION
