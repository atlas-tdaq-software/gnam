//$Id$

#ifndef GNAM_HISTO_HDR
#define GNAM_HISTO_HDR

#include <stdint.h>
#include <string>
#include <vector>
#include <map>

#include <gnam/gnamutils/GnamPAO-def.h>
#include <gnam/gnamutils/GnamLibException.h>
#include <gnam/gnamutils/GnamQueue.h>

#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <oh/OHRootProvider.h>
#include <Rtypes.h>

#define FLAG_BIT_PROTO(method) \
    bool method (void) const; \
    void method (bool on_off)

typedef std::vector< std::pair <std::string, std::string> > GNAM_ann_t;

class GnamHisto;
typedef std::map<std::string, class GnamHisto *> gnamMap_t;

class GnamHisto
{
private:
    unsigned int whoami;
    enum histoType {GnamHisto_1F, GnamHisto_2F};
    unsigned int histo_type;
    TH1 *histo1;
    TH2 *histo2;
    bool to_be_published;
    bool to_be_flushed;
    void InternalCheck (void) const;

public:

    enum {
              NEVER_RESET               = 0x00,
              RESET_AT_WASTART          = 0x10,
              RESET_AT_WASTOP           = 0x20,
              RESET_AT_WASS             = 0x30
         };

    enum {
              FILL_ANYTIME              = 0x00,
              NOT_FILL_WHEN_READY       = 0x40,
              NOT_FILL_WHEN_STANDBY     = 0x80,
              NEVER_FILL                = 0xC0,
              FILL_RESET_W_RDY          = 0x100
         };

#define BINTYPEMASK 0xf
    enum binType {SHORTBIN, INTBIN, FLOATBIN, DOUBLEBIN,
                  NOTPUBLISHED  = 0x00010000,
                  NOTFILLED     = 0x00020000,
                  NOTDUMPED     = 0x00040000,
                  NOTREFRESHED  = 0x00080000,
                  ASHISTORY     = 0x01000000,
                  FROMHISTORY   = 0x02000000};
    // inline void SetPublishFlag(){to_be_published=true;}
    void SetPublishFlag();
  
    /*
     * 32 bit mask; by default, all bits are enabled
     */
private:
    uint32_t flags_mask;
    void flags_mask_init (uint32_t hflags);
public:
    FLAG_BIT_PROTO (IsPublished);
    FLAG_BIT_PROTO (IsFilled);
    FLAG_BIT_PROTO (IsDumped);
    FLAG_BIT_PROTO (IsRefreshedAtEor);
    FLAG_BIT_PROTO (NotAsHistory);
    FLAG_BIT_PROTO (NotFromHistory);

    FLAG_BIT_PROTO (NeverReset);
    FLAG_BIT_PROTO (ResetAtWarmStart);
    FLAG_BIT_PROTO (ResetAtWarmStop);
    FLAG_BIT_PROTO (ResetAtWarmSS);

    FLAG_BIT_PROTO (FillAnyTime);
    FLAG_BIT_PROTO (FillWhenReady);
    FLAG_BIT_PROTO (FillWhenStandby);
    FLAG_BIT_PROTO (NeverFill);
    FLAG_BIT_PROTO (StartFillingResetWhenReady);

    /*
     * Constructors and destructor
     */
private:
    void GnamHisto1DCommon(const char * name, int hflags,
                           const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                           Int_t from_history_size = 0);
    void GnamHisto1D (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup, 
                      int hflags=SHORTBIN,
                      const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                      Int_t from_history_size = 0);
    void GnamHisto1D (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins, 
                      int hflags=SHORTBIN, 
                      const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                      Int_t from_history_size = 0);
 
    void GnamHisto2DCommon(const char * name, int hflags,
                           const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                           Int_t from_history_size = 0);
    void GnamHisto2D (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup,
                      Int_t nbinsy, Axis_t ylow, Axis_t yup, 
                      int hflags=SHORTBIN,
                      const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                      Int_t from_history_size = 0);
    void GnamHisto2D (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins,
                      Int_t nbinsy, Double_t * ybins, 
                      int hflags=SHORTBIN,
                      const GNAM_ann_t &aAnn = oh::util::EmptyAnnotation,
                      Int_t from_history_size = 0);

public:
    //1D
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t *xbins,
               int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
  
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               GnamPAO *tmppao, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t * xbins,
               GnamPAO *tmppao, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
  
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               const char *pname, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t * xbins,
               const char *pname, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);

    //2D
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               Int_t nbinsy, Axis_t ylow, Axis_t yup, 
               int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t * xbins,
               Int_t nbinsy, Double_t * ybins, 
               int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);

    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               Int_t nbinsy, Axis_t ylow, Axis_t yup,
               GnamPAO *tmppao, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t * xbins,
               Int_t nbinsy, Double_t * ybins,
               GnamPAO *tmppao, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);

    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Axis_t xlow, Axis_t xup,
               Int_t nbinsy, Axis_t ylow, Axis_t yup,
               const char *pname, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);
    GnamHisto (const char *name, const char *title,
               Int_t nbinsx, Double_t * xbins,
               Int_t nbinsy, Double_t * ybins,
               const char *pname, int hflags=SHORTBIN,
               const GNAM_ann_t &ann = oh::util::EmptyAnnotation,
               Int_t from_history_size = 0);

    ~GnamHisto ();

    unsigned int whoAmI() { return whoami; }

    /*
     * OH annotations
     */
private:
    GNAM_ann_t ann;
public:
    inline GNAM_ann_t &getAnnotation () {return ann;}
  
    /*
     * publish on OH
     */
private:
    bool OH_ObjectTypeMismatch;
    static bool last_publish_was_ok;
public:
    Int_t Publish (OHRootProvider *provider, unsigned int * pub_chan);

    /*
     * fits are allowed only during prePublish
     */
private:
    static bool fit_enabled;
public:
    static void GnamHistoFitEnable (bool flag);
    static bool GnamHistoFitEnabled (void);
    
    /*
     * history
     */
private:
    UInt_t hist_size, next2add;
    Double_t *hist_buf;
    bool hist_buf_filled_once;
    bool hist_with_errors;
    bool hist_with_weights;
    void FillHistory (Double_t x);
    void FillHistory (Double_t x, Double_t y);
    void FillHistory (Double_t x, Double_t y, Double_t w);
    UInt_t first_to_read (UInt_t n_entries);
    void ResetHistory (void);
    void FillFromHistory (void);
    void FillAsHistory (void); // throw (daq::gnamlib::DimNumMismatch)
    void UnsetHistory (void);
    void SetHistory (UInt_t new_hist_size=0); // throw (daq::gnamlib::DimNumMismatch)
    void HistorySetBinError (Int_t bin, Double_t error);


public:
    bool IsHistory (void) const;
    bool AsHistory (void) const;
    bool FromHistory (void) const;
    void IsHistory (bool on_off);
    void AsHistory (bool on_off);
    void FromHistory (bool on_off, UInt_t hist_size=0);
    void FlushHistory (void);
  
    /*
     * PAO
     */
private:
    GnamPAO *mypao;
public:
    GnamPAO *GetPAO (void) const;
    void AddToPAO (GnamPAO *newpao); // throw (daq::gnamlib::HistoAlreadyInPAO)
    void RemoveFromPAO (void);
  
    /*
     * dynalib
     */
private:
    unsigned int dynalib_ind;
public:
    unsigned int GetDynalib (void) const;

    /*
     * uncovered needs
     */
private:
    void * m_ptr_any;
public:
    int ghctl (int f = 0, void* p = 0);
  
    /*
     * ROOT
     */
public:  
    // give access to TH1 & TH2 objects
    inline TH1 * gTH1 (void) { return histo1; }
    inline TH2 * gTH2 (void) { return histo2; }
    uint32_t nBins (void);
    // Fill a 1d histo
    Int_t Fill (Double_t x); // throw (daq::gnamlib::DimNumMismatch)
    // Fill a 1d histo with weight OR fill a 2d histo
    Int_t Fill (Double_t x, Double_t y); // throw (daq::gnamlib::GnamHistoAPIUsage)
    // Fill a 2d histo with weight
    Int_t Fill (Double_t x, Double_t y, Double_t w);//  throw (daq::gnamlib::GnamHistoAPIUsage, daq::gnamlib::DimNumMismatch)
  
    void Write (void);
    const char *GetName (void) const;
    const char *GetTitle (void) const;
    TClass *IsA (void) const;
    Int_t GetNbinsX (void) const;
    Int_t GetNbinsY (void) const; // throw (daq::gnamlib::DimNumMismatch)
  
    void Reset (void);
    void Reset (Option_t* option);
  
    void SetBins (Int_t nXbin, Axis_t Xmin, Axis_t Xmax);
    void SetBins (Int_t nXbin, Axis_t Xmin, Axis_t Xmax,
                  Int_t nYbin, Axis_t Ymin, Axis_t Ymax); // throw (daq::gnamlib::DimNumMismatch)
  
    void SetName (const char *name);
    void SetTitle (const char *name);
    void SetNameTitle (const char *name, const char *title);
  
    TAxis *GetXaxis (void) const;
    TAxis *GetYaxis (void) const;
  
    Int_t Fit (const char *formula, Option_t *option, Option_t *goption,
               Axis_t xmin = 0, Axis_t xmax = 0); // throw (daq::gnamlib::DimNumMismatch);
    Int_t Fit (TF1 *f1, Option_t* option, Option_t* goption,
               Axis_t xmin = 0, Axis_t xmax = 0); // throw (daq::gnamlib::DimNumMismatch);
    void FitSlicesX (TF1* f1 = 0, Int_t binmin = 1, Int_t binmax = 0,
                     Int_t cut = 0, Option_t* option = "QNR"); // throw (daq::gnamlib::DimNumMismatch);
    void FitSlicesY (TF1* f1 = 0, Int_t binmin = 1, Int_t binmax = 0,
                     Int_t cut = 0, Option_t* option = "QNR"); // throw (daq::gnamlib::DimNumMismatch);
  
    Stat_t GetEntries (void) const;
    Stat_t GetMean (Int_t axis = 1) const;
    void StatOverflows (Bool_t flag = kTRUE);
    Double_t GetRMS (Int_t axis = 1) const;
    Double_t GetRMSError (Int_t axis = 1) const;
  
    //Int_t GetBin (Int_t binx, Int_t biny = 0, Int_t binz = 0) const;
    Int_t GetBin (Int_t binx, Int_t biny = 0) const; // throw (daq::gnamlib::DimNumMismatch);

    Axis_t GetBinCenter (Int_t bin) const;
    Stat_t GetBinContent (Int_t bin) const;
    Stat_t GetBinContent (Int_t binx, Int_t biny) const; // throw (daq::gnamlib::DimNumMismatch);
    Stat_t GetBinError (Int_t bin) const;
    Stat_t GetBinError (Int_t binx, Int_t biny) const; // throw (daq::gnamlib::DimNumMismatch);
    Axis_t GetBinLowEdge (Int_t bin) const;
    Axis_t GetBinWidth (Int_t bin) const;

    TF1 * GetFunction(const char *name);
  
    void Scale(Double_t c1=1);
  
    TProfile* ProfileX(const char* name = "_pfx", Int_t firstybin = -1, 
                       Int_t lastybin = -1, Option_t* option="") const; // throw (daq::gnamlib::DimNumMismatch);
    TProfile* ProfileY(const char* name = "_pfy", Int_t firstxbin = -1, 
                       Int_t lastxbin = -1, Option_t* option="") const; // throw (daq::gnamlib::DimNumMismatch);
    TH1D* ProjectionX(const char* name = "_px", Int_t firstybin = -1, 
                      Int_t lastybin = -1, Option_t* option="") const; // throw (daq::gnamlib::DimNumMismatch);
    TH1D* ProjectionY(const char* name = "_py", Int_t firstxbin = -1, 
                      Int_t lastxbin = -1, Option_t* option="") const; // throw (daq::gnamlib::DimNumMismatch);

    Bool_t Add(const TH1* h1, Double_t c1 = 1);
    Bool_t Add(TF1* h1, Double_t c1 = 1, Option_t* option = "");
    Bool_t Add(const TH1* h, const TH1* h2, Double_t c1 = 1, Double_t c2 = 1);
    void AddBinContent(Int_t bin);
    void AddBinContent(Int_t bin, Double_t w);

    void SetBinContent(Int_t bin, Double_t content);
    void SetBinContent(Int_t binx, Int_t biny, Double_t content);
    void SetBinContent(Int_t binx, Int_t biny, Int_t binz, Double_t content);

    void SetBinError(Int_t bin, Double_t error);
    void SetBinError(Int_t binx, Int_t biny, Double_t error);
    void SetBinError(Int_t binx, Int_t biny, Int_t binz, Double_t error);

    Bool_t Divide(const TH1* h1);
    Bool_t Divide(TF1* f1, Double_t c1 = 1);
    Bool_t Divide(const TH1* h1, const TH1* h2, Double_t c1 = 1, 
		  Double_t c2 = 1, Option_t* option = "");

    Bool_t Multiply(const TH1* h1);
    Bool_t Multiply(TF1* h1, Double_t c1 = 1);
    Bool_t Multiply(const TH1* h1, const TH1* h2, Double_t c1 = 1,
		    Double_t c2 = 1, Option_t* option = "");

    /*
     * GnamHisto map
     * GnamHisto publish queue
     */
private:
    static gnamMap_t m_gnamHistoMap;
    static GnamQueue m_gnamPublishQueue;
public:
    static GnamHisto * name2histo (const char *name);
    static void StoreName (const char *name, GnamHisto *ghisto);
    static void RemoveName (const char *name);
    static void setPublishAll();
    static void resetAll();
    static void flushAll();
    static bool available2publish();
    static std::string::size_type nr2publish();
    static GnamHisto * next2publish();
    static void set2publish (GnamHisto *ghisto);
    static void reset2publish (GnamHisto *ghisto);
    static unsigned int publishAll (OHRootProvider *provider);
    static void writeAll();
    static void cleanUp (std::map<uint32_t,uint32_t> & tobecleaned);
    static gnamMap_t & getHistoMap();

    /*
     * warm start and stop handling
     */
private:
    static bool m_ready;
    static uint32_t m_fillMask;
public:
    static void GnamHistoSetReady ( bool s);
    static void warmStart();
    static void warmStop();

    /*
     * uncovered needs
     */
private:
    static void * m_GnamHistoPtr;
public:
    static int GnamHistoCtl (int f = 0, void* p = 0);

};

typedef std::vector<class GnamHisto *> gnamList_t;

#endif // #if GNAM_HISTO_HDR

