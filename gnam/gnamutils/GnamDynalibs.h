//$Id$

#ifndef GNAM_DYNALIBS_HDR
#define GNAM_DYNALIBS_HDR

#include <stdint.h>

#include <gnam/gnamutils/GnamHisto.h>

#include <config/Configuration.h>

#include <gnamdal/GnamLibrary.h>

/*******************************/
/* LOAD                        */
/*******************************/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*initDB_t) (Configuration *, const gnamdal::GnamLibrary *);
#else
extern "C" void initDB (Configuration *confDB, 
                        const gnamdal::GnamLibrary *library);
#endif

/*******************************/
/* CONFIG                      */
/*******************************/

/*
  You should declare to GNAM which objects will be managed by decoding
  routines.
*/

/*******************************/
/* START OF RUN                */
/*******************************/

/*
  Run number "run" is starting ...
*/

#ifdef GNAM_THIS_IS_GNAM
typedef const gnamList_t * (*startOfRun_t) (int run, std::string type);
#else
extern "C" const gnamList_t * startOfRun (int run, std::string type);
#endif

/*******************************/
// Every Event
/*******************************/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*decode_t) (const std::vector<const uint32_t *> *,
                          const std::vector<unsigned long int> *,
                          const uint32_t *, unsigned long int);
#else
extern "C" void decode (const std::vector<const uint32_t *> *rods,
                        const std::vector<unsigned long int> *sizes,
                        const uint32_t *event, unsigned long int event_size);
#endif

#ifdef GNAM_THIS_IS_GNAM
typedef void (*fillHisto_t) (void);
#else
extern "C" void fillHisto (void);
#endif

#ifdef GNAM_THIS_IS_GNAM
typedef void (*warmStart_t) (void);
#else
extern "C" void warmStart (void);
#endif

#ifdef GNAM_THIS_IS_GNAM
typedef void (*warmStop_t) (void);
#else
extern "C" void warmStop (void);
#endif

/*******************************/
/* Before publishing histos    */
/*******************************/

/*
  "prePublish" is called just before publishing some histograms
  to the Information Service.
  If the run has been completed, endOfRun will be set to true.
  You may use some one-entry histos to report the up-to-date
  status of your detector, so you don't need to fill it 
  unless it is going to be published.
  You may also this routine to perform some operations on your
  histos, but, please, do not run heavy functions in the GNAM
  context: on-line monitoring must work at any level and must
  be fast, and it is not the right place for fits.
*/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*prePublish_t) (bool endOfRun);
#else
extern "C" void prePublish (bool endOfRun);
#endif

/*******************************/
// CUSTOM COMMANDS
/*******************************/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*customCommand_t) (const GnamHisto *histo, int argc,
                                 const char * const *argv);
#else
extern "C" void customCommand (const GnamHisto *histo, int argc,
                               const char * const *argv);
#endif

/*******************************/
// END OF RUN
/*******************************/

/*
  Run is going to finish; this is a good
  place to publish the lat histograms.
*/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*endOfRun_t) (void);
#else
extern "C" void endOfRun (void);
#endif

/*******************************/
// DATAFLOW STOPPED
/*******************************/

/*
  Run is done; this is a good
  place to clean up your objects
  and make final sums or operations.
*/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*DFStopped_t) (void);
#else
extern "C" void DFStopped (void);
#endif

/*******************************/
// EXIT
/*******************************/

/*
  Your library is going to be unloaded: final clean-up
  should be performed. Special attention to memory
  used, please: free whatever memory you allocated!
*/

#ifdef GNAM_THIS_IS_GNAM
typedef void (*end_t) (void);
#else
extern "C" void end (void);
#endif

#endif // #ifndef GNAM_DYNALIBS_HDR

