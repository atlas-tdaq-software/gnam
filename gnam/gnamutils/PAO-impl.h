//$Id$

#ifndef PAO_IMPL_HDR
#define PAO_IMPL_HDR

#include <gnam/gnamutils/GnamPAO-def.h>
//#include <gnam/gnamutils/bufmgr.h>

class GnamPAO {
public:
    std::string name;
    std::vector<GnamHisto *> histos;
};

// called centrally by GNAM
void PAOResetAll (void);

// used internally by GnamHisto::AddToPAO
void PAOAddHisto (GnamPAO *pao, GnamHisto *histo);

// used internally by GnamHisto::RemoveFromPAO
void PAORemoveHisto (GnamPAO *pao, GnamHisto *histo);

#endif // PAO_IMPL_HDR

