/* $Id$ */

#ifndef BUFMGR_HDR
#define BUFMGR_HDR

#include <stdlib.h>
#include <stdint.h>

#ifndef cextern
#ifdef __cplusplus
#define cextern extern "C"
#else /* #ifdef __cplusplus */
#define cextern
#endif /* #ifdef __cplusplus */
#endif /* #ifndef cextern */

struct bm_bchunk {
    /*  magic number */
    uint32_t magic;
    /*  amount of free space */
    size_t free_space;
    /*  first free byte address */
    void *first_free;
    /*  next chunk in buffer */
    struct bm_bchunk *next_bchunk;
};
typedef struct bm_bchunk bm_bchunk_t;

struct bm_buffer {
    /*  magic number */
    uint32_t magic;
    /*  memory quantum */
    size_t quantum;
    /*  first and last chunk */
    bm_bchunk_t *first_bchunk, *last_bchunk;
};
typedef struct bm_buffer bm_buffer_t;

struct bm_garray {
    /*  magic number */
    uint32_t magic;
    /*  single component size */
    size_t comp_size;
    /*  array actual address */
    void *address;
    /*  actual number of components */
    size_t n_comp;
};
typedef struct bm_garray bm_garray_t;

/*
 * malloc functions
 */
cextern void *bm_realloc (void *ptr, size_t size) __attribute__ ((malloc));
#define bm_malloc(size) bm_realloc (NULL, size)

/*
 * buffer
 */
cextern bm_buffer_t *bm_new_buffer (size_t quantum);
cextern void bm_clear_buffer (bm_buffer_t *buffer);
cextern void bm_del_buffer (bm_buffer_t **p2buffer);
cextern void *
bm_add2buffer (bm_buffer_t *buffer, const void *src, size_t size);

/*
 * garray
 */
cextern bm_garray_t *bm_new_garray (size_t comp_size);
cextern void bm_clear_garray (bm_garray_t *garray);
cextern void bm_del_garray (bm_garray_t **p2garray);
cextern size_t bm_add2garray (bm_garray_t *garray, const void *new_comp);

/*
 * old interface
 */
cextern int buf_init (size_t size);
cextern int buf_clear (int buf_index);
cextern void *buf_add_object (int buf_index, const void *object, size_t size);
cextern char *buf_add_string (int buf_index, const char *string);

#endif /*  #ifndef BUFMGR_HDR */

