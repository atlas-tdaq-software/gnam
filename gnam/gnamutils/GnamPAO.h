//$Id$

#ifndef GNAMPAO_HDR
#define GNAMPAO_HDR

#include <gnam/gnamutils/GnamPAO-def.h>
#include <gnam/gnamutils/GnamHisto.h>
#include <gnam/gnamutils/GnamLibException.h>

GnamPAO *PAOFindByName (const char *name); // throw (daq::gnamlib::NotExistingPAO)
GnamPAO *NewPAO (const char *name); // throw (daq::gnamlib::AlreadyExistingPAO)
const char *PAOGetName (const GnamPAO *pao);
const std::vector<GnamHisto *> &PAOGetHistos (const GnamPAO *pao);

#endif // #ifndef GNAMPAO_HDR

