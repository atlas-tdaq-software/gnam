//$Id$

#ifndef DYNACOMMON_HDR
#define DYNACOMMON_HDR

#include "gnam/gnamutils/GnamLibException.h"
#include "gnam/gnamutils/GnamDynalibs.h"
#include <ostream>

struct dynalib {
    // handler returned from dlopen
    void *lib;
    // file name
    char *name;
    // library is still usable
    bool usable;
    // function pointers
    //init_t init;
    initDB_t initDB;
    startOfRun_t startOfRun;
    decode_t decode;
    fillHisto_t fillHisto;
    prePublish_t prePublish;
    endOfRun_t endOfRun;
    DFStopped_t DFStopped;
    customCommand_t customCommand;
    end_t end;
    warmStart_t warmStart;
    warmStop_t warmStop;
};

/*
  #define DLEXEC( lib, fun, args ...) do { \
  struct dynalib _lib_ = (lib); \
  if (_lib_.fun) { \
  MR_DEBUG (2, "DLEXEC - library: %s - function: %s", \
  _lib_.name, # fun); \
  _lib_.fun (args); \
  } \
  } while (0)
*/

#define DLEXEC_BASE_START( only_if_usable, fun) \
    if (only_if_usable && !_lib_->usable) { \
        MR_DEBUG (2, "DLEXEC - library: " << _lib_->name \
                  << " is not usable any more"); \
    } else if (!_lib_->fun) { \
        MR_DEBUG (2, "DLEXEC - library: " << _lib_->name \
                  << " - no such function: " # fun); \
    } else { \
        MR_DEBUG (2, "DLEXEC - library: " << _lib_->name \
                  << " - function starting: " # fun); \
        try {

#define DLEXEC_SET_UNUSABLE \
            _lib_->usable = false; 
            

#define DLEXEC_BASE_END(fun) \
        } \
        catch (ers::Issue &exc) { \
            std::ostringstream oss; \
            oss << "Library "<<_lib_->name << \
            " is unusable and will not be used anymore"; \
            daq::gnamlib::LibraryUnusable issue(ERS_HERE, \
                                                oss.str().c_str(), \
                                                exc); \
            ers::error(issue); \
            DLEXEC_SET_UNUSABLE; \
        } \
        catch (std::exception &exc) { \
            std::ostringstream oss; \
            oss << "Exception by library " << _lib_->name << \
            ": "<< exc.what() <<". Library will not be used anymore."; \
            daq::gnamlib::LibraryUnusable issue(ERS_HERE, oss.str().c_str(), \
                                                exc); \
            ers::error(issue); \
            DLEXEC_SET_UNUSABLE; \
        } \
        catch (...) { \
            std::ostringstream oss; \
            oss << "Unknown exception by library " << _lib_->name << \
            ". Library will not be used anymore."; \
            daq::gnamlib::LibraryUnusable issue(ERS_HERE, \
                                                oss.str().c_str()); \
            ers::error(issue); \
            DLEXEC_SET_UNUSABLE; \
        } \
        MR_DEBUG (2, "DLEXEC - library: " <<  _lib_->name \
                     << " - function finished: " << # fun); \
    }

#define DLEXEC_2( only_if_usable, out, lib, fun, arg1, arg2) do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    out = _lib_->fun (arg1, arg2); \
    DLEXEC_BASE_END (fun); \
} while (0)

#define DLEXEC_VOID_4( only_if_usable, lib, fun, arg1, arg2, arg3, arg4) \
do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    _lib_->fun (arg1, arg2, arg3, arg4); \
    DLEXEC_BASE_END (fun); \
} while (0)

#define DLEXEC_VOID_3( only_if_usable, lib, fun, arg1, arg2, arg3) \
do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    _lib_->fun (arg1, arg2, arg3); \
    DLEXEC_BASE_END (fun); \
} while (0)

#define DLEXEC_VOID_2( only_if_usable, lib, fun, arg1, arg2) \
do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    _lib_->fun (arg1, arg2); \
    DLEXEC_BASE_END (fun); \
} while (0)

#define DLEXEC_VOID_1( only_if_usable, lib, fun, arg1) \
do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    _lib_->fun (arg1); \
    DLEXEC_BASE_END (fun); \
} while (0)

#define DLEXEC_VOID_0( only_if_usable, lib, fun) \
do { \
    struct dynalib *_lib_ = &(lib); \
    DLEXEC_BASE_START (only_if_usable, fun); \
    _lib_->fun (); \
    DLEXEC_BASE_END (fun); \
} while (0)


#endif // #ifndef DYNACOMMON_HDR

