// $Id$

#ifndef GNAM_EVENT_ITERATOR
#define GNAM_EVENT_ITERATOR

#include "gnam/gnam/GnamException.h"
#include "emon/dal/SamplingParameters.h"
#include "emon/EventIterator.h"

#include <vector>
#include <string>

namespace daq {
    namespace gnam {
      class gnamEventIterator;
      class GnamSampler;
    }
}

class daq::gnam::gnamEventIterator {

private:
  emon::EventIterator * m_iter;
  IPCPartition partition;
  const emon::dal::SamplingParameters * sampler;
 public:
  gnamEventIterator (const IPCPartition &partition_in,
		     const emon::dal::SamplingParameters * Sampler);
  void connect(); // throw (daq::gnam::SamplerNotFound);
  void disconnect();
  ~gnamEventIterator();
  emon::EventIterator * iter() const;
  std::string dumpsampler() const;
};

inline
emon::EventIterator *
daq::gnam::gnamEventIterator::iter() const
{
    ERS_ASSERT (m_iter);
    return m_iter;
}

#endif // GNAM_EVENT_ITERATOR
