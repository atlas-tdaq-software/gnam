//$Id$

#ifndef COMM_LISTENER_HDR
#define COMM_LISTENER_HDR

#include "gnam/gnam/GnamException.h"

#include <oh/OHCommandListener.h>

#include <vector>
#include <string>

namespace daq {
    namespace gnam {
        class CommQueueEntry;
        CommQueueEntry *CommQueuePop (void);
        class CommListener;

        const std::string GNAM_COMM_RESET_STR = "Reset";
        const int GNAM_COMM_RESET = 0;
        const std::string GNAM_COMM_REBIN_STR = "Rebin";
        const int GNAM_COMM_REBIN = 1;
        const std::string GNAM_COMM_CUSTOM_STR = "Custom";
        const int GNAM_COMM_CUSTOM = 2;
        const std::string GNAM_COMM_UPDATE_STR = "Update";
        const int GNAM_COMM_UPDATE = 3;
        const std::string GNAM_COMM_PUBLISHED_STR = "IsPublished";
        const int GNAM_COMM_PUBLISHED = 4;
        const std::string GNAM_COMM_FILLED_STR = "IsFilled";
        const int GNAM_COMM_FILLED = 5;
        const std::string GNAM_COMM_DUMPED_STR = "IsDumped";
        const int GNAM_COMM_DUMPED = 6;
        const std::string GNAM_COMM_NEVER_RESET_STR = "NeverReset";
        const int GNAM_COMM_NEVER_RESET = 7;
        const std::string GNAM_COMM_RESET_AT_WASTA_STR = "WStartReset";
        const int GNAM_COMM_RESET_AT_WASTA = 8;
        const std::string GNAM_COMM_RESET_AT_WASTO_STR = "WStopReset";
        const int GNAM_COMM_RESET_AT_WASTO = 9;
        const std::string GNAM_COMM_RESET_AT_WASS_STR = "WSSReset";
        const int GNAM_COMM_RESET_AT_WASS = 10;
        const std::string GNAM_COMM_FILL_ANYTIME_STR = "FillAlways";
        const int GNAM_COMM_FILL_ANYTIME = 11;
        const std::string GNAM_COMM_FILL_WHEN_READY_STR = "FillWhenReady";
        const int GNAM_COMM_FILL_WHEN_READY = 12;
        const std::string GNAM_COMM_FILL_WHEN_STANDBY_STR = "FillWhenStandby";
        const int GNAM_COMM_FILL_WHEN_STANDBY = 13;
        const std::string GNAM_COMM_NEVER_FILL_STR = "NeverFill";
        const int GNAM_COMM_NEVER_FILL = 14;
        const std::string GNAM_COMM_WASTA_STR = "WarmStart";
        const int GNAM_COMM_WASTA = 15;
        const std::string GNAM_COMM_WASTO_STR = "WarmStop";
        const int GNAM_COMM_WASTO = 16;
        const std::string GNAM_COMM_START_FILLING_RESET_WHEN_READY_STR = "StartFillingResetWhenReady";
        const int GNAM_COMM_START_FILLING_RESET_WHEN_READY = 17;
 
    }
}

/*
 * CommQueueEntry
 */

class daq::gnam::CommQueueEntry {
private:
    std::string hname_str;
    std::string command;
    std::vector<std::string> buf;
    void clear () noexcept;
    void init (const std::string &HistoName, const std::string &Command); // throw (BadCommand);
public:
    const char *hname;
    int cmd;
    int argc;
    const char **argv;
    CommQueueEntry () noexcept;
    CommQueueEntry (const std::string &HistoName, const std::string &Command); // throw (BadCommand);
    CommQueueEntry (const CommQueueEntry &Source) noexcept;
    CommQueueEntry &operator= (const CommQueueEntry &Source) noexcept;
    ~CommQueueEntry ();
};

/*
 * CommListener
 */

class daq::gnam::CommListener
    : public OHCommandListener
{
public:
    // OHCommandListener virtual functions
    void command (const std::string &hname, const std::string &cmd);
    void command (const std::string &cmd);
};

#endif // #ifndef COMM_LISTENER_HDR
