//$Id$

#ifndef GNAM_EXCEPTION
#define GNAM_EXCEPTION

#include "gnam/gnamutils/GnamLibException.h"

#include <ers/ers.h>

#include <string.h>
#include <stdlib.h>

namespace daq
{
    /***************
     * EXCEPTIONS
     ***************/

    ERS_DECLARE_ISSUE (gnam, Exception, , ) VSJFSC;

    // Not Found

    ERS_DECLARE_ISSUE_BASE
    (gnam, NotFound, Exception, , , ) VSJFSC;

    /*
      ERS_DECLARE_ISSUE_BASE
      (gnam, ConfFileNotFound, NotFound,
      "Cannot get configuration from file '" << name << "'",
      , ((std::string) name)) VSJFSC;
    */

    ERS_DECLARE_ISSUE_BASE
    (gnam, ConfDBNotFound, NotFound,
     "Impossible to open the configuration DB",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, ConfObjNotFound, NotFound,
     "Configuration object '" << name << "' not found",
     , ((std::string) name)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, SamplerNotFound, NotFound,
     "Cannot connect to the sampler. " << message,
     , ((std::string) message)) VSJFSC;

    // Bad Configuration

    ERS_DECLARE_ISSUE_BASE
    (gnam, BadConf, Exception, , , ) VSJFSC;

    /*
      ERS_DECLARE_ISSUE_BASE
      (gnam, KeyValueMismatch, BadConf,
      "Mismatch in the number of keys and values for sampler selection",
      , ) VSJFSC;
    */

    ERS_DECLARE_ISSUE_BASE
    (gnam, InvalidProcessName, BadConf
     , "Process name cannot contain '.' characters: " << name,
     , ((std::string) name)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, InvalidPartition, BadConf,
     "Invalid partition '" << part << "'",
     , ((std::string) part)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, NoPartition, BadConf,
     "environment variable TDAQ_PARTITION is not set",
     , ) VSJFSC;

    /*
      ERS_DECLARE_ISSUE_BASE
      (gnam, MissingKV, BadConf,
      "with '-f conffile'," " '-k keys' and '-v values' are required",
      , ) VSJFSC;
    */

    // Internal Error

    ERS_DECLARE_ISSUE_BASE
    (gnam, InternalErrorBase, Exception, , , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, InternalError, InternalErrorBase,
     "INTERNAL ERROR (please contact the authors): " << message,
     , ((std::string) message)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, NeverHere, InternalErrorBase,
     "INTERNAL ERROR (please contact the authors): this line should never"
     " be executed",
     , ) VSJFSC;

    // FullEventFragment Issues

    ERS_DECLARE_ISSUE_BASE
    (gnam, DataDecodingIssue, Exception,
     "Unable to instantiate full event fragment from emon event data,"
     " skipping",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, FullEventFragmentReadIssue, Exception,
     "Unable to read full event fragment payload, skipping", , ) VSJFSC;

    // miscellanea
     
    ERS_DECLARE_ISSUE_BASE
    (gnam, DLOpenError, Exception,
     "Error '" << error << "' loading library '" << library << "'",
     , ((std::string) error) ((std::string) library)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, ThreadTimeout, Exception,
     "Thread end timed out ",
     ,) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, BadROOTFileName, Exception,
     "invalid filename: " << filename,
     , ((std::string) filename)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, POSIXError, Exception,
     "system call '" << syscall << "'failed: " << strerror (errnum) << ": "
     << comment,
     , ((std::string) syscall) ((int) errnum) ((std::string) comment)) VSJFSC;

    /************
     * MESSAGES
     ***********/

    ERS_DECLARE_ISSUE (gnam, Message, , ) VSJFSC;

    // OHS commands

    ERS_DECLARE_ISSUE_BASE
    (gnam, BadCommand, Message,
     "bad command: " << command,
     , ((std::string) command)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, BadCommandHistogram, Message,
     "histogram " << histo << " not found: discarding command: " << command,
     , ((std::string) histo) ((std::string) command)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, BadCommandSyntax, Message,
     "bad syntax for command: " << command << " (" << message << ")",
     , ((std::string) command) ((std::string) message)) VSJFSC;

    // miscellanea
 
    ERS_DECLARE_ISSUE_BASE
    (gnam, EMONSamplerStopped, Message,
     "EventSampler stopped sampling; will try and reconnect later",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, dlcloseError, Message,
     "dlclose (" << lib << "): " << error,
     , ((std::string) lib) ((std::string) error)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, MissingTDAQ_DB, Message,
     "In interactive mode the TDAQ_DB variable must be set",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, OHSProvider, Message,
     "Error received while trying to create new OHRootProvider;"
     " histograms will not be published to OHS",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, ISRunNumber, Message,
     "Exception caught while trying to fetch the run number;"
     " setting run number to 0",
     , ) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, ROOTFileExists, Message,
     "file " << file << " already exists; remaning it to " << backup,
     , ((std::string) file) ((std::string) backup)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, RepeatedHistoName, Message,
     "repeated histogram name: " << histo << "; discarding",
     , ((std::string) histo)) VSJFSC;

    ERS_DECLARE_ISSUE_BASE
    (gnam, HistoMemoryLeak, Message,
     "*** MEMORY LEAK RECOVERED ***: "
     << n_histo << " histograms left from " << dynalib,
     , ((unsigned int) n_histo) ((std::string) dynalib)) VSJFSC;
} 

#endif // GNAM_EXCEPTION 
