//$Id$

#ifndef GETRODS_HDR
#define GETRODS_HDR

#include <stdint.h>

int getrods(const uint32_t *data, unsigned long int size, 
            std::vector<const uint32_t *> &rods, 
            std::vector<unsigned long int> &sizes);
#endif // #ifndef GETRODS_HDR

