//$Id$

#ifndef _GnamVariables_h
#define _GnamVariables_h

#include "gnam/gnam/GnamEventIterator.h"
#include "gnam/gnam/GnamException.h"
#include "gnamdal/GnamApplication.h"

#include <vector>
#include <string>

namespace daq {
    namespace gnam {
        class GnamVariables;
    }
}

/*
 * WARNING WARNING WARNING
 *
 * The default values for some of the GnamVariables are written also
 * in the database schema file. Please keep these two files synchronized!
 */

class daq::gnam::GnamVariables {
public:
    GnamVariables (int argc, const char * const *argv); // throw (NoPartition, InvalidProcessName);
    ~GnamVariables () noexcept;
    GnamVariables &operator= (const gnamdal::GnamApplication *GnamDB) noexcept;
    std::string Dump() noexcept;
    void one_more_event () noexcept;
    void new_run (int RunNumber, const std::string&RunType) noexcept;
    void new_runst (bool st) noexcept;
    void reset_eventsinrun () noexcept;
private:
    // environment
    std::string partition;
    // cmdline
    bool isinteractive;
    std::string parent;
    std::string segment;
    std::string processname;
    const std::string &providername; // = processname
    // database
    std::string servername;
    std::string outfile;
    int updatefr_evt;
    int updatefr_sec;
    bool dump;
    int dump_fr_evt;
    unsigned int chan2oh;
    int start_to;
    int thread_to;
    // internal
    int runnumber;
    std::string runtype;
    bool runstate;
    int eventstotal;
    int eventsinrun;
    bool try2reconnect;
public:
    // getters
    const std::string &get_partition () const
        {return partition;}
    bool get_isinteractive () const
        {return isinteractive;}
    const std::string &get_parent () const
        {return parent;}
    const std::string &get_segment () const
        {return segment;}
    const std::string &get_processname () const
        {return processname;}
    const std::string &get_providername () const
        {return providername;}
    const std::string &get_servername () const
        {return servername;}
    const std::string &get_outfile () const
        {return outfile;}
    int get_updatefr_evt () const
        {return updatefr_evt;}
    int get_updatefr_sec () const
        {return updatefr_sec;}
    bool get_dump () const
        {return dump;}
    int get_dump_fr_evt () const
        {return dump_fr_evt;}
    unsigned int get_chan2oh () const
        {return chan2oh;}
    int get_start_to () const
        {return start_to;}
    int get_thread_to () const
      {return thread_to;}
    int get_runnumber () const
        {return runnumber;}
    bool get_runstate () const
        {return runstate;}
    const std::string &get_runtype () const
        {return runtype;}
    int get_eventstotal () const
        {return eventstotal;}
    int get_eventsinrun () const
        {return eventsinrun;}
    bool get_try2reconnect () const
        {return try2reconnect;}
    // setter
    void set_try2reconnect (bool flag)
        {try2reconnect = flag;}
};

inline
void
daq::gnam::GnamVariables::one_more_event () noexcept
{
    ++ eventsinrun;
    ++ eventstotal;
}


#endif /* GnamVariables_h */


