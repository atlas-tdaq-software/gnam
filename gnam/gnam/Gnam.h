//$Id$

#ifndef _Gnam_h
#define _Gnam_h

#include "gnam/gnamutils/GnamHisto.h"
#include "gnam/gnam/EventThread.h"
#include "gnam/gnam/CommListener.h"
#include "gnam/gnam/GnamEventIterator.h"
#include "gnam/gnam/GnamVariables.h"
#include "gnamdal/GnamApplication.h"

#include <owl/timer.h>
#include <oh/OHRootProvider.h>
#include <config/Configuration.h>
#include "RunControl/Common/Controllable.h"

#include <string>
#include <set>

namespace daq {
    namespace gnam {
        class Gnam;
    }
}

class daq::gnam::Gnam
    : public daq::rc::Controllable
{

public:
    Gnam (GnamVariables &Variables);
    ~Gnam () noexcept;
  

    void configure (const daq::rc::TransitionCmd& cmd);
    void connect (const daq::rc::TransitionCmd& cmd);
    void prepareForRun (const daq::rc::TransitionCmd& cmd);
    void stopRecording (const daq::rc::TransitionCmd& cmd);
    void stopGathering(const daq::rc::TransitionCmd& cmd);
    void stopArchiving(const daq::rc::TransitionCmd& cmd);
    void disconnect (const daq::rc::TransitionCmd& cmd);
    void unconfigure (const daq::rc::TransitionCmd& cmd);
    void onExit (daq::rc::FSM_STATE state) noexcept;
    void user(const daq::rc::UserCmd& cmd);

private:
    GnamVariables &vars;
    const gnamdal::GnamApplication *gnamDB;
    OHRootProvider *provider;
    OWLTimer timertotal;
    OWLTimer timerinrun;
    EventThread *eventThread;
    CommListener *lst;
    gnamEventIterator *gnam_ev_it;
    std::set<std::string> third_party_libs;
};

#endif
