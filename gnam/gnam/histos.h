//$Id$

#ifndef GNAM_HISTOS_H
#define GNAM_HISTOS_H

#include <vector>
#include <oh/OHRootProvider.h>

#include "gnam/gnamutils/GnamHisto.h"

#define GNAM_THIS_IS_GNAM
#include "gnam/gnam/dynacommon.h"

void histo_reset (GnamHisto *histo, OHRootProvider *provider);

void histo_reset_all ();

void histo_rebin (GnamHisto *histo, OHRootProvider *provider,
                  int argc, const char * const *argv);

void histo_dump_move (const char *filename, int run, bool first);
void histo_dump (const char *filename, int run);

void histo_set_publish_all ();

unsigned int histo_publish (GnamHisto *histo, OHRootProvider *provider);

unsigned int histo_publish_all (OHRootProvider *provider, unsigned int n_chan);

GnamHisto *histo_name2histo (const char *name);

void histo_clear (const std::vector <struct dynalib> &dynalibs);

#endif // #ifndef GNAM_HISTOS_H

