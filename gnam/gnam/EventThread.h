//$Id$

#ifndef _EventThread_h
#define _EventThread_h

#include <oh/OHRootProvider.h>

#include "gnam/gnam/GnamVariables.h"
#include "gnam/gnam/GnamEventIterator.h"
#include "gnam/gnamutils/GnamHisto.h"
#define GNAM_THIS_IS_GNAM

#include "gnam/gnam/dynacommon.h"

#include <iostream>
#include <thread>
#include <atomic>

namespace daq {
    namespace gnam {
        class EventThread;
    }
}

class daq::gnam::EventThread
{
public:
    EventThread(GnamVariables &vars,
                gnamEventIterator *agnam_ev_it,
                std::vector <struct dynalib> &aDynalibs,
                OHRootProvider *aProvider);
    void run();
    void startExecution();
    void stopExecution();

private:
    GnamVariables &variables;
    gnamEventIterator *gnam_ev_it;
    std::vector <struct dynalib> &dynalibs;
    OHRootProvider *provider;
    std::atomic<bool> m_running;
    std::thread m_thread;
};

#endif
