#!/usr/bin/env tdaq_python


import os, sys
import pm.project
import subprocess
from optparse import OptionParser
from operator import add
from ipc import IPCPartition
from ispy import InfoReader


def run(cmd, exc):

    exit_code = 0
    
    p = spawn(cmd, exc)

    if exc:
        p.communicate()
        exit_code = p.poll()

    return exit_code


def spawn(cmd, exc):

    p = None
    
    if exc:
        p = subprocess.Popen(cmd, \
                             env = os.environ, shell= True)
    else:
        print cmd

    return p


def pmg_kill(partition, application, host, exc):

    command = 'pmg_kill_app -p %s -n %s -H %s' \
          % (partition, application, host)

    code = run(command, exc)

    if code != 0:
        print 'Command "%s" failed with code %d' % (command, code)
    

def oh_cmd(partition, server, provider, cmd, exc):

    command = 'oh_cmd -p %s -s %s -n %s -c %s' \
          % (partition, server, provider, cmd)

    code = run(command, exc)

    if code != 0:
        print 'Command "%s" failed with code %d' % (command, code)
        

def checkout_is(partition, server, info):

    r = InfoReader(partition, server, info)
    r.update()

    return r.objects


def app_exists_pmg(partition, app):

    objs = checkout_is(partition, 'PMG','.*%s' % (app.id))

    pmgapps = []
    for a in objs.keys():
        try:
            pmgapps.append(a.split('|',1)[1])
        except IndexError:
            pass
            
    return  app.id in pmgapps
   


def app_exists_runctrl(partition, app):

    objs = checkout_is(partition, 'RunCtrl', app.id)

    return (True if objs else False)


if __name__ == '__main__':

    parser = OptionParser()
    parser.add_option("-w", "--wstart",
                      action='store_true', dest="wstart",
                      help="Send the wart start command to all gnam instances")
    parser.add_option("-s", "--wstop",
                      action='store_true', dest="wstop",
                      help="Send the wart stop command to all gnam instances")
    parser.add_option("-r", "--reset",
                      action='store_true', dest="reset",
                      help="Reset all gnam histograms")
    parser.add_option("-k", "--kill",
                      action='store_true', dest="kill",
                      help="Kill all gnam instances")
    parser.add_option("-p", "--pmg",
                      action="store_true", dest="use_pmg",
                      help="Use PMG IS server to check application status (default)")
    parser.add_option("-c", "--runctrl",
                      action="store_false", dest="use_pmg",
                      help="Use RunCtrl IS server to check application status")
    parser.add_option("-d", "--dorun",
                      action='store_true', dest="dorun",
                      help="Actually run the requested action (otherwise only prints the action list)")

    parser.set_defaults(dorun=False, use_pmg=True, \
                        kill=False, reset=False, \
                        wstart = False, wstop = False)

    if len(sys.argv) < 2:
        parser.print_help()
        sys.exit(0)
    
    (options, args) = parser.parse_args()

    actions = (options.reset, options.kill, options.wstart, options.wstop)

    if reduce(add,actions) != 1:
        parser.print_help()
        sys.exit(0)

    if options.use_pmg:
        app_exists = app_exists_pmg
    else:
        app_exists = app_exists_runctrl

    dbname = os.getenv('TDAQ_DB')
    if dbname == None:
        print 'TDAQ_DB is not defined'
        sys.exit(1)
    
    partition = os.getenv('TDAQ_PARTITION')
    if partition == None:
        print 'TDAQ_PARTITION is not defined'
        sys.exit(1)

    #Initialize IPC for RDB operation
    part = IPCPartition(partition)

    if not part.isValid():
        print "Partition '%s' not found" % (partition)
        sys.exit(1)

    db = pm.project.Project(dbname)

    gnams = db.getObject('GnamApplication')

    if options.reset: action = 'RESET'
    elif options.kill: action = 'KILL'
    elif options.wstart: action = 'WSTART'
    elif options.wstop: action = 'WSTOP'

    print 'Will run the following action: '
    print
    print '%s (Dry run: %s)' \
          % (action, not options.dorun)
    print
    print 'on the following applications:'
    print
    print reduce(add, ['%s, ' % g.id for g in gnams])
    print
    
    print 'Starting now ...'
    print
    
    for g in gnams:

        if not app_exists(part, g):
            print "Application '%s' not found .. skipping" % (g.id)
            continue
        
        if options.reset:
            oh_cmd(partition, g.OHServerName, g.id, 'reset', options.dorun)
        elif options.wstart:
            oh_cmd(partition, g.OHServerName, g.id, 'WarmStart', options.dorun)
        elif options.wstop:
            oh_cmd(partition, g.OHServerName, g.id, 'WarmStop', options.dorun)
        elif options.kill:
            pmg_kill(partition, g.id, g.RunsOn.id, options.dorun) 
                
