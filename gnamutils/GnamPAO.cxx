//$Id$

#include <ers/ers.h>

#include <gnam/gnamutils/GnamPAO.h>
#include <gnam/gnamutils/PAO-impl.h>
#include <gnam/gnamutils/GnamMessRep.h>
#include <gnam/gnamutils/GnamLibException.h>

#include <string.h>

// list of all the defined PAOs
static std::vector<GnamPAO *> paos;

GnamPAO *
PAOFindByName (const char *name)
{
    for (size_t k = 0; k < paos.size(); k++) {
        GnamPAO *tmppao = paos[k];
        if (name == tmppao->name) {
            return tmppao;
        }
    }
    throw daq::gnamlib::NotExistingPAO (ERS_HERE, name);
}

void
PAOResetAll (void)
{
    MR_DEBUG (1, "PAOResetAll");
    for (size_t k = 0; k < paos.size(); k++) {
        delete paos[k];
    }
    paos.clear();
}

GnamPAO *
NewPAO (const char *name)
{
    GnamPAO *pao;
    // check whether it already exists
    try {
        PAOFindByName (name);
        throw daq::gnamlib::AlreadyExistingPAO (ERS_HERE, name);
    }
    catch (daq::gnamlib::NotExistingPAO &exc) {
    }
    // Allocate memory
    pao = new GnamPAO;
    pao->name = name;
    // record the PAO
    paos.push_back (pao);
    // return the address to the calling function
    return pao;
}

void
PAOAddHisto (GnamPAO *pao, GnamHisto *histo)
{
    // ERROR: please use GnamHisto::AddToPAO
    ERS_ASSERT (histo->GetPAO() == pao);
    pao->histos.push_back (histo);
}    

void
PAORemoveHisto (GnamPAO *pao, GnamHisto *histo)
{
    ERS_ASSERT (histo->GetPAO() == pao);
    std::vector<GnamHisto *> * histos = &(pao->histos);
    for (size_t k = 0; k < (*histos).size(); k++) {
        if (histo == (*histos)[k]) {
            (*histos).erase ((*histos).begin()+k);
            break;
        }
     }
}    

const char *
PAOGetName (const GnamPAO *pao)
{
    return pao->name.c_str();
}

const std::vector<GnamHisto *> &
PAOGetHistos (const GnamPAO *pao)
{
    return pao->histos;
}


