#include <iostream>
#include <string>

#include "gnam/gnamutils/GnamQueue.h"

GnamQueue::GnamQueue()
 {
  pthread_mutex_init (&m_qmutex, NULL);
 }

GnamQueue::~GnamQueue()
 {
  pthread_mutex_destroy (&m_qmutex);
 }

bool GnamQueue::empty(void)
 {
  pthread_mutex_lock (&m_qmutex);
  std::string::size_type gqsize = m_gqueue.size();
  std::string::size_type insize = m_isin.size();
  bool empty = m_gqueue.empty();
  pthread_mutex_unlock (&m_qmutex);
  if (gqsize != insize) throw "Broken queue";
  return empty;
 }

std::string::size_type GnamQueue::size(void)
 {
  pthread_mutex_lock (&m_qmutex);
  std::string::size_type gqsize = m_gqueue.size();
  std::string::size_type insize = m_isin.size();
  pthread_mutex_unlock (&m_qmutex);
  if (gqsize != insize) throw "Broken queue";
  return gqsize;
 }

void GnamQueue::erase(void* gh)
 {
  pthread_mutex_lock (&m_qmutex);
  if (m_isin.count(gh))
   {
    std::list<void*>::iterator it;
    for (it = m_gqueue.begin(); it != m_gqueue.end(); it++)
     {
      if (*it==gh)
       {
        m_gqueue.erase(it);
        m_isin.erase(gh);
        break;
       }
     }
    if (it == m_gqueue.end())
     {
      pthread_mutex_unlock (&m_qmutex);
      throw "Broken queue";
     }
   }
  std::string::size_type gqsize = m_gqueue.size();
  std::string::size_type insize = m_isin.size();
  pthread_mutex_unlock (&m_qmutex);
  if (gqsize != insize) throw "Broken queue";
 }

void GnamQueue::push(void* gh)
 {
  pthread_mutex_lock (&m_qmutex);
  if (m_isin.count(gh)==0)
   {
    m_gqueue.push_back(gh);
    m_isin.insert(gh);
   }
  std::string::size_type gqsize = m_gqueue.size();
  std::string::size_type insize = m_isin.size();
  pthread_mutex_unlock (&m_qmutex);
  if (gqsize != insize) throw "Broken queue";
 }

void* GnamQueue::pop(void)
 {
  void* gh(NULL);
  pthread_mutex_lock (&m_qmutex);
  if (!m_gqueue.empty())
   {
    gh = m_gqueue.front();
    m_gqueue.pop_front();
    m_isin.erase(gh);
   }
  std::string::size_type gqsize = m_gqueue.size();
  std::string::size_type insize = m_isin.size();
  pthread_mutex_unlock (&m_qmutex);
  if (gqsize != insize) throw "Broken queue";
  return gh;
 }

