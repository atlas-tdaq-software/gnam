/* $Id$ */

#include <gnam/gnamutils/dynalib_histo.h>

#define DYNALIB_IND_INVALID 999999

static unsigned int dynalib_ind = DYNALIB_IND_INVALID;

void
dynalib_ind_set (unsigned int ind)
{
    dynalib_ind = ind;
}

void
dynalib_ind_reset (void)
{
    dynalib_ind = DYNALIB_IND_INVALID;
}

unsigned int
dynalib_ind_get (void)
{
    return dynalib_ind;
}

