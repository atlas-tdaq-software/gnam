/* $Id$ */

#ifdef GNAM_DEBUG_BUFMGR
#include <stdio.h>
#define MR_DEBUG fprintf
#endif

#include "gnam/gnamutils/bufmgr.h"

#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <stddef.h>
#include <stdlib.h>

#define MAGIC_bchunk ((uint32_t) 0x01cc23cc)
#define MAGIC_buffer ((uint32_t) 0x01bb23bb)
#define MAGIC_garray ((uint32_t) 0x01aa23aa)

#define BM_CHECK( type, pointer, ret) { \
    __typeof__ (pointer) p = (pointer); \
    if (p == NULL) { \
        /* MR_DEBUG (3, "ERROR: invalid NULL pointer"); */ \
        return ret; \
    } \
    if (p->magic != MAGIC_ ## type) { \
        /* MR_DEBUG (3, "ERROR: invalid magic number: %u", p->magic); */ \
        return ret; \
    } \
    /* MR_DEBUG (3, "check magic ok"); */ \
}
#define BM_CHECK_BCHUNK( pointer, ret) BM_CHECK (bchunk, pointer, ret)
#define BM_CHECK_BUFFER( pointer, ret) BM_CHECK (buffer, pointer, ret)
#define BM_CHECK_GARRAY( pointer, ret) BM_CHECK (garray, pointer, ret)

/*
 * malloc functions
 */

void *
bm_realloc (void *ptr, size_t size)
{
    void *tmp;
    tmp = realloc (ptr, size);
    if (tmp != NULL) {
        return tmp;
    } else {
#ifdef GNAM_DEBUG_BUFMGR
        MR_DEBUG (/* 3, */ "ERROR: out of memory; cannot allocate %u bytes",
                  size);
#endif
        abort();
    }
}


/*
 * bchunk
 */

static bm_bchunk_t *
bm_new_bchunk (size_t size)
/*
  Allocate new memory and store a "struct bchunk" as header
*/
{
    void *newmem;
    bm_bchunk_t *newbchunk;
    /* allocate new memory */
    newmem = bm_malloc (sizeof (bm_bchunk_t) + size);
    /* reserve space for a "struct bchunk" and fill it */
    newbchunk = (bm_bchunk_t *) newmem;
    newbchunk->magic = MAGIC_bchunk;
    newbchunk->free_space = size;
    newbchunk->first_free = (void *) ((size_t) newmem + sizeof (bm_bchunk_t));
    newbchunk->next_bchunk = (bm_bchunk_t *) NULL;
    return newbchunk;
}

static void
bm_del_bchunk (bm_bchunk_t *bchunk)
{
    BM_CHECK_BCHUNK (bchunk, );
    free (bchunk);
}

static void *
bm_add2bchunk (bm_bchunk_t *bchunk, const void *src, size_t size)
{
    void *old_first;
    BM_CHECK_BCHUNK (bchunk, NULL);
    if (size > bchunk->free_space) {
        return NULL;
    }
    old_first = bchunk->first_free;
    memcpy (old_first, src, size);
    bchunk->first_free = (void *) ((size_t) bchunk->first_free + size);
    bchunk->free_space -= size;
    return old_first;
}


/*
 * buffer
 */

static void
bm_init_buffer (bm_buffer_t *buffer, size_t quantum)
{
    buffer->magic = MAGIC_buffer;
    if (quantum == 0) {
        buffer->quantum = 4096 - sizeof (bm_bchunk_t);
    } else {
        buffer->quantum = quantum;
    }
    buffer->first_bchunk = (bm_bchunk_t *) NULL;
    buffer->last_bchunk = (bm_bchunk_t *) NULL;
    BM_CHECK_BUFFER (buffer, );
}


bm_buffer_t *
bm_new_buffer (size_t quantum)
{
    bm_buffer_t *buffer;
    buffer = bm_malloc (sizeof (bm_buffer_t));
    bm_init_buffer (buffer, quantum);
    return buffer;
}


void
bm_clear_buffer (bm_buffer_t *buffer)
{
    bm_bchunk_t *delete, *next;
    BM_CHECK_BUFFER (buffer, );
    delete = buffer->first_bchunk;
    while (delete != NULL) {
        next = delete->next_bchunk;
        bm_del_bchunk (delete);
        delete = next;
    }
    bm_init_buffer (buffer, buffer->quantum);
}


void
bm_del_buffer (bm_buffer_t **p2buffer)
{
    bm_buffer_t *buffer = *p2buffer;
    BM_CHECK_BUFFER (buffer, );
    bm_clear_buffer (buffer);
    free (buffer);
    *p2buffer = (bm_buffer_t *) NULL;
}

void *
bm_add2buffer (bm_buffer_t *buffer, const void *src, size_t size)
{
    bm_bchunk_t *tmp;
    void *new_pos = NULL;
    size_t new_bchunk_size;
    bm_bchunk_t *new_bchunk;
    BM_CHECK_BUFFER (buffer, NULL);
    /* try to add object to old chunks */
    tmp = buffer->first_bchunk;
    while (tmp != NULL) {
        new_pos = bm_add2bchunk (tmp, src, size);
        if (new_pos != NULL) {
            return new_pos;
        }
        tmp = tmp->next_bchunk;
    }
    /* need another chunk */
    if (size > buffer->quantum) {
        new_bchunk_size = size;
    } else {
        new_bchunk_size = buffer->quantum;
    }
    /* memory is allocated with bm_malloc; bm_newbchunk cannot fail */
    new_bchunk = bm_new_bchunk (new_bchunk_size);
    /* integrate new chunk in buffer */
    if (buffer->first_bchunk == NULL) {
        buffer->first_bchunk = new_bchunk;
    }
    if (buffer->last_bchunk != NULL) {
        buffer->last_bchunk->next_bchunk = new_bchunk;
    }
    buffer->last_bchunk = new_bchunk;
    /* add object to new chunk */
    return bm_add2bchunk (buffer->last_bchunk, src, size);
}    


/*
 * garray
 */

static void
bm_init_garray (bm_garray_t *garray, size_t comp_size)
{
    if (comp_size == 0) {
#ifdef GNAM_DEBUG_BUFMGR
        MR_DEBUG (/* 3, */ "INTERNAL ERROR: invalid null component size");
#endif
        return;
    }
    garray->magic = MAGIC_garray;
    garray->comp_size = comp_size;
    garray->address = NULL;
    garray->n_comp = 0;
    BM_CHECK_GARRAY (garray, );
}


bm_garray_t *
bm_new_garray (size_t comp_size)
{
    bm_garray_t *garray;
    if (comp_size == 0) {
#ifdef GNAM_DEBUG_BUFMGR
        MR_DEBUG (/* 3, */ "ERROR: invalid null component size");
#endif
        return NULL;
    }
    garray = bm_malloc (sizeof (bm_garray_t));
    bm_init_garray (garray, comp_size);
    return garray;
}


void
bm_clear_garray (bm_garray_t *garray)
{
    BM_CHECK_GARRAY (garray, );
    free (garray->address);
    bm_init_garray (garray, garray->comp_size);
}


void
bm_del_garray (bm_garray_t **p2garray)
{
    bm_garray_t *garray = *p2garray;
    BM_CHECK_GARRAY (garray, );
    bm_clear_garray (garray);
    free (garray);
    *p2garray = (bm_garray_t *) NULL;
}

size_t
bm_add2garray (bm_garray_t *garray, const void *new_comp)
{
    size_t new_size;
    void *new_comp_addr;
    BM_CHECK_GARRAY (garray, (size_t) 0);
    /* ensure enough memory (possibly changing address) */
    new_size = garray->comp_size * (garray->n_comp + 1);
    garray->address = bm_realloc (garray->address, new_size);
    /* copy new component */
    new_comp_addr = (void *) ((size_t) garray->address +
                              garray->comp_size * garray->n_comp);
    memcpy (new_comp_addr, new_comp, garray->comp_size);
    /* update number of components and report it */
    garray->n_comp++;
    return garray->n_comp;
}


/*
 * old interface
 */

static bm_garray_t *buffer_pointers = (bm_garray_t *) NULL;

int
buf_init (size_t size)
{
    int bufindex;
    void *buffer;
    if (size == 0) {
#ifdef GNAM_DEBUG_BUFMGR
        MR_DEBUG (/* 3, */ "ERROR: invalid null size");
#endif
        return -1;
    }
    if (buffer_pointers == (bm_garray_t *) NULL) {
#ifdef GNAM_DEBUG_BUFMGR
        MR_DEBUG (/* 3, */ "initialising buffer_pointers");
#endif
        buffer_pointers = bm_new_garray (sizeof (bm_buffer_t *));
    }
#ifdef GNAM_DEBUG_BUFMGR
    MR_DEBUG (/* 3, */ "creating new buffer");
#endif
    buffer = (void *) bm_new_buffer (0);
#ifdef GNAM_DEBUG_BUFMGR
    MR_DEBUG (/* 3, */ "created new buffer: 0x%08x", (size_t) buffer);
#endif
    bm_add2garray (buffer_pointers, &buffer);
    bufindex = (int) (buffer_pointers->n_comp) - 1;
#ifdef GNAM_DEBUG_BUFMGR
    MR_DEBUG (/* 3, */ "new bufindex: %d", bufindex);
#endif
    return bufindex;
}
  
int
buf_clear (int buf_index)
{
    bm_buffer_t *buffer;
    if (buf_index < 0 || buf_index >= (int) buffer_pointers->n_comp) {
        return 1;
    }
    buffer = ((bm_buffer_t **) buffer_pointers->address)[buf_index];
    bm_del_buffer (&buffer);
    return 0;
}

void *
buf_add_object (int buf_index, const void *object, size_t size)
{
    bm_buffer_t *buffer;
    if (buf_index < 0 || buf_index >= (int) buffer_pointers->n_comp) {
        return NULL;
    }
    buffer = ((bm_buffer_t **) buffer_pointers->address)[buf_index];
    return bm_add2buffer (buffer, object, size);
}

    
char *
buf_add_string (int buf_index, const char *string)
{
    if (string == NULL) {
        return (char *) buf_add_object (buf_index, "", 1);
    } else {
        return (char *) buf_add_object (buf_index, (void *) string,
                                        strlen (string) + 1);
    }
}




