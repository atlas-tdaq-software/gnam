/* $Id$ */

#include <gnam/gnamutils/dynalib_histo.h>

#define DYNALIB_IND_INVALID 999999

static unsigned int dynalib_ind = DYNALIB_IND_INVALID;

void
daq::gnam::dynalib_histo::set (unsigned int ind)
{
    dynalib_ind = ind;
}

void
daq::gnam::dynalib_histo::reset (void)
{
    dynalib_ind = DYNALIB_IND_INVALID;
}

unsigned int
daq::gnam::dynalib_histo::get (void)
{
    return dynalib_ind;
}

