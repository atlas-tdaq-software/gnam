/*$Id$ */

#ifdef GNAM_DEBUG_FILE2STRINGS
#include <stdio.h>
#define MR_DEBUG fprintf
#endif

#include <stdio.h>
#include <stdlib.h>

#include "gnam/gnamutils/file2strings.h"
#include "gnam/gnamutils/line_parse.h"
#include "gnam/gnamutils/bufmgr.h"

/* This may be useful */
#ifndef NULL
#define NULL ((void *)0)
#endif

/*
 * Info from configuration files
 */
/* N of libraries */
static unsigned int n_libs = 0;
static struct std_c_args *libs = NULL;
/* strings */
static int n_strings_tot = 0;
static char **strings = NULL;
/* buffer containing all the characters */
static int n_chars_tot = 0;
static int chars = -1;

void
file2strings_reset (void)
{
    n_libs = 0;
    free (libs);
    libs = NULL;
    n_strings_tot = 0;
    free (strings); 
    strings = 0;
    n_chars_tot = 0;
    if (chars >= 0) {
        if (buf_clear (chars) != 0) {
#ifdef GNAM_DEBUG_FILE2STRINGS
            MR_DEBUG (/* 3, */ "FATAL: Internal Error 1");
#endif
            exit (3);
        }
        chars = 0;
    }
}


struct std_c_args *
file2strings (const char *filename, unsigned int *nlibs)
{
    FILE *stream;
    char *line;
    size_t len;
    ssize_t length;
    int line_ok;
    int tmp;
    void *tmpp;
    unsigned int n_libs_tmp;
    int n_strings_tmp;
    /* line_parse arguments */
    size_t n_strings, n_chars;
    char *first_string;
    /* Initial reset */
    file2strings_reset();
#define F2S_ERROR {file2strings_reset(); return NULL;}
    /* Open file */
    stream = fopen (filename, "r");
    if (stream == NULL) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "ERROR: cannot open file `%s': %m", filename);
#endif
        F2S_ERROR;
    }
    /* Get the numbers */
    while (1) {
        line = NULL;
        len = 0;
        length = getline (&line, &len, stream);
        if (length == -1) {
            break;
        }
        n_strings = 0;
        line_ok = line_parse (line, length, &n_strings, &n_chars, NULL);
        if (line_ok != 0) {
            F2S_ERROR;
        }
        if (n_strings > 0) {
            n_libs++;
            n_strings_tot += n_strings;
            /* For each "argv", we need a final NULL pointer. */
            n_strings_tot ++;
            n_chars_tot += n_chars;
            /* For each string, we need a final null character. */
            n_chars_tot += n_strings;
        }
        free (line);
    }
#ifdef GNAM_DEBUG_FILE2STRINGS
    MR_DEBUG (/* 3, */ "check: n_libs (%d) - n_strings_tot (%d)",
              n_libs, n_strings_tot);
#endif
    /* Allocate enough memory */
    libs = (struct std_c_args *) malloc ((n_libs + 1) *
                                         sizeof (struct std_c_args));
    if (libs == NULL ) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "FATAL: not enough memory for array `libs'");
#endif
        exit (3);
    }
    strings = (char **) malloc ((n_strings_tot) *sizeof (char *));
    if (strings == NULL ) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "FATAL: not enough memory for array `strings'");
#endif
        exit (3);
    }
    /*strings = buf_init (n_strings_tot * sizeof (char *)); */
#ifdef GNAM_DEBUG_FILE2STRINGS
    MR_DEBUG (/* 3, */ "n_chars_tot: %d", n_chars_tot);
#endif
    chars = buf_init (n_chars_tot * sizeof (char));
    /* Rewind */
    tmp = fseek (stream, 0, SEEK_SET);
    if (tmp != 0) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "ERROR: fseek on file `%s'", filename);
#endif
        F2S_ERROR;
    }
    /* Fill the arrays */
    /*******************************************************************
     *                                                                 *
     * WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING *
     *                                                                 *
     * I will assume that the file has not changed in the meanwhile    *
     *                                                                 *
     * WARNING WARNING WARNING WARNING WARNING WARNING WARNING WARNING *
     *                                                                 *
     *******************************************************************/
    n_libs_tmp = 0;
    n_strings_tmp = 0;
    while (1) {
        line = NULL;
        len = 0;
        length = getline (&line, &len, stream);
        if (length == -1) {
            break;
        }
        line_ok = line_parse (line, length, &n_strings, NULL, NULL);
        if (n_strings > 0) {
            libs[n_libs_tmp].argc = n_strings;
            libs[n_libs_tmp].argv = strings + n_strings_tmp;
            n_libs_tmp++;
            do {
                line_ok = line_parse (line, length, &n_strings, NULL,
                                      &first_string);
#ifdef GNAM_DEBUG_FILE2STRINGS
                MR_DEBUG (/* 3, */ "%s", first_string);
#endif
                tmpp = buf_add_string (chars, first_string);
                if (tmpp == NULL) {
#ifdef GNAM_DEBUG_FILE2STRINGS
                    MR_DEBUG (/* 3, */ "FATAL: Internal Error");
#endif
                    exit (3);
                }
                strings[n_strings_tmp] = (char *) tmpp;
                n_strings_tmp++;
#ifdef GNAM_DEBUG_FILE2STRINGS
                MR_DEBUG (/* 3, */ "check: %d (%d) - %d (%d)",
                          n_libs_tmp, n_libs, n_strings_tmp, n_strings_tot);
#endif
            } while (n_strings > 1);
            /* For every "argv", we need a final NULL pointer. */
            strings[n_strings_tmp] = (char *) NULL;
            n_strings_tmp++;
#ifdef GNAM_DEBUG_FILE2STRINGS
            MR_DEBUG (/* 3, */ "check1: %d (%d) - %d (%d)", n_libs_tmp, n_libs,
                      n_strings_tmp, n_strings_tot);
#endif
        }
        free (line);
    }
    /* Final checks */
    if (n_libs_tmp != n_libs) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "FATAL: Internal Error 2");
#endif
        exit (3);
    }
    if (n_strings_tmp != n_strings_tot) {
#ifdef GNAM_DEBUG_FILE2STRINGS
        MR_DEBUG (/* 3, */ "FATAL: Internal Error 3");
#endif
        exit (3);
    }
    /* Null termination for "libs" */
    libs[n_libs].argc = 0;
    libs[n_libs].argv = NULL;
    /* return */
    if (nlibs != NULL) {
        *nlibs = n_libs;
    }
    return libs;
}



