//$Id$

#include <gnam/gnamutils/GnamMessRep.h>
#include <gnam/gnamutils/GnamBranches.h>
#include <gnam/gnamutils/GnamLibException.h>

#include <string>
#include <vector>

struct SPIE {
    std::string name;
    const void *p2object;
};

static std::vector<SPIE> registered;

void
spie_reset (void)
{
    MR_DEBUG (0, "SPIE: resetting internal status");
    registered.clear();
}  

const void *
spie_fill (const std::string &branchname)
{
    size_t k, max;
    ERS_PRECONDITION (branchname.size() > 0);
    max = registered.size();
    for (k = 0; k < max; k++) {
        if (registered[k].name == branchname) {
            MR_DEBUG (0, "branch: " << branchname << " - address: "
                      << registered[k].p2object);
            ERS_ASSERT (registered[k].p2object);
            return registered[k].p2object;
        }
    }
    MR_DEBUG (0, "branch not found: " << branchname);
    throw daq::gnamlib::NotExistingBranch (ERS_HERE, branchname);
}

void
spie_register (const std::string &branchname, const void *p2object)
{
    struct SPIE tmp;
    ERS_PRECONDITION (branchname.size() > 0);
    ERS_PRECONDITION (p2object != NULL);
    // check whether it already exists
    try {
        spie_fill (branchname);
        throw daq::gnamlib::AlreadyExistingBranch (ERS_HERE, branchname);
    }
    catch (daq::gnamlib::NotExistingBranch &exc) {
    }
    // ok, it doesn't exist
    tmp.name = branchname;
    tmp.p2object = p2object;
    registered.push_back (tmp);
    MR_DEBUG (0, "branch: " << branchname << " - address: " << p2object);
}

