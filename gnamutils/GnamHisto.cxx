//$Id$

#include <ers/ers.h>

#include "gnam/gnamutils/GnamHisto.h"
#include "gnam/gnamutils/GnamMessRep.h"
#define MR_LOG(mess) do { \
    ERS_LOG (mess); \
} while (false) 
 
#include "gnam/gnamutils/GnamPAO.h"
#include "gnam/gnamutils/PAO-impl.h"
#include "gnam/gnamutils/GnamLibException.h"
#include "gnam/gnamutils/dynalib_histo.h"

using namespace daq::gnam;

#define DYNALIB_HISTO {dynalib_ind = dynalib_histo::get ();}

#include <TH1F.h>
#include <TH2F.h>
#include <TH1S.h>
#include <TH2S.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH1I.h>
#include <TH2I.h>

#include <stdlib.h>
#include <string.h>
#include <time.h>

using namespace daq::gnamlib;

/*
 * Macro definitions
 */
#define HISTO_TYPE_1D 1
#define HISTO_TYPE_2D 2

#define FLAG_BIT_FUN( bit, method) \
    const UInt_t flags_bit_ ## method = bit; \
    bool GnamHisto::method (void) const \
    { \
        if (flags_mask & flags_bit_ ## method) return false; \
        else return true; \
    } \
    void GnamHisto::method (bool on_off) \
    { \
        if (! on_off) flags_mask |= flags_bit_ ## method; \
        else flags_mask &= ~ flags_bit_ ## method; \
        this->InternalCheck(); \
    }

const UInt_t flags_bit_IsFilled = GnamHisto::NOTFILLED;
bool GnamHisto::IsFilled (void) const
{
    return ((this->flags_mask & m_fillMask) == 0);
}
void GnamHisto::IsFilled (bool on_off)
{
    if (! on_off) flags_mask |= flags_bit_IsFilled;
    else flags_mask &= ~ flags_bit_IsFilled;
    this->InternalCheck();
}

bool GnamHisto::NeverReset (void) const
{
    return ((this->flags_mask & GnamHisto::RESET_AT_WASS) == 0);
}
void GnamHisto::NeverReset (bool on_off)
{
    if (on_off) flags_mask &= ~ GnamHisto::RESET_AT_WASS;
    else flags_mask |= GnamHisto::RESET_AT_WASS;
    this->InternalCheck();
}

bool GnamHisto::ResetAtWarmStart (void) const
{
    return ((this->flags_mask & GnamHisto::RESET_AT_WASTART) != 0);
}
void GnamHisto::ResetAtWarmStart (bool on_off)
{
    if (on_off) flags_mask |= GnamHisto::RESET_AT_WASTART;
    else flags_mask &= ~ GnamHisto::RESET_AT_WASTART;
    this->InternalCheck();
}

bool GnamHisto::ResetAtWarmStop (void) const
{
    return ((this->flags_mask & GnamHisto::RESET_AT_WASTOP) != 0);
}
void GnamHisto::ResetAtWarmStop (bool on_off)
{
    if (on_off) flags_mask |= GnamHisto::RESET_AT_WASTOP;
    else flags_mask &= ~ GnamHisto::RESET_AT_WASTOP;
    this->InternalCheck();
}

bool GnamHisto::ResetAtWarmSS (void) const
{
    return ((this->flags_mask & GnamHisto::RESET_AT_WASS) == GnamHisto::RESET_AT_WASS);
}
void GnamHisto::ResetAtWarmSS (bool on_off)
{
    if (on_off) flags_mask |= GnamHisto::RESET_AT_WASS;
    else flags_mask &= ~ GnamHisto::RESET_AT_WASS;
    this->InternalCheck();
}

bool GnamHisto::FillAnyTime (void) const
{
    return ((this->flags_mask & GnamHisto::NEVER_FILL) == 0);
}
void GnamHisto::FillAnyTime (bool on_off)
{
    if (on_off) flags_mask &= ~ GnamHisto::NEVER_FILL;
    else flags_mask |= GnamHisto::NEVER_FILL;
    this->InternalCheck();
}

bool GnamHisto::FillWhenReady (void) const
{
    return ((this->flags_mask & GnamHisto::NOT_FILL_WHEN_READY) == 0);
}
void GnamHisto::FillWhenReady (bool on_off)
{
    if (! on_off) flags_mask |= GnamHisto::NOT_FILL_WHEN_READY;
    else flags_mask &= ~ GnamHisto::NOT_FILL_WHEN_READY;
    this->InternalCheck();
}

bool GnamHisto::FillWhenStandby (void) const
{
    return ((this->flags_mask & GnamHisto::NOT_FILL_WHEN_STANDBY) == 0);
}
void GnamHisto::FillWhenStandby (bool on_off)
{
    if (! on_off) flags_mask |= GnamHisto::NOT_FILL_WHEN_STANDBY;
    else flags_mask &= ~ GnamHisto::NOT_FILL_WHEN_STANDBY;
    this->InternalCheck();
}

bool GnamHisto::NeverFill (void) const
{
    return ((this->flags_mask & GnamHisto::NEVER_FILL) == GnamHisto::NEVER_FILL);
}
void GnamHisto::NeverFill (bool on_off)
{
    if (on_off) flags_mask |= GnamHisto::NEVER_FILL;
    else flags_mask &= ~ GnamHisto::NEVER_FILL;
    this->InternalCheck();
}

bool GnamHisto::StartFillingResetWhenReady (void) const
{
    return ((this->flags_mask & GnamHisto::FILL_RESET_W_RDY) == GnamHisto::FILL_RESET_W_RDY);
}
void GnamHisto::StartFillingResetWhenReady (bool on_off)
{
    if (on_off) flags_mask |= GnamHisto::FILL_RESET_W_RDY;
    else flags_mask &= ~ GnamHisto::FILL_RESET_W_RDY;
    this->InternalCheck();
}

/*
 * Initializing static members
 */
bool GnamHisto::last_publish_was_ok = true;
bool GnamHisto::fit_enabled = false;

uint32_t GnamHisto::nBins (void)
{
    return (histo_type == HISTO_TYPE_1D) ?
        histo1->GetNbinsX() :
        histo2->GetNbinsX()*histo2->GetNbinsY();
}

void
GnamHisto::GnamHistoFitEnable (bool flag)
{
    if (flag) {
        MR_DEBUG (1, "enabling GnamHisto fits");
    } else {
        MR_DEBUG (1, "disabling GnamHisto fits");
    }
    fit_enabled = flag;
}

bool
GnamHisto::GnamHistoFitEnabled (void)
{
    static bool warned = false;
    if (!fit_enabled && !warned) {
        MR_WARNING (NoFitting (ERS_HERE));
        warned = true;
    }
    return fit_enabled;
}

/*
 * GnamHisto methods
 */
void
GnamHisto::InternalCheck (void) const
{
    ERS_ASSERT (histo_type == HISTO_TYPE_1D || histo_type == HISTO_TYPE_2D);
    switch (histo_type) {
    case HISTO_TYPE_1D:
        ERS_ASSERT (histo1 != (TH1 *) 0);
        break;
    case HISTO_TYPE_2D:
        ERS_ASSERT (histo2 != (TH2 *) 0);
        ERS_ASSERT (histo1 == (TH1 *) histo2);
        break;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::flags_mask_init (uint32_t hflags)
{
    flags_mask = hflags;
}

FLAG_BIT_FUN (GnamHisto::NOTPUBLISHED, IsPublished) VSJFSC;
// FLAG_BIT_FUN (GnamHisto::NOTFILLED, IsFilled) VSJFSC;
FLAG_BIT_FUN (GnamHisto::NOTDUMPED, IsDumped) VSJFSC;
FLAG_BIT_FUN (GnamHisto::NOTREFRESHED, IsRefreshedAtEor) VSJFSC;
FLAG_BIT_FUN (GnamHisto::ASHISTORY, NotAsHistory) VSJFSC;
FLAG_BIT_FUN (GnamHisto::FROMHISTORY, NotFromHistory) VSJFSC;

#define SET_PUBLISH_FLAG(FLAG) do if (this->IsPublished() && !FLAG) {          \
  GnamHisto::set2publish(this);                                                \
  FLAG=true;                                                                   \
} while(0)

void
GnamHisto::SetPublishFlag()
{
    SET_PUBLISH_FLAG(to_be_published);
}

bool
GnamHisto::IsHistory (void) const
{
    return !( this->NotAsHistory() && this->NotFromHistory());
}

bool
GnamHisto::AsHistory (void) const
{
    return ! this->NotAsHistory();
}

bool
GnamHisto::FromHistory (void) const
{
    return ! this->NotFromHistory();
}

void
GnamHisto::AsHistory (bool on_off)
{
    if (on_off)
    {
        this->NotAsHistory(false);
        this->NotFromHistory(true);
        this->SetHistory();
    }
    else
    {
        this->NotAsHistory(true);
        this->NotFromHistory(true);
        this->UnsetHistory();
        this->Reset();
    }
}

void
GnamHisto::FromHistory (bool on_off, UInt_t n_entries)
{
    if (on_off)
    {
        this->NotAsHistory(true);
        this->NotFromHistory(false);
        this->SetHistory(n_entries);
    }
    else
    {
        this->NotAsHistory(true);
        this->NotFromHistory(true);
        this->UnsetHistory();
        this->Reset();
    }
}

void
GnamHisto::IsHistory (bool on_off)
{
    this->AsHistory(on_off);
}

void 
GnamHisto::GnamHisto1DCommon(const char * name,
                             int hflags,
                             const GNAM_ann_t &aAnn,
                             Int_t from_history_size)
{
    GnamHisto::StoreName (name, this);

    histo_type = HISTO_TYPE_1D;
    histo2 = (TH2 *) 0;
    hist_size = 0;
    next2add = 0;
    hist_buf_filled_once = false;
    mypao = 0;
    ann=aAnn;
    DYNALIB_HISTO;
    flags_mask_init(uint32_t(hflags));
    // DEBUG are not published by default
    if(strncmp(name,"/DEBUG/",strlen("/DEBUG/"))==0){
        this->IsPublished(false);
    }
    OH_ObjectTypeMismatch = false;
    to_be_published=false;
    to_be_flushed=false;
    m_ptr_any = (void*)0;
    if (this->IsHistory()) this->SetHistory (from_history_size);
    this->InternalCheck();
}

void
GnamHisto::GnamHisto1D (const char *name, const char *title,
                        Int_t nbinsx, Axis_t xlow, Axis_t xup, int hflags,
                        const GNAM_ann_t &aAnn,
                        Int_t from_history_size)
{
    int bintype = hflags & BINTYPEMASK;
    switch(bintype){
    case GnamHisto::FLOATBIN: 
        histo1 = new TH1F (name, title, nbinsx, xlow, xup);
        break;
    case GnamHisto::DOUBLEBIN:
        histo1 = new TH1D (name, title, nbinsx, xlow, xup);
        break;
    case GnamHisto::INTBIN:
        histo1 = new TH1I (name, title, nbinsx, xlow, xup);
        break;
    case GnamHisto::SHORTBIN:
    default:
        histo1 = new TH1S (name, title, nbinsx, xlow, xup);
        break;
    }
    GnamHisto1DCommon (name, hflags, aAnn, from_history_size);
}

void
GnamHisto::GnamHisto1D (const char *name, const char *title,
                        Int_t nbinsx, Double_t * xbins, int hflags,
                        const GNAM_ann_t &aAnn,
                        Int_t from_history_size)
{
    int bintype = hflags & BINTYPEMASK;
    switch(bintype){
    case GnamHisto::FLOATBIN: 
        histo1 = new TH1F (name, title, nbinsx, xbins);
        break;
    case GnamHisto::DOUBLEBIN:
        histo1 = new TH1D (name, title, nbinsx, xbins);
        break;
    case GnamHisto::INTBIN:
        histo1 = new TH1I (name, title, nbinsx, xbins);
        break;
    case GnamHisto::SHORTBIN:
    default:
        histo1 = new TH1S (name, title, nbinsx, xbins);
        break;
    }
    GnamHisto1DCommon (name, hflags, aAnn, from_history_size);
}


void 
GnamHisto::GnamHisto2DCommon(const char * name,
                             int hflags,
                             const GNAM_ann_t &aAnn,
                             Int_t from_history_size)
{
    GnamHisto::StoreName (name, this);

    histo_type = HISTO_TYPE_2D;
    histo1 = (TH1 *) histo2;
    hist_size = 0;
    next2add = 0;
    hist_buf_filled_once = false;
    mypao = 0;
    ann=aAnn;
    DYNALIB_HISTO;
    flags_mask_init(uint32_t(hflags));
    // DEBUG are not published by default
    if(strncmp(name,"/DEBUG/",strlen("/DEBUG/"))==0){
        this->IsPublished(false);
    }
    OH_ObjectTypeMismatch = false;
    to_be_published=false;
    to_be_flushed=false;
    m_ptr_any = (void*)0;
    if (this->IsHistory()) this->SetHistory (from_history_size);
    this->InternalCheck();
}

void
GnamHisto::GnamHisto2D (const char *name, const char *title,
                        Int_t nbinsx, Axis_t xlow, Axis_t xup,
                        Int_t nbinsy, Axis_t ylow, Axis_t yup, int hflags,
                        const GNAM_ann_t &aAnn,
                        Int_t from_history_size)
{ 
    int bintype = hflags & BINTYPEMASK;
    switch(bintype){
    case GnamHisto::FLOATBIN:
        histo2 = new TH2F (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
        break;
    case GnamHisto::DOUBLEBIN:
        histo2 = new TH2D (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
        break;
    case GnamHisto::INTBIN:
        histo2 = new TH2I (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
        break;
    case GnamHisto::SHORTBIN:
    default:
        histo2 = new TH2S (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup);
        break;
    }
    GnamHisto2DCommon (name, hflags, aAnn, from_history_size);
}

void
GnamHisto::GnamHisto2D (const char *name, const char *title,
                        Int_t nbinsx, Double_t * xbins,
                        Int_t nbinsy, Double_t * ybins, int hflags,
                        const GNAM_ann_t &aAnn,
                        Int_t from_history_size)
{ 
    int bintype = hflags & BINTYPEMASK;
    switch(bintype){
    case GnamHisto::FLOATBIN:
        histo2 = new TH2F (name, title, nbinsx, xbins, nbinsy, ybins);
        break;
    case GnamHisto::DOUBLEBIN:
        histo2 = new TH2D (name, title, nbinsx, xbins, nbinsy, ybins);
        break;
    case GnamHisto::INTBIN:
        histo2 = new TH2I (name, title, nbinsx, xbins, nbinsy, ybins);
        break;
    case GnamHisto::SHORTBIN:
    default:
        histo2 = new TH2S (name, title, nbinsx, xbins, nbinsy, ybins);
        break;
    }
    GnamHisto2DCommon (name, hflags, aAnn, from_history_size);
}

/***********************
 * Public methods
 ***********************/

/*
 * PAO
 */

GnamPAO *
GnamHisto::GetPAO (void) const
{
    this->InternalCheck();
    return mypao;
}

void
GnamHisto::AddToPAO (GnamPAO *newpao)
{
    this->InternalCheck();
    if (mypao) {
        throw daq::gnamlib::HistoAlreadyInPAO (ERS_HERE, GetName(),
                                               PAOGetName (mypao));
    }
    mypao = newpao;
    PAOAddHisto (newpao, this);
}

void
GnamHisto::RemoveFromPAO ()
{
    this->InternalCheck();
    if (mypao) {
      PAORemoveHisto (mypao, this);
    }
}

static GnamPAO *
findPAO (const char *name)
{
    GnamPAO *pao;
    try {
        pao = PAOFindByName (name);
    }
    catch (daq::gnamlib::NotExistingPAO &ex) {
        try {
            pao = NewPAO (name);
        }
        catch (daq::gnamlib::AlreadyExistingPAO &ex2) {
            FatalError (ex2);
        }
    }
    ERS_ASSERT (pao);
    return pao;
}

/*
 * Constructors and destructor
 */

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamHisto1D (name, title, nbinsx, xlow, xup, hflags, ann,
                 from_history_size);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamHisto1D (name, title, nbinsx, xbins, hflags, ann, from_history_size);
}


GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup, 
                      GnamPAO *tmppao, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamHisto1D (name, title, nbinsx, xlow, xup, hflags, ann,
                 from_history_size);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins, 
                      GnamPAO *tmppao, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamHisto1D (name, title, nbinsx, xbins, hflags, ann, from_history_size);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup, 
                      const char *pname, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamPAO *tmppao;
    GnamHisto1D (name, title, nbinsx, xlow, xup, hflags, ann,
                 from_history_size);
    tmppao = findPAO (pname);
    ERS_ASSERT (tmppao);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins, 
                      const char *pname, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{
    GnamPAO *tmppao;
    GnamHisto1D (name, title, nbinsx, xbins, hflags, ann, from_history_size);
    tmppao = findPAO (pname);
    ERS_ASSERT (tmppao);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup,
                      Int_t nbinsy, Axis_t ylow, Axis_t yup, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamHisto2D (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup, 
                 hflags, ann, from_history_size);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins,
                      Int_t nbinsy, Double_t * ybins, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamHisto2D (name, title, nbinsx, xbins, nbinsy, ybins, 
                 hflags, ann, from_history_size);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup,
                      Int_t nbinsy, Axis_t ylow, Axis_t yup, 
                      GnamPAO *tmppao, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamHisto2D (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup, 
                 hflags, ann, from_history_size);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins,
                      Int_t nbinsy, Double_t * ybins, 
                      GnamPAO *tmppao, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamHisto2D (name, title, nbinsx, xbins, nbinsy, ybins, 
                 hflags, ann, from_history_size);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Axis_t xlow, Axis_t xup,
                      Int_t nbinsy, Axis_t ylow, Axis_t yup, 
                      const char *pname, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamPAO *tmppao;
    GnamHisto2D (name, title, nbinsx, xlow, xup, nbinsy, ylow, yup, 
                 hflags, ann, from_history_size);
    tmppao = findPAO (pname);
    ERS_ASSERT (tmppao);
    this->AddToPAO (tmppao);
}

GnamHisto::GnamHisto (const char *name, const char *title,
                      Int_t nbinsx, Double_t * xbins,
                      Int_t nbinsy, Double_t * ybins, 
                      const char *pname, int hflags,
                      const GNAM_ann_t &ann,
                      Int_t from_history_size)
{ 
    GnamPAO *tmppao;
    GnamHisto2D (name, title, nbinsx, xbins, nbinsy, ybins, 
                 hflags, ann, from_history_size);
    tmppao = findPAO (pname);
    ERS_ASSERT (tmppao);
    this->AddToPAO (tmppao);
}

GnamHisto::~GnamHisto(void)
{
    this->InternalCheck();
    GnamHisto::RemoveName (this->GetName());
    if (mypao) this->RemoveFromPAO();
    delete histo1;
    //histo1 = 0;
    //histo2 = 0;
    //mypao = 0;
}

/*
 * HISTORY
 */

void
GnamHisto::SetHistory (UInt_t new_hist_size)
{
    this->InternalCheck();
    this->Reset();
    size_t hist_buf_size;
    if (hist_size) this->UnsetHistory();
    if (this->AsHistory() && (histo_type != HISTO_TYPE_1D))
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), histo_type);
    hist_size = (this->FromHistory() && new_hist_size) ?
        new_hist_size : (UInt_t)(this->GetNbinsX());
    next2add = 0;
    hist_buf_filled_once = false;
    hist_buf_size = hist_size * sizeof (Double_t);
    hist_buf_size *= (histo_type == HISTO_TYPE_2D) ? 3 : 2;
    hist_buf = (Double_t *) malloc (hist_buf_size);
    hist_with_errors = false;
    hist_with_weights = false;
    ERS_ASSERT (hist_buf);
    if (this->AsHistory()) histo1->SetBins (1, 0, 1);
}

void
GnamHisto::UnsetHistory (void)
{
    this->InternalCheck();
    hist_size = 0;
    ERS_ASSERT (hist_buf);
    free (hist_buf);
}

void
GnamHisto::ResetHistory (void)
{
    this->InternalCheck();
    next2add = 0;
    hist_buf_filled_once = false;
    to_be_flushed = false;
}

void
GnamHisto::FillHistory (Double_t x)
{
    this->InternalCheck();
    if (!this->IsFilled()) return;

    ERS_ASSERT (hist_size);
    ERS_ASSERT (hist_buf);
    
    switch (histo_type) {
    case HISTO_TYPE_1D: {
        UInt_t ind = next2add * 2;
        hist_buf[ind] = x;
        break;
    }
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
    next2add ++;
    if (next2add == hist_size) {
        hist_buf_filled_once = true;
        next2add = 0;
    }
    SET_PUBLISH_FLAG(to_be_flushed);
}

void
GnamHisto::FillHistory (Double_t x, Double_t y)
{
    this->InternalCheck();
    if (!this->IsFilled()) return;

    ERS_ASSERT (hist_size);
    ERS_ASSERT (hist_buf);
    
    UInt_t ind;
    switch (histo_type) {
    case HISTO_TYPE_1D: {
        hist_with_weights = true;
        ind = next2add * 2;
        break;
    }
    case HISTO_TYPE_2D: {
        ind = next2add * 3;
        break;
    }
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
    hist_buf[ind] = x;
    ind ++;
    hist_buf[ind] = y;

    next2add ++;
    if (next2add == hist_size) {
        hist_buf_filled_once = true;
        next2add = 0;
    }
    SET_PUBLISH_FLAG(to_be_flushed);
}

void
GnamHisto::FillHistory (Double_t x, Double_t y, Double_t w)
{
    this->InternalCheck();
    if (!this->IsFilled()) return;

    ERS_ASSERT (hist_size);
    ERS_ASSERT (hist_buf);
    
    if ((histo_type != HISTO_TYPE_2D) || ! this->FromHistory())
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));

    hist_with_weights = true;
    UInt_t ind = next2add * 3;
    hist_buf[ind] = x;
    ind ++;
    hist_buf[ind] = y;
    ind ++;
    hist_buf[ind] = w;

    next2add ++;
    if (next2add == hist_size) {
        hist_buf_filled_once = true;
        next2add = 0;
    }
    SET_PUBLISH_FLAG(to_be_flushed);
}

void
GnamHisto::HistorySetBinError (Int_t bin, Double_t error)
{
    this->InternalCheck();
    ERS_ASSERT (hist_size);
    ERS_ASSERT (hist_buf);
    UInt_t ubin = UInt_t(bin);
    UInt_t blim = hist_buf_filled_once ? hist_size : next2add;
    if (ubin >= blim) {
        throw daq::gnamlib::HistoryBinOutOfRange
            (ERS_HERE, __PRETTY_FUNCTION__, blim, GetName(), ubin);
    }
    hist_with_errors = true;
    UInt_t ind = (ubin < next2add) ? next2add-ubin : hist_size+next2add-ubin;
    hist_buf[2*ind-1] = error;

    SET_PUBLISH_FLAG(to_be_published);
}

void
GnamHisto::FlushHistory (void)
{
    if (to_be_flushed)
    {
        if (this->AsHistory())
            this->FillAsHistory();
        else if (this->FromHistory())
            this->FillFromHistory();
        to_be_flushed = false;
        to_be_published = true;
    }
}

UInt_t
GnamHisto::first_to_read (UInt_t n_entries)
{
    this->InternalCheck();
    UInt_t max_avail = hist_buf_filled_once ? hist_size : next2add;
    UInt_t tmp_read;
    MR_DEBUG (3, "max_avail = " << max_avail);
    if (n_entries <= max_avail) {
        tmp_read = n_entries;
    } else {
        MR_DEBUG (1, "histogram " << GetName() << ": " << n_entries
                  << " entries available: " << max_avail << " requested");
        tmp_read = max_avail;
    }
    if (tmp_read <= next2add) {
        return (next2add - tmp_read);
    } else {
        return (hist_size + next2add - tmp_read);
    }
}

void
GnamHisto::FillFromHistory (void)
{
    this->InternalCheck();
    UInt_t k = this->first_to_read (hist_size);
    UInt_t keep_hist_size = hist_size;
    bool break_if_less = true;
    if (k >= next2add) {
        break_if_less = false;
    }
    histo1->Reset ();

    // cheat check on Fill methods
    hist_size = 0;

    MR_DEBUG (1, "Histo " << GetName() << ": hist_size "
              << keep_hist_size << ": next2add " << next2add);

    for (;;) {
        UInt_t ind2 = k * 2;
        UInt_t ind3 = k * 3;
        switch (histo_type) {
        case HISTO_TYPE_1D:
            hist_with_weights
                ? this->Fill (hist_buf[ind2], hist_buf[ind2+1])
                : this->Fill (hist_buf[ind2]);
            break;
        case HISTO_TYPE_2D: {
            hist_with_weights
                ? this->Fill (hist_buf[ind3], hist_buf[ind3+1], hist_buf[ind3+2])
                : this->Fill (hist_buf[ind3], hist_buf[ind3+1]);
            break;
        }
        default:
            FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
        }
        // increase k
        k++;
        if (k == keep_hist_size) {
            k = 0;
            break_if_less = true;
        }
        if (break_if_less && k == next2add) {
            break;
        }
    }
    
    // IMPORTANT
    hist_size = keep_hist_size;
}

void
GnamHisto::FillAsHistory (void)
{
    this->InternalCheck();
    UInt_t k = this->first_to_read (hist_size);
    UInt_t keep_hist_size = hist_size;
    unsigned int j;
    bool break_if_less = true;
    if (k >= next2add) {
        break_if_less = false;
    }

    // this is definitely a 1-dim histo
    switch (histo_type) {
    case HISTO_TYPE_1D:
        break;
    case HISTO_TYPE_2D:
        throw daq::gnamlib::DimNumMismatch
            (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), 2);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }

    histo1->Reset ();
    histo1->SetBins (hist_size, 0, hist_size);
    
    // cheat check on Fill methods
    hist_size = 0;

    for (j = 0; 1; j++) {
        UInt_t ind = k * 2;
        this->Fill (j + 0.5, hist_buf[ind]);
        if (hist_with_errors) this->SetBinError (j + 1, hist_buf[ind+1]);
        // increase k
        k++;
        if (k == keep_hist_size) {
            k = 0;
            break_if_less = true;
        }
        if (break_if_less && k == next2add) {
            break;
        }
    }

    // IMPORTANT
    hist_size = keep_hist_size;
}


/*
 * ROOT
 */

// Fill a 1d histo
Int_t
GnamHisto::Fill (Double_t x)
{
    if (hist_size) {
        this->FillHistory (x);
        return 0;
    }

    this->InternalCheck();
    if (!this->IsFilled()) {
        return 0;
    }

    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Fill ((Axis_t) x);
    case HISTO_TYPE_2D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), 2);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

// Fill a 1d histo with weight OR fill a 2d histo
Int_t
GnamHisto::Fill (Double_t x, Double_t y)
{
    if (hist_size) {
        this->AsHistory() ? this->FillHistory (x*y) : this->FillHistory (x, y);
        return 0;
    }

    this->InternalCheck();
    if (!this->IsFilled()) {
        return 0;
    }

    Int_t retcode;
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        retcode = histo1->Fill ((Axis_t) x, (Stat_t) y);
        return retcode;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        retcode = histo2->Fill ((Axis_t) x, (Axis_t) y);
        return retcode;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

// Fill a 2d histo with weight
Int_t
GnamHisto::Fill (Double_t x, Double_t y, Double_t w)
{
    if (hist_size) {
        this->FillHistory (x, y, w);
        return 0;
    }

    this->InternalCheck();
    if (!this->IsFilled()) {
        return 0;
    }

    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Fill ((Axis_t) x, (Axis_t) y, (Stat_t) w);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Int_t
GnamHisto::Publish (OHRootProvider *provider, unsigned int  *pub_chan)
{
    ERS_PRECONDITION (pub_chan);
    *pub_chan=0;
    this->InternalCheck();
    if (! (this->IsPublished() && this->IsFilled())) {
        return 1;
    }
    if (OH_ObjectTypeMismatch) {
        return 2;
    }
    if (!to_be_published && !to_be_flushed) {
        return 5;
    }
    ERS_ASSERT (provider != NULL);
    try{
        this->FlushHistory();
        switch (histo_type) {
        case HISTO_TYPE_1D:
            provider->publish (*histo1, histo1->GetName(), -1, ann);
            *pub_chan= histo1->GetNbinsX();
            break;
        case HISTO_TYPE_2D:
            provider->publish (*histo2, histo2->GetName(), -1, ann);
            *pub_chan= histo2->GetNbinsX()*histo2->GetNbinsY();
            break;
        default:
            FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
        }
    } catch (daq::oh::ObjectTypeMismatch &ex) {
        MR_ERROR (OHSObjectTypeMismatch (ERS_HERE, GetName(), ex));
        OH_ObjectTypeMismatch = true;
        return 3;
    } catch (daq::oh::RepositoryNotFound &ex) {
        OHSRepositoryNotFound err (ERS_HERE, GetName(), ex);
        if (last_publish_was_ok) {
            last_publish_was_ok = false;
            MR_ERROR (err);
        } else {
            MR_DEBUG (2, err);
        }
        return 4;
    }
    if (!last_publish_was_ok) {
        last_publish_was_ok = true;
        MR_LOG ("histogram '" << GetName() << "' published to OH");
    }
        
    GnamHisto::reset2publish(this);         \
    to_be_published=false;
        
    return 0;
}

/*
TH1 *
GnamHisto::h2dump (void)
{
    this->InternalCheck();
    
    if (IsDumped()) {
        this->FlushHistory();
        return histo1;
    }
    return NULL;
}

TH1 *
GnamHisto::Clone (const char* name)
{
    this->InternalCheck();
    TH1 *hclone = (TH1*)(histo1->Clone (name));
    return hclone;
}
*/

void
GnamHisto::Write (void)
{
    this->InternalCheck();
    
    if(IsDumped()) {
        this->FlushHistory();
        histo1->Write();
    }
}

const char *
GnamHisto::GetName (void) const
{
    this->InternalCheck();
    return histo1->GetName();
}

const char *
GnamHisto::GetTitle (void) const
{
    this->InternalCheck();
    return histo1->GetTitle();
}

TClass *
GnamHisto::IsA (void) const
{
    this->InternalCheck();
    return histo1->IsA();
}

Int_t
GnamHisto::GetNbinsX (void) const
{
    this->InternalCheck();
    return histo1->GetNbinsX();
}

Int_t
GnamHisto::GetNbinsY (void) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->GetNbinsY();
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::Reset (void)
{
    this->InternalCheck();
    this->ResetHistory();
    if (this->GetEntries()) {
        histo1->Reset();
        SET_PUBLISH_FLAG(to_be_published);
    }
}

void
GnamHisto::Reset (Option_t* option)
{
    this->InternalCheck();
    this->ResetHistory();
    if (this->GetEntries()) {
        histo1->Reset (option);
        SET_PUBLISH_FLAG(to_be_published);
    }
}

void
GnamHisto::SetBins (Int_t nXbin, Axis_t Xmin, Axis_t Xmax)
{
    this->InternalCheck();
    histo1->SetBins (nXbin, Xmin, Xmax);
    SET_PUBLISH_FLAG(to_be_published);
}

void
GnamHisto::SetBins (Int_t nXbin, Axis_t Xmin, Axis_t Xmax,
                    Int_t nYbin, Axis_t Ymin, Axis_t Ymax)
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->SetBins (nXbin, Xmin, Xmax, nYbin, Ymin, Ymax);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::SetName (const char *name)
{
    GnamHisto::RemoveName (this->GetName());
    GnamHisto::StoreName (name, this);
    this->InternalCheck();
    histo1->SetName (name);
    SET_PUBLISH_FLAG(to_be_published);
}

void
GnamHisto::SetTitle (const char *name)
{
    this->InternalCheck();
    histo1->SetTitle (name);
    SET_PUBLISH_FLAG(to_be_published);
}

void
GnamHisto::SetNameTitle (const char *name, const char *title)
{
    GnamHisto::RemoveName (this->GetName());
    GnamHisto::StoreName (name, this);
    this->InternalCheck();
    histo1->SetNameTitle (name, title);
    SET_PUBLISH_FLAG(to_be_published);
}

TAxis *
GnamHisto::GetXaxis (void) const
{
    this->InternalCheck();
    return histo1->GetXaxis ();
}

TAxis *
GnamHisto::GetYaxis (void) const
{
    this->InternalCheck();
    return histo1->GetYaxis ();
}

Int_t
GnamHisto::Fit (const char *formula, Option_t *option, Option_t *goption,
                Axis_t xmin, Axis_t xmax)
{
    this->InternalCheck();
    if (!GnamHistoFitEnabled()) {
        return 0;
    }
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Fit (formula, option, goption, xmin, xmax);
    case HISTO_TYPE_2D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), 2);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Int_t
GnamHisto::Fit (TF1 *f1, Option_t* option, Option_t* goption,
                Axis_t xmin, Axis_t xmax)
{
    this->InternalCheck();
    if (!GnamHistoFitEnabled()) {
        return 0;
    }
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Fit (f1, option, goption, xmin, xmax);
    case HISTO_TYPE_2D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), 2);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::FitSlicesX (TF1* f1, Int_t binmin, Int_t binmax, Int_t cut,
                       Option_t* option)
{
    this->InternalCheck();
    if (!GnamHistoFitEnabled()) {
        return;
    }
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        histo2->FitSlicesX (f1, binmin, binmax, cut, option);
        SET_PUBLISH_FLAG(to_be_published);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::FitSlicesY (TF1* f1, Int_t binmin, Int_t binmax, Int_t cut,
                       Option_t* option)
{
    this->InternalCheck();
    if (!GnamHistoFitEnabled()) {
        return;
    }
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        histo2->FitSlicesY (f1, binmin, binmax, cut, option);
        SET_PUBLISH_FLAG(to_be_published);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Stat_t
GnamHisto::GetEntries (void) const
{
    this->InternalCheck();
    return hist_size
            ? (hist_buf_filled_once ? hist_size : next2add)
            : histo1->GetEntries ();
}

Stat_t
GnamHisto::GetMean (Int_t axis) const
{
    this->InternalCheck();
    return histo1->GetMean (axis);
}

void
GnamHisto::StatOverflows (Bool_t flag)
{
    this->InternalCheck();
    histo1->StatOverflows (flag);
}

Double_t
GnamHisto::GetRMS (Int_t axis) const
{
    this->InternalCheck();
    return histo1->GetRMS (axis);
}

Double_t
GnamHisto::GetRMSError (Int_t axis) const
{
    this->InternalCheck();
    return histo1->GetRMSError (axis);
}

Int_t
//GnamHisto::GetBin(Int_t binx, Int_t biny, Int_t binz) const
GnamHisto::GetBin(Int_t binx, Int_t biny) const
{
    this->InternalCheck();
    /*
      if (binz != 0) {
      throw daq::gnamlib::DimNumMismatch 
      (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
      MR_DEBUG (0, "ERROR: 3-dim method requested");
      return -1;
      }
    */
    switch (histo_type) {
    case HISTO_TYPE_1D:
        if (biny != 0) {
            throw daq::gnamlib::DimNumMismatch 
                (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
        }
        return histo1->GetBin (binx);
    case HISTO_TYPE_2D:
        return histo2->GetBin (binx, biny);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Axis_t
GnamHisto::GetBinCenter(Int_t bin) const
{
    this->InternalCheck();
    return histo1->GetBinCenter (bin);
}

Stat_t
GnamHisto::GetBinContent(Int_t bin) const
{
    this->InternalCheck();
    return histo1->GetBinContent (bin);
}

Stat_t
GnamHisto::GetBinContent (Int_t binx, Int_t biny) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->GetBinContent (binx, biny);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Stat_t
GnamHisto::GetBinError(Int_t bin) const
{
    this->InternalCheck();
    return histo1->GetBinError (bin);
}

Stat_t
GnamHisto::GetBinError(Int_t binx, Int_t biny) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->GetBinError (binx, biny);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Axis_t
GnamHisto::GetBinLowEdge(Int_t bin) const
{
    this->InternalCheck();
    return histo1->GetBinLowEdge (bin);
}

Axis_t
GnamHisto::GetBinWidth(Int_t bin) const
{
    this->InternalCheck();
    return histo1->GetBinWidth (bin);
}


TProfile* 
GnamHisto::ProfileX(const char* name, Int_t firstybin, 
                    Int_t lastybin, Option_t* option) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->ProfileX (name,firstybin,lastybin,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

TProfile* 
GnamHisto::ProfileY(const char* name, Int_t firstxbin, 
                    Int_t lastxbin, Option_t* option) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->ProfileY (name,firstxbin,lastxbin,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::Scale(Double_t c1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Scale(c1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Scale(c1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

TH1D* 
GnamHisto::ProjectionX(const char* name, Int_t firstybin, 
                       Int_t lastybin, Option_t* option) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->ProjectionX (name,firstybin,lastybin,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

TH1D* 
GnamHisto::ProjectionY(const char* name, Int_t firstxbin, 
                       Int_t lastxbin, Option_t* option) const
{
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        throw daq::gnamlib::DimNumMismatch 
            (ERS_HERE, __PRETTY_FUNCTION__, 2, GetName(), 1);
    case HISTO_TYPE_2D:
        return histo2->ProjectionY (name,firstxbin,lastxbin,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}


TF1 * 
GnamHisto::GetFunction(const char *name){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        return histo1->GetFunction(name);
    case HISTO_TYPE_2D:
        return histo2->GetFunction(name);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}


Bool_t 
GnamHisto::Add(const TH1* h1, Double_t c1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Add(h1,c1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Add(h1,c1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}
 
Bool_t
GnamHisto::Add(TF1* h1, Double_t c1, Option_t* option){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Add(h1,c1,option);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Add(h1,c1,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t 
GnamHisto::Add(const TH1* h, const TH1* h2, Double_t c1, Double_t c2){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Add(h,h2,c1,c2);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Add(h,h2,c1,c2);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::AddBinContent(Int_t bin){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->AddBinContent(bin);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->AddBinContent(bin);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::AddBinContent(Int_t bin, Double_t w){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->AddBinContent(bin,w);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->AddBinContent(bin,w);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::SetBinContent(Int_t bin, Double_t content){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinContent(bin,content);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinContent(bin,content);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::SetBinContent(Int_t binx, Int_t biny, Double_t content){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinContent(binx,biny,content);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinContent(binx,biny,content);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::SetBinContent(Int_t binx, Int_t biny, Int_t binz, Double_t content){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinContent(binx,biny,binz,content);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinContent(binx,biny,binz,content);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::SetBinError(Int_t bin, Double_t error){
    this->InternalCheck();
    if (hist_size) {
        if ((histo_type != HISTO_TYPE_1D) || this->FromHistory())
            throw daq::gnamlib::DimNumMismatch 
                (ERS_HERE, __PRETTY_FUNCTION__, 1, GetName(), histo_type);
        this->HistorySetBinError (bin, error);
        return;
    }
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinError(bin,error);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinError(bin,error);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void
GnamHisto::SetBinError(Int_t binx, Int_t biny, Double_t error){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinError(binx,biny,error);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinError(binx,biny,error);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

void 
GnamHisto::SetBinError(Int_t binx, Int_t biny, Int_t binz, Double_t error){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        histo1->SetBinError(binx,biny,binz,error);
        return;
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        histo2->SetBinError(binx,biny,binz,error);
        return;
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t 
GnamHisto::Divide(const TH1* h1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Divide(h1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Divide(h1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}
 
Bool_t
GnamHisto::Divide(TF1* f1, Double_t c1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Divide(f1,c1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Divide(f1,c1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t 
GnamHisto::Divide(const TH1* h1, const TH1* h2, Double_t c1, 
                  Double_t c2, Option_t* option){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Divide(h1,h2,c1,c2,option);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Divide(h1,h2,c1,c2,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t
GnamHisto::Multiply(const TH1* h1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        return histo1->Multiply(h1);
    case HISTO_TYPE_2D:
        return histo2->Multiply(h1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t 
GnamHisto::Multiply(TF1* h1, Double_t c1){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Multiply(h1,c1);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Multiply(h1,c1);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

Bool_t 
GnamHisto::Multiply(const TH1* h1, const TH1* h2, Double_t c1,
                    Double_t c2, Option_t* option){
    this->InternalCheck();
    switch (histo_type) {
    case HISTO_TYPE_1D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo1->Multiply(h1,h2,c1,c2,option);
    case HISTO_TYPE_2D:
        SET_PUBLISH_FLAG(to_be_published);
        return histo2->Multiply(h1,h2,c1,c2,option);
    default:
        FatalError (UnexpectedHistoType (ERS_HERE, histo_type));
    }
}

// dynalib

unsigned int 
GnamHisto::GetDynalib (void) const
{
    return dynalib_ind;
}

int
GnamHisto::ghctl (int f, void* p)
{
    return (p) ? f : 0;
} 

