#include "gnam/gnamutils/GnamLibException.h"
#include "gnam/gnamutils/GnamMessRep.h"
#include "gnam/gnamutils/GnamHisto.h"
#include "gnam/gnamutils/GnamPAO.h"

/*
 * Naming convention
 */
static void
checkName (const char *name)
{
    const char *tmp, *ind;
    int n;

    //at least 3 '/'
    tmp = name;
    n = 0;
    do {
        tmp = index (tmp, '/');
        ind = tmp;
        tmp++;
        n++;
    } while (ind != NULL);
  
    if (n<4) {
        FatalError (daq::gnamlib::InvalidHistoName
                    (ERS_HERE, name, "Histogram names should contain at least"
                     " 3 '/' characters"));
    }

    //name must start with /SHIFT or /EXPERT or /RUNSTAT or /DEBUG
    if (strncmp (name, "/SHIFT/", strlen ("/SHIFT/")) != 0
        && strncmp (name, "/EXPERT/", strlen ("/EXPERT/")) != 0
        && strncmp (name, "/RUNSTAT/", strlen ("/RUNSTAT/")) != 0
        && strncmp (name, "/DEBUG/", strlen ("/DEBUG/")) != 0)
    {
        FatalError (daq::gnamlib::InvalidHistoName
                    (ERS_HERE, name, "Histogram names should start with"
                     " /SHIFT or /EXPERT or /RUNSTAT or /DEBUG"));
    }
}

/* Static members and methods */

gnamMap_t GnamHisto::m_gnamHistoMap;
GnamQueue GnamHisto::m_gnamPublishQueue;

GnamHisto *
GnamHisto::name2histo (const char *name)
{
    if (name == NULL) return NULL;
    std::string sname(name);
    return (m_gnamHistoMap.count(sname)) ? m_gnamHistoMap[sname] : NULL;
}

void
GnamHisto::StoreName (const char *name, GnamHisto *ghisto)
{
    checkName (name);
    std::string sname(name);
    if (m_gnamHistoMap.count(sname))
        throw daq::gnamlib::DuplicatedHistoName (ERS_HERE, name);
    m_gnamHistoMap[sname] = ghisto;
}

void
GnamHisto::RemoveName (const char *name)
{
    std::string sname(name);
    if (m_gnamHistoMap.count(sname)) {
        m_gnamPublishQueue.erase(m_gnamHistoMap[sname]);
        m_gnamHistoMap.erase(sname);
    }
}

void
GnamHisto::setPublishAll()
{
    gnamMap_t::iterator ghit;
    // Set the publish flags, so we can have the first
    // publication for all the histos
    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        (*ghit).second->SetPublishFlag();
    }
}

void
GnamHisto::resetAll()
{
    gnamMap_t::iterator ghit;

    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        (*ghit).second->Reset();
    }
}

void
GnamHisto::flushAll()
{
    gnamMap_t::iterator ghit;

    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        (*ghit).second->FlushHistory();
    }
}

bool
GnamHisto::available2publish()
{
    return ! m_gnamPublishQueue.empty();
}

std::string::size_type
GnamHisto::nr2publish()
{
    return m_gnamPublishQueue.size();
}

GnamHisto *
GnamHisto::next2publish()
{
    return (GnamHisto *)m_gnamPublishQueue.pop();
}

void
GnamHisto::set2publish (GnamHisto *ghisto)
{
    GnamPAO* pao = ghisto->GetPAO();
    // uint32_t nbins = ghisto->nBins();
    if (pao != NULL) {
        const std::vector<GnamHisto*>& pao_list = PAOGetHistos (pao);
        if (pao_list.size() > 0) ghisto = pao_list[0];
    }
    m_gnamPublishQueue.push (ghisto);
}

void
GnamHisto::reset2publish (GnamHisto *ghisto)
{
    m_gnamPublishQueue.erase (ghisto);
}

unsigned int
GnamHisto::publishAll (OHRootProvider *provider)
{
    if (provider == 0) return 0;
    unsigned int pub_tot = 0;

    GnamHisto * ghisto;
    while (!m_gnamPublishQueue.empty()) {
        unsigned int pub_chan;
        ghisto = (GnamHisto *)m_gnamPublishQueue.pop();
        ghisto->Publish (provider, &pub_chan);
        pub_tot += pub_chan;
    }
    return pub_tot;
}

void
GnamHisto::writeAll()
{
    gnamMap_t::iterator ghit;

    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        (*ghit).second->Write();
    }
}

void
GnamHisto::cleanUp (std::map<uint32_t,uint32_t> & tobecleaned)
{
    unsigned int dlnr;
    GnamHisto * myhisto;

    while (m_gnamHistoMap.size()) {
        myhisto = m_gnamHistoMap.begin()->second;
        dlnr = myhisto->GetDynalib();
        tobecleaned[dlnr] ++;
        delete myhisto;
    }
}

gnamMap_t &
GnamHisto::getHistoMap()
{
    return m_gnamHistoMap;
}

// warm start / warm stop handling

bool GnamHisto::m_ready(false);
uint32_t GnamHisto::m_fillMask ( GnamHisto::NOT_FILL_WHEN_STANDBY | GnamHisto::NOTFILLED );

void
GnamHisto::GnamHistoSetReady ( bool s)
{
    uint32_t k = s ? GnamHisto::NOT_FILL_WHEN_READY : GnamHisto::NOT_FILL_WHEN_STANDBY;
    m_fillMask = k | GnamHisto::NOTFILLED;
    m_ready = s;
}

void
GnamHisto::warmStart ()
{
    m_fillMask = GnamHisto::NOT_FILL_WHEN_READY | GnamHisto::NOTFILLED;
    m_ready = true;

    gnamMap_t::iterator ghit;
    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        if (ghit->second->ResetAtWarmStart()) ghit->second->Reset();
        else if (ghit->second->StartFillingResetWhenReady()) {
            ghit->second->Reset();
            ghit->second->StartFillingResetWhenReady(false);
            ghit->second->FillWhenReady(true);
        }
    }
}

void
GnamHisto::warmStop ()
{
    m_fillMask = GnamHisto::NOT_FILL_WHEN_STANDBY | GnamHisto::NOTFILLED;
    m_ready = false;

    gnamMap_t::iterator ghit;
    for (ghit = m_gnamHistoMap.begin(); ghit != m_gnamHistoMap.end(); ghit++) {
        if (ghit->second->ResetAtWarmStop()) ghit->second->Reset();
    }
}

void * GnamHisto::m_GnamHistoPtr(0);

int
GnamHisto::GnamHistoCtl (int f, void* p)
{
    return (p) ? f : 0;
}

