/* $Id$ */

#ifdef GNAM_DEBUG_LINE_PARSE
#include <stdio.h>
#define MR_DEBUG fprintf
#endif

#include "gnam/gnamutils/line_parse.h"

#include <string.h>

static char open_string = 0;
static char open_quote = 0;
static char quoted_char = 0;
/* internal copy of final arguments */
static int n_strings_tmp = 0;
static int n_chars_tmp = 0;
static char *first_string_tmp = NULL;
/* first string end */
static char *first_string_end = NULL;

static void
reset (void)
{
    open_string = 0;
    open_quote = 0;
    quoted_char = 0;
    n_strings_tmp = 0;
    n_chars_tmp = 0;
    first_string_tmp = NULL;
    first_string_end = NULL;
}

int
line_parse (char *string, ssize_t size, size_t *n_strings, size_t *n_chars,
            char **first_string)
{
    /* string length */
    size_t len;
    /* tmp variables */
    /*size_t k; */
    ssize_t k;
    char ch;
    reset ();
    len = strlen (string);
#ifdef GNAM_DEBUG_LINE_PARSE
    MR_DEBUG (/* 3, */ "%d ==? %d", len, size);
#endif
    /*for (k = 0; k < len; k++) { */
    for (k = 0; k < size; k++) {
        ch = string[k];
        /*MR_DEBUG (3, "%c (%d)", ch, ch); */
        if (ch == 0) {
#ifdef GNAM_DEBUG_LINE_PARSE
            MR_DEBUG (/* 3, */ "reset");
#endif
            reset ();
            continue;
        }
        if (ch == '\\' && !quoted_char) {
            quoted_char = 1;
            continue;
        }
        if (open_quote) {
            if (!open_string) {
#ifdef GNAM_DEBUG_LINE_PARSE
                MR_DEBUG (/* 3, */ "FATAL: Internal Error 1");
#endif
                exit (3);
            }
            if (ch == '"' && !quoted_char) {
                open_quote = 0;
            } else {
                n_chars_tmp++;
                /*MR_DEBUG (3, "n_chars_tmp++ (%d)", n_chars_tmp); */
            }
        } else if (ch == '#' && !open_string) {
            break;
        } else if (isspace (ch)) {
            if (open_string) {
                open_string = 0;
                if (n_strings_tmp == 1) {
                    first_string_end = string + k;
                }
            }
        } else {
            if (!open_string) {
                open_string = 1;
                /*MR_DEBUG (3, "n_strings_tmp++"); */
                n_strings_tmp++;
                if (n_strings_tmp == 1) {
                    first_string_tmp = string + k;
                }
            }
            if (ch == '"' && !quoted_char) {
                open_quote = 1;
            } else {
                n_chars_tmp++;
                /*MR_DEBUG (3, "n_chars_tmp++ (%d)", n_chars_tmp); */
            }
        }
        quoted_char = 0;
    }
    /* Final check */
    if (open_quote) {
#ifdef GNAM_DEBUG_LINE_PARSE
        MR_DEBUG (/* 3, */ "ERROR: Invalid Quoting: **%s**", string);
#endif
        return 1;
    }
    /*n_strings, size_t *n_chars, char **first_string */
    if (n_strings != NULL) {
        *n_strings = n_strings_tmp;
    }
    if (n_chars != NULL) {
        *n_chars = n_chars_tmp;
    }
    if (first_string != NULL) {
        if (first_string_end != NULL) {
            *first_string_end = 0;
        }
        *first_string = first_string_tmp;
    }
#ifdef GNAM_DEBUG_LINE_PARSE
    MR_DEBUG (/* 3, */ "n_strings_tmp: %d", n_strings_tmp);
#endif
#ifdef GNAM_DEBUG_LINE_PARSE
    MR_DEBUG (/* 3, */ "n_chars_tmp: %d", n_chars_tmp);
#endif
    if (first_string_tmp != NULL) {
#ifdef GNAM_DEBUG_LINE_PARSE
        MR_DEBUG (/* 3, */ "--%s--", first_string_tmp);
#endif
    }
    return 0;
}
